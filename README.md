# YouboraLib JS
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com) 
[![codecov](https://codecov.io/bb/npaw/lib-plugin-js/branch/master/graph/badge.svg)](https://codecov.io/bb/npaw/lib-plugin-js)
[![Package Quality](https://packagequality.com/shield/youboralib.svg)](https://packagequality.com/#?package=youboralib)
[![Pull requests](https://img.shields.io/bitbucket/pr/npaw/lib-plugin-js)](https://bitbucket.org/npaw/lib-plugin-js/pull-requests/)

## Folder Structure
```
/
├── dist/     Built & Minified files ready to include and run
├── spec/     Tests
├── samples/  Sample files
└── src/      Sourcecode of the project
```

## Documentation, Installation & Usage
Please refer to [Our Documentation](https://documentation.npaw.com/npaw).

## I need help!
If you find a bug, have a suggestion or need assistance send an E-mail to <support@nicepeopleatwork.com>
