const YouboraProductAnalytics = require('../../src/productanalytics/productanalytics')

describe('ProductAnalytics', () => {
  var Plugin = require('../../src/plugin/plugin')
  var Adapter = require('../../src/adapter/adapter')
  var Infinity = require('../../src/infinity/infinity')
  var Log = require('../../src/log')
  var ProductAnalyticsUserState = require('../../src/productanalytics/productanalytics_userstate')
  var plugin

  beforeEach(() => {
    global.spyXHR()

    plugin = new Plugin({ 'accountCode': 'nicedev' })
    plugin.productAnalytics.initialize({autoTrackNavigation: false, autoTrackAttribution: false, enableStateTracking: true, pageRules: [{rule: '\/movies$', page: 'Movies'}]});

    pluginAutoEnabled = new Plugin({ 'accountCode': 'nicedev' })
    pluginAutoEnabled.productAnalytics.initialize({autoTrackNavigation: true, autoTrackAttribution: true, enableStateTracking: true, pageRules: [{rule: '\/movies$', page: 'Movies'}]});
  })

  // -----------------------------------------------------------------------------------------------
  // SESSION
  // -----------------------------------------------------------------------------------------------

  it('should newSession', (done) => {
    plugin.infinity.on(Infinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_START)
      done()
    })
    plugin.productAnalytics.newSession()
  })

  it('should endSession', (done) => {
    plugin.infinity.on(Infinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_STOP)
      done()
    })
    plugin.productAnalytics.endSession()
  })

  // -----------------------------------------------------------------------------------------------
  // LOGIN / LOGOUT
  // -----------------------------------------------------------------------------------------------

  it('should not loginSuccessful when no valid arguments are supplied', (done) => {
    spyOn(Log, 'warn')

    plugin.productAnalytics.loginSuccessful()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot log in successfully since userId is unavailable.')

    plugin.productAnalytics.loginSuccessful(null)
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot log in successfully since userId is unavailable.')

    plugin.productAnalytics.loginSuccessful('')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot log in successfully since userId is unset.')

    done()
  })

  it('should loginSuccessful', (done) => {

    var userId = '000001'
    var completed = 0

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('User Login Successful')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.user)
      expect(e.data.params.username).toBe(userId)
      expect(completed).toBe(0)
      completed += 1
    })

    plugin.infinity.on(Infinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_STOP)
      expect(completed).toBe(1)
      completed += 2
    })

    plugin.infinity.on(Infinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_START)
      expect(completed).toBe(3)
      expect(e.data.params.username).toBe(userId)
      done()
    })

    plugin.productAnalytics.loginSuccessful(userId)
  })


  it('should loginSuccessful with profile', (done) => {

    var userId = '000001'
    var profileId = '000009'
    var completed = 0

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {

      switch (completed){
        case 0:
          expect(e.type).toBe(Infinity.Event.EVENT)
          expect(e.data.params.name).toBe('User Login Successful')
          expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.user)
          expect(e.data.params.username).toBe(userId)
          expect(completed).toBe(0)
          completed += 1
          break
        case 1:
          expect(e.type).toBe(Infinity.Event.EVENT)
          expect(e.data.params.name).toBe('User Profile Selected')
          expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.userProfile)
          expect(e.data.params.profileId).toBe(profileId)
          expect(completed).toBe(1)
          completed += 2
          break
        default:
          expect(completed).toBe(0)
          break;

      }
    })

    plugin.infinity.on(Infinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_STOP)
      expect(completed).toBe(3)
      completed += 2
    })

    plugin.infinity.on(Infinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_START)
      expect(completed).toBe(5)
      expect(e.data.params.username).toBe(userId)
      expect(e.data.params.profileId).toBe(profileId)
      done()
    })

    plugin.productAnalytics.loginSuccessful(userId, profileId)
  })

  it('should loginUnsuccessful', (done) => {

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('User Login Unsuccessful')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.user)
      done()
    })

    plugin.productAnalytics.loginUnsuccessful()
  })

  it('should logout', (done) => {
    completed = 0

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('User Logout')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.user)
      expect(completed).toBe(0)
      completed += 1
    })

    plugin.infinity.on(Infinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_STOP)
      expect(completed).toBe(1)
      completed += 2
    })

    plugin.infinity.on(Infinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_START)
      expect(e.data.params.username).toBe(undefined)
      expect(completed).toBe(3)
      done()
    })

    plugin.productAnalytics.logout()
  })

  // -----------------------------------------------------------------------------------------------
  // PROFILE
  // -----------------------------------------------------------------------------------------------

  it('should not userProfileCreated when no valid arguments are supplied', (done) => {
    spyOn(Log, 'warn')

    plugin.productAnalytics.userProfileCreated()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot create user profile since profileId is unavailable.')

    plugin.productAnalytics.userProfileCreated(null)
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot create user profile since profileId is unavailable.')

    plugin.productAnalytics.userProfileCreated('')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot create user profile since profileId is unset.')


    plugin.productAnalytics.endSession()
    plugin.productAnalytics.userProfileCreated('aaaa')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot create user profile since session is closed.')

    done()
  })

  it('should userProfileCreated', (done) => {

    var userProfile = '0005'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('User Profile Created')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.userProfile)
      expect(e.data.params.profileId).toBe(userProfile)
      done()
    })

    plugin.productAnalytics.userProfileCreated(userProfile)
  })

  it('should not userProfileSelected when no valid arguments are supplied', (done) => {
    spyOn(Log, 'warn')

    plugin.productAnalytics.userProfileSelected()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot select user profile since profileId is unavailable.')

    plugin.productAnalytics.userProfileSelected(null)
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot select user profile since profileId is unavailable.')

    plugin.productAnalytics.userProfileSelected('')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot select user profile since profileId is unset.')

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.userProfileSelected('aaaa')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot select user profile since session is closed.')

    done()
  })

  it('should userProfileSelected', (done) => {

    var userProfile = '0005'
    var profileType = 'Kid'
    var completed = 0

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('User Profile Selected')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.userProfile)
      expect(e.data.params.dimensions.profileType).toBe(profileType)
      expect(e.data.params.profileId).toBe(userProfile)
      expect(completed).toBe(0)
      completed += 1
    })

    plugin.infinity.on(Infinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_STOP)
      expect(completed).toBe(1)
      completed += 2
    })

    plugin.infinity.on(Infinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(Infinity.Event.SESSION_START)
      expect(e.data.params.profileId).toBe(userProfile)
      expect(completed).toBe(3)
      done()
    })

    plugin.productAnalytics.userProfileSelected(userProfile, profileType)
  })


  it('should not userProfileDeleted when no valid arguments are supplied', (done) => {
    spyOn(Log, 'warn')

    plugin.productAnalytics.userProfileDeleted()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot delete user profile since profileId is unavailable.')

    plugin.productAnalytics.userProfileDeleted(null)
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot delete user profile since profileId is unavailable.')

    plugin.productAnalytics.userProfileDeleted('')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot delete user profile since profileId is unset.')

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.userProfileDeleted('aaaa')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot delete user profile since session is closed.')

    done()
  })

  it('should userProfileDeleted', (done) => {

    var userProfile = '0005'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('User Profile Deleted')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.userProfile)
      expect(e.data.params.profileId).toBe(userProfile)
      done()
    })

    plugin.productAnalytics.userProfileDeleted(userProfile)
  })

  // -----------------------------------------------------------------------------------------------
  // NAVIGATION
  // -----------------------------------------------------------------------------------------------

  it('should not call trackNavByName when no page name is supplied', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_trackNavigation')

    plugin.productAnalytics.trackNavByName(null)

    expect(plugin.productAnalytics._trackNavigation).not.toHaveBeenCalled()

    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track navigation since page has not been supplied.')

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackNavByName('aaaa')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track navigation since session is closed.')
  })

  it('should call trackNavByName even if autoTrackNavigation is enabled', () => {
    var page = 'Home'

    spyOn(pluginAutoEnabled.productAnalytics, '_trackNavigation')

    pluginAutoEnabled.productAnalytics.trackNavByName(page)
    expect(pluginAutoEnabled.productAnalytics._trackNavigation).toHaveBeenCalled()
  })

  it('should trackNavByName', (done) => {
    var page = 'Home'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Navigation ' + page)
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.navigation)
      expect(e.data.params.dimensions.page).toBe(page)
      done()
    })

    plugin.productAnalytics.trackNavByName(page)
  })

  it('should not call trackNavByRoute when no valid URL is supplied', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_trackNavigation')

    plugin.productAnalytics.trackNavByRoute(null)
    plugin.productAnalytics.trackNavByRoute('aaabbb')
    expect(plugin.productAnalytics._trackNavigation).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackNavByRoute('https://test.com/movies')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track navigation since session is closed.')
  })

  it('should call trackNavByRoute even if autoTrackNavigation is enabled', (done) => {
    var page = 'Movies'
    var url = 'https://test.com/movies'

    pluginAutoEnabled.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Navigation ' + page)
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.navigation)
      expect(e.data.params.dimensions.page).toBe(page)
      done()
    })

    pluginAutoEnabled.productAnalytics.trackNavByRoute(url)
  })

  it('should trackNavByRoute', (done) => {
    var page = 'Movies'
    var url = 'https://test.com/movies'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Navigation ' + page)
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.navigation)
      expect(e.data.params.dimensions.page).toBe(page)
      done()
    })

    plugin.productAnalytics.trackNavByRoute(url)
  })

  // -----------------------------------------------------------------------------------------------
  // ATTRIBUTION
  // -----------------------------------------------------------------------------------------------

  it('should not call trackAttribution when autoTrackAttribution is enabled', () => {
    spyOn(Log, 'warn')
    spyOn(pluginAutoEnabled.productAnalytics, '_trackAttribution')

    pluginAutoEnabled.productAnalytics.trackAttribution('Source', 'Medium', 'Campaign', 'Term', 'Content')
    expect(pluginAutoEnabled.productAnalytics._trackAttribution).not.toHaveBeenCalled()

    pluginAutoEnabled.productAnalytics.endSession()
    pluginAutoEnabled.productAnalytics.trackAttribution('Source', 'Medium', 'Campaign', 'Term', 'Content')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track attribution since session is closed.')
  })

  it('should trackAttribution', (done) => {
    var utmSource = 'source'
    var utmMedium = 'medium'
    var utmCampaign = 'campaign'
    var utmTerm = 'term'
    var utmContent = 'content'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Attribution')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.attribution)
      expect(e.data.params.dimensions.utmSource).toBe(utmSource)
      expect(e.data.params.dimensions.utmMedium).toBe(utmMedium)
      expect(e.data.params.dimensions.utmCampaign).toBe(utmCampaign)
      expect(e.data.params.dimensions.utmTerm).toBe(utmTerm)
      expect(e.data.params.dimensions.utmContent).toBe(utmContent)
      done()
    })

    plugin.productAnalytics.trackAttribution(utmSource, utmMedium, utmCampaign, utmTerm, utmContent)
  })

  // -----------------------------------------------------------------------------------------------
  // SECTION
  // -----------------------------------------------------------------------------------------------

  it('should not call _fireEvent when arguments are not properly supplied to trackSectionVisible', () => {
    var section = 'featured'
    var sectionOrder = 1

    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackSectionVisible()
    plugin.productAnalytics.trackSectionVisible(section)
    plugin.productAnalytics.trackSectionVisible(section, -sectionOrder)
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackSectionVisible(section, sectionOrder)
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track section visible since session is closed.')
  })

  it('should trackSectionVisible', (done) => {
    var section = 'featured'
    var sectionOrder = 1

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Section Visible')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.section)
      expect(e.data.params.dimensions.section).toBe(section)
      expect(e.data.params.dimensions.sectionOrder).toBe(sectionOrder)
      done()
    })

    plugin.productAnalytics.trackSectionVisible(section, sectionOrder)
  })

  it('should not call _fireEvent when arguments are not properly supplied to trackSectionHidden', () => {
    var section = 'featured'
    var sectionOrder = 1

    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackSectionHidden()
    plugin.productAnalytics.trackSectionHidden(section)
    plugin.productAnalytics.trackSectionHidden(section, -sectionOrder)
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackSectionHidden(section, sectionOrder)
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track section hidden since session is closed.')
  })

  it('should trackSectionHidden', (done) => {
    var section = 'featured'
    var sectionOrder = 1

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Section Hidden')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.section)
      expect(e.data.params.dimensions.section).toBe(section)
      expect(e.data.params.dimensions.sectionOrder).toBe(sectionOrder)
      done()
    })

    plugin.productAnalytics.trackSectionHidden(section, sectionOrder)
  })

  // -----------------------------------------------------------------------------------------------
  // CONTENT
  // -----------------------------------------------------------------------------------------------

  it('should not call _fireEvent when arguments are not properly supplied to contentFocusIn', () => {
    var section = 'featured'
    var sectionOrder = 1
    var column = 5
    var row = 3

    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    // section, sectionOrder, column, row, contentID, dimensions, metrics

    plugin.productAnalytics.contentFocusIn()
    plugin.productAnalytics.contentFocusIn(section)
    plugin.productAnalytics.contentFocusIn(section, -sectionOrder)
    plugin.productAnalytics.contentFocusIn(section, sectionOrder)
    plugin.productAnalytics.contentFocusIn(section, sectionOrder, -column)
    plugin.productAnalytics.contentFocusIn(section, sectionOrder, column)
    plugin.productAnalytics.contentFocusIn(section, sectionOrder, column, -row)
    plugin.productAnalytics.contentFocusIn(section, sectionOrder, column, row)
    plugin.productAnalytics.contentFocusIn(section, sectionOrder, column, row, '')
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.contentFocusIn(section, sectionOrder, column, row, 'content')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track content highlight since session is closed.')
  })

  it('should contentFocusIn', (done) => {
    var section = 'featured'
    var sectionOrder = 1
    var column = 5
    var row = 3
    var contentId = 'contentCode'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Section Content Highlight')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.section)
      expect(e.data.params.dimensions.section).toBe(section)
      expect(e.data.params.dimensions.sectionOrder).toBe(sectionOrder)
      expect(e.data.params.dimensions.column).toBe(column)
      expect(e.data.params.dimensions.row).toBe(row)
      expect(e.data.params.contentId).toBe(contentId)
      done()
    })

    plugin.productAnalytics.contentFocusIn(section, sectionOrder, column, row, contentId)
  })


  it('should not call _fireEvent when arguments are not properly supplied to trackContentClick', () => {
    var section = 'featured'
    var sectionOrder = 1
    var column = 5
    var row = 3

    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackContentClick()
    plugin.productAnalytics.trackContentClick(section)
    plugin.productAnalytics.trackContentClick(section, -sectionOrder)
    plugin.productAnalytics.trackContentClick(section, sectionOrder)
    plugin.productAnalytics.trackContentClick(section, sectionOrder, -column)
    plugin.productAnalytics.trackContentClick(section, sectionOrder, column)
    plugin.productAnalytics.trackContentClick(section, sectionOrder, column, -row)
    plugin.productAnalytics.trackContentClick(section, sectionOrder, column, row)
    plugin.productAnalytics.trackContentClick(section, sectionOrder, column, row, '')
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackContentClick(section, sectionOrder, column, row, 'content')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track content click since session is closed.')
  })

  it('should trackContentClick', (done) => {
    var section = 'featured'
    var sectionOrder = 1
    var column = 5
    var row = 3
    var contentId = 'contentCode'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Section Content Click')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.section)
      expect(e.data.params.dimensions.section).toBe(section)
      expect(e.data.params.dimensions.sectionOrder).toBe(sectionOrder)
      expect(e.data.params.dimensions.column).toBe(column)
      expect(e.data.params.dimensions.row).toBe(row)
      expect(e.data.params.contentId).toBe(contentId)
      done()
    })

    plugin.productAnalytics.trackContentClick(section, sectionOrder, column, row, contentId)
  })

  // -----------------------------------------------------------------------------------------------
  // CONTENT PLAYBACK
  // -----------------------------------------------------------------------------------------------

  it('should not call _fireAdapterEvent when arguments are not properly supplied to trackPlay', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireAdapterEvent')

    plugin.productAnalytics.trackPlay()
    expect(plugin.productAnalytics._fireAdapterEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackPlay('content')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track play since session is closed.')
  })

  it('should trackPlay', (done) => {
    var contentId = 'contentCode'

    plugin.setAdapter(new Adapter(true))

    plugin.getAdapter().on(Adapter.Event.START, (e) => {
      expect(e.type).toBe(Adapter.Event.START)
      done()
    })

    plugin.getAdapter().on(Adapter.Event.VIDEO_EVENT, (e) => {
      expect(e.type).toBe(Adapter.Event.VIDEO_EVENT)
      expect(e.data.params.name).toBe('Content Play')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.contentPlayback)
      expect(e.data.params.contentId).toBe(contentId)
      expect(plugin.productAnalytics._userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)
      done()
    })

    plugin.getAdapter().fireStart()

    expect(plugin.getAdapter().flags.isStarted).toBe(true)
    expect(plugin.getAdapter().chronos.join.startTime).not.toBe(0)
    expect(plugin.getAdapter().chronos.total.startTime).not.toBe(0)

    plugin.productAnalytics.trackPlay(contentId)
  })

  it('should not call _fireAdapterEvent when arguments are not properly supplied to trackPlayerInteraction', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireAdapterEvent')

    plugin.productAnalytics.trackPlayerInteraction()
    expect(plugin.productAnalytics._fireAdapterEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackPlayerInteraction('eventName')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track player interaction since session is closed.')
  })

  it('should trackPlayerInteraction', (done) => {
    var eventName = 'pause'

    plugin.setAdapter(new Adapter(true))

    plugin.getAdapter().on(Adapter.Event.START, (e) => {
      expect(e.type).toBe(Adapter.Event.START)
      done()
    })

    plugin.getAdapter().on(Adapter.Event.VIDEO_EVENT, (e) => {
      expect(e.type).toBe(Adapter.Event.VIDEO_EVENT)
      expect(e.data.params.name).toBe('Content Play ' + eventName)
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.contentPlayback)
      expect(plugin.productAnalytics._userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)
      done()
    })

    plugin.getAdapter().fireStart()

    expect(plugin.getAdapter().flags.isStarted).toBe(true)
    expect(plugin.getAdapter().chronos.join.startTime).not.toBe(0)
    expect(plugin.getAdapter().chronos.total.startTime).not.toBe(0)

    plugin.productAnalytics.trackPlayerInteraction(eventName)
  })

  // -----------------------------------------------------------------------------------------------
  // SEARCH
  // -----------------------------------------------------------------------------------------------

  it('should not call _fireEvent when arguments are not properly supplied to trackSearchQuery', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackSearchQuery()
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackSearchQuery('search query')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track search query since session is closed.')
  })

  it('should trackSearchQuery', (done) => {
    var query = 'search query'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Search Query')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.search)
      expect(e.data.params.dimensions.query).toBe(query)
      done()
    })

    plugin.productAnalytics.trackSearchQuery(query)
  })

  it('should not call _fireEvent when arguments are not properly supplied to trackSearchResult', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackSearchResult()
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackSearchResult(5)
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track search result since session is closed.')
  })

  it('should trackSearchResult', (done) => {
    var resultCount = 5
    var query = 'search query'

    plugin.productAnalytics.trackSearchQuery(query)

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Search Results')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.search)
      expect(e.data.params.dimensions.query).toBe(query)
      expect(e.data.params.dimensions.resultCount).toBe(resultCount)
      done()
    })

    plugin.productAnalytics.trackSearchResult(resultCount)
  })

  it('should not call _fireEvent when arguments are not properly supplied to trackSearchClick', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackSearchClick()
    plugin.productAnalytics.trackSearchClick('Search Section')
    plugin.productAnalytics.trackSearchClick('Search Section', 3)
    plugin.productAnalytics.trackSearchClick('Search Section', 3, 1)
    plugin.productAnalytics.trackSearchClick('Search Section', 3, 1, 2)
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackSearchClick('Search Section', 3, 1, 2, 'contentId')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track search click since session is closed.')
  })

  it('should trackSearchClick', (done) => {
    var contentId = 'contentCode'
    var section = 'Search Section'
    var sectionOrder = 3
    var column = 5
    var query = 'search query'
    var row = 3

    plugin.productAnalytics.trackSearchQuery(query)

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Search Result Click')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.search)
      expect(e.data.params.dimensions.query).toBe(query)
      expect(e.data.params.dimensions.section).toBe(section)
      expect(e.data.params.dimensions.sectionOrder).toBe(sectionOrder)
      expect(e.data.params.dimensions.column).toBe(column)
      expect(e.data.params.dimensions.row).toBe(row)
      expect(e.data.params.contentId).toBe(contentId)
      done()
    })

    plugin.productAnalytics.trackSearchClick(section, sectionOrder, column, row, contentId)
  })

  // -----------------------------------------------------------------------------------------------
  // EXTERNAL APPLICATIONS
  // -----------------------------------------------------------------------------------------------

  it('should not call _fireEvent when arguments are not properly supplied to trackExternalAppLaunch', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackExternalAppLaunch()
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackExternalAppLaunch('appName')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track external application launch since session is closed.')
  })

  it('should trackExternalAppLaunch', (done) => {
    var appName = 'Application Name'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('External Application Launch')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.externalApplication)
      expect(e.data.params.dimensions.appName).toBe(appName)
      done()
    })

    plugin.productAnalytics.trackExternalAppLaunch(appName)
  })

  it('should not call _fireEvent when arguments are not properly supplied to trackExternalAppExit', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackExternalAppExit()
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackExternalAppExit('appName')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track external application exit since session is closed.')
  })

  it('should trackExternalAppExit', (done) => {
    var appName = 'Application Name'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('External Application Exit')
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.externalApplication)
      expect(e.data.params.dimensions.appName).toBe(appName)
      done()
    })

    plugin.productAnalytics.trackExternalAppExit(appName)
  })

  // -----------------------------------------------------------------------------------------------
  // ENGAGEMENT
  // -----------------------------------------------------------------------------------------------

  it('should not call _fireEvent when arguments are not properly supplied to trackEngagementEvent', () => {
    var eventName = 'Event Name'

    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackEngagementEvent()
    plugin.productAnalytics.trackEngagementEvent(eventName)
    plugin.productAnalytics.trackEngagementEvent(eventName, null)
    plugin.productAnalytics.trackEngagementEvent(eventName, '')
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackEngagementEvent(eventName, 'contentId')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track engagement event since session is closed.')
  })

  it('should trackEngagementEvent', (done) => {
    var eventName = 'Event Name'
    var contentId = 'contentCode'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Engagement ' + eventName)
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.engagement)
      expect(e.data.params.contentId).toBe(contentId)
      done()
    })

    plugin.productAnalytics.trackEngagementEvent(eventName, contentId)
  })

  // -----------------------------------------------------------------------------------------------
  // CUSTOM EVENT
  // -----------------------------------------------------------------------------------------------

  it('should not call _fireEvent when arguments are not properly supplied to trackEvent', () => {
    spyOn(Log, 'warn')
    spyOn(plugin.productAnalytics, '_fireEvent')

    plugin.productAnalytics.trackEvent()
    expect(plugin.productAnalytics._fireEvent).not.toHaveBeenCalled()

    plugin.productAnalytics.endSession()
    plugin.productAnalytics.trackEvent('eventName')
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot track custom event since session is closed.')
  })

  it('should trackEvent', (done) => {
    var eventName = 'Event Name'

    plugin.infinity.on(Infinity.Event.EVENT, (e) => {
      expect(e.type).toBe(Infinity.Event.EVENT)
      expect(e.data.params.name).toBe('Custom ' + eventName)
      expect(e.data.params.dimensions.eventType).toBe(YouboraProductAnalytics.EventTypes.custom)
      done()
    })

    plugin.productAnalytics.trackEvent(eventName)
  })
})
