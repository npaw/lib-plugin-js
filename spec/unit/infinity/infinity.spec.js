describe('YInfinity', () => {
  var YInfinity = require('../../../src/infinity/infinity')
  var Timer = require('../../../src/timer')
  var pluginMock = {
    viewTransform: {},
    getContext: () => {},
    setOptions: () => {},
    restartViewTransform: () => {},
    _beat: new Timer(this._sendBeat, 30000),
    _sendBeat: () => {},
    getIsSessionExpired: () => { return true },
    storage: {
      getSession: () => { return '1' },
      setSession: () => {},
      setLocal: () => {},
      setStorages: () => {},
      getLocal: () => {},
      getStorages: () => {},
      removeSession: () => {},
      removeLocal: () => {},
      removeStorages: () => {}
    },
    getLastActive: () => {}
  }
  var infinity

  beforeEach(() => {
    infinity = new YInfinity(pluginMock)
  })

  it('return comm', () => {
    expect(infinity.getComm()).toBeUndefined()
    infinity.begin()
    expect(infinity.getComm()).toBeDefined()
  })

  it('should sessionStart', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_START)
      done()
    })
    infinity.fireSessionStart()
  })

  it('should sessionStop', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_STOP)
      done()
    })
    infinity.fireSessionStart()
    infinity.fireSessionStop()
  })

  it('should nav, new context', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.isActive = function () { return true }
    infinity.on(YInfinity.Event.NAV, (e) => {
      expect(e.type).toBe(YInfinity.Event.NAV)
      done()
    })
    infinity.fireNav()
  })

  it('should not nav', () => {
    var infinity = new YInfinity(pluginMock)
    infinity.isActive = function () { return false }
    spyOn(infinity, 'emit')
    infinity.fireNav()
    expect(infinity.emit).not.toHaveBeenCalled()
  })

  it('should not create a context', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.isActive = function () { return true }
    infinity._plugin.getContext = function () {
      return 'Context'
    }
    infinity.on(YInfinity.Event.NAV, (e) => {
      expect(e.type).toBe(YInfinity.Event.NAV)
      done()
    })
    infinity.fireNav()
  })

  it('should event', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.EVENT, (e) => {
      expect(e.type).toBe(YInfinity.Event.EVENT)
      expect(e.data.params.name).toBe('eventname')
      expect(e.data.params.param1).toBe('paramval')
      expect(e.data.params.dimensions.dim1).toBe('dimval1')
      expect(e.data.params.values.val1).toBe(123)
      done()
    })
    infinity.fireSessionStart()
    infinity.fireEvent('eventname',{dim1: 'dimval1'}, {val1: 123}, {param1: 'paramval'})
  })

  it('should start at begin', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_START)
      done()
    })
    infinity.begin()
  })

  it('should start at andBeyond', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_START)
      done()
    })
    infinity.andBeyond()
  })

  it('should create a new session without previous one with newSession', (done)=> {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_START)
      done()
    })
    infinity.newSession()
  })

  it('should create a new session closing previous one with newSession', (done)=> {
    var infinity = new YInfinity(pluginMock)
    spyOn(infinity._plugin,'restartViewTransform')
    infinity.fireSessionStart()
    infinity.on(YInfinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_STOP)
      done()
    })
    infinity.newSession()
    expect(infinity._plugin.restartViewTransform).toHaveBeenCalled()
  })

  it('should be detected as active 1', () => {
    infinity._plugin.storage.getLocal = function (input) {
      if (input === 'infinityStarted') return true
    }
    expect(infinity.isActive()).toBeTruthy()
  })

  it('should be detected as inactive', () => {
    infinity._plugin.storage.getLocal = function (input) {
      return null
    }
    expect(infinity.isActive()).toBeFalsy()
  })

  it('should register', () => {
    infinity.register({ 'dim1': 1 }, { 'val1': 1 })
    expect(infinity._registeredProperties.dimensions.dim1).toBe(1)
    expect(infinity._registeredProperties.values.val1).toBe(1)
  })

  it('should register, removing previous one', () => {
    infinity.register({ 'dim1': 1 }, { 'val1': 1 })
    infinity.register({ 'dim2': 2 }, { 'val2': 2 })
    expect(infinity._registeredProperties.dimensions.dim1).toBeUndefined()
    expect(infinity._registeredProperties.values.val1).toBeUndefined()
    expect(infinity._registeredProperties.dimensions.dim2).toBe(2)
    expect(infinity._registeredProperties.values.val2).toBe(2)
  })

  it('should register once, NOT removing previous one', () => {
    infinity.registerOnce({ 'dim1': 1 }, { 'val1': 1 })
    infinity.registerOnce({ 'dim2': 2 }, { 'val2': 2 })
    expect(infinity._registeredProperties.dimensions.dim2).toBeUndefined()
    expect(infinity._registeredProperties.values.val2).toBeUndefined()
    expect(infinity._registeredProperties.dimensions.dim1).toBe(1)
    expect(infinity._registeredProperties.values.val1).toBe(1)
  })

  it('should unregister', () => {
    infinity.register({ 'dim1': 1 }, { 'val1': 1 })
    infinity.unregister()
    expect(infinity._registeredProperties).toBeFalsy()
  })

  it('should return the required parameters', () => {
    infinity.register({ 'dim1': 1 }, { 'val1': 1 })
    var ret = infinity._getParamsJson({ 'dim2': 2 }, { 'val2': 2 }, 'name')
    expect(ret.params.values.val2).toBe(2)
    expect(ret.params.values.val1).toBe(1)
    expect(ret.params.dimensions.dim2).toBe(2)
    expect(ret.params.dimensions.dim1).toBe(1)
    expect(ret.params.name).toBe('name')
  })

  it('should return the required parameters, removing page and route', () => {
    infinity.register({ 'dim1': 1 }, { 'val1': 1 })
    var ret = infinity._getParamsJson({ 'dim2': 2, 'page': 'asd', 'route': 'asd' }, { 'val2': 2 }, 'name', true)
    expect(ret.params.values).toBeUndefined()
    expect(ret.params.dimensions).toBeUndefined()
  })

  it('should create a new session', () => {
    infinity.fireSessionStart()
    infinity._plugin.setOptions = function () { return null }
    infinity._plugin.restartViewTransform = function () { return null }
    spyOn(infinity, 'fireSessionStop')
    spyOn(infinity, 'fireSessionStart')
    spyOn(infinity._plugin, 'setOptions')
    spyOn(infinity._plugin, 'restartViewTransform')
    infinity.newSession()
    expect(infinity.fireSessionStop).toHaveBeenCalled()
    expect(infinity._plugin.setOptions).toHaveBeenCalled()
    expect(infinity._plugin.restartViewTransform).not.toHaveBeenCalled()
  })

  it('should continue an old view', () => {
    spyOn(infinity._plugin, '_sendBeat')
    spyOn(infinity, 'fireNav')
    spyOn(infinity, 'fireSessionStart')
    infinity._plugin.storage.getLocal = function (e) {
      return (e === 'infinityStarted')
    }
    infinity._plugin.getIsSessionExpired = function () {
      return false
    }
    infinity.begin()
    expect(infinity._plugin._sendBeat).not.toHaveBeenCalled()
    expect(infinity.fireNav).not.toHaveBeenCalled()
    expect(infinity.fireSessionStart).toHaveBeenCalled()
  })
})
