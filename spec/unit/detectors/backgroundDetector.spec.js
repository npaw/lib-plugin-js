describe('Device detector', () => {
  var BackgroundDetector = require('../../../src/detectors/backgroundDetector')

  it('should be created', () => {
    var pluginRef = {
      options: {
        'background.settings': null
      }
    }
    var detector = new BackgroundDetector(pluginRef)
    expect(detector.plugin).toBe(pluginRef)
    expect(detector.isInBackground).toBeFalsy()
    expect(detector.isBackgroundDetectorStarted).toBeFalsy()
  })

  it('should detect going to background', () => {
    var detector = new BackgroundDetector()
    spyOn(detector, '_getSettings')
    detector.startDetection()
    detector._visibilityListener() // trigger window.document.visibilitychange
    expect(detector._getSettings).toHaveBeenCalled()
  })

  it('should detect going to foreground', () => {
    var detector = new BackgroundDetector()
    spyOn(detector, '_getSettings')
    detector.startDetection()
    window.document.visibilityState = 'visible'
    detector._visibilityListener() // trigger window.document.visibilitychange
    expect(detector._getSettings).toHaveBeenCalled()
  })

  it('should get the right settings with generic', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return true },
        isIphone: function () { return false },
        isSmartTV: function () { return false },
        isPlayStation: function () { return false }
      },
      options: {
        'background.settings': 'pause',
        'background.settings.android': 'stop'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, android', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return true },
        isIphone: function () { return false },
        isSmartTV: function () { return false },
        isPlayStation: function () { return false }
      },
      options: {
        'background.settings.android': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, iphone', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return false },
        isIphone: function () { return true },
        isSmartTV: function () { return false },
        isPlayStation: function () { return false }
      },
      options: {
        'background.settings.iOS': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, desktop', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return true },
        isAndroid: function () { return false },
        isIphone: function () { return false },
        isSmartTV: function () { return false },
        isPlayStation: function () { return false }
      },
      options: {
        'background.settings.desktop': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, smartTv', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return true },
        isIphone: function () { return false },
        isSmartTV: function () { return true },
        isPlayStation: function () { return false }
      },
      options: {
        'background.settings.tv': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, playStation', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return false },
        isIphone: function () { return false },
        isSmartTV: function () { return false },
        isPlayStation: function () { return true }
      },
      options: {
        'background.settings.playstation': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should call expected methods for firepause', () => {
    var detector = new BackgroundDetector()
    var adapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    var adsAdapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    detector.plugin = {
      '_adapter': adapter,
      '_adsAdapter': adsAdapter,
      getAdapter: function () { return this._adapter },
      getAdsAdapter: function () { return this._adsAdapter }
    }
    spyOn(detector.plugin._adapter, 'firePause')
    spyOn(detector.plugin._adsAdapter, 'firePause')
    detector._firePause()
    expect(detector.plugin._adapter.firePause).toHaveBeenCalled()
    expect(detector.plugin._adsAdapter.firePause).toHaveBeenCalled()
  })

  it('should call plugin stop', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      fireStop: function () { },
      getAdapter: function () { return null },
      getAdsAdapter: function () { return null }
    }
    spyOn(detector.plugin, 'fireStop')
    detector._fireStop()
    expect(detector.plugin.fireStop).toHaveBeenCalled()
  })

  it('should call plugin pause, ads not playing', () => {
    var detector = new BackgroundDetector()
    var adapter1 = {
      flags: {
        isStarted: true
      },
      firePause: function () { }
    }
    var adapter2 = {
      flags: {
        isStarted: false
      },
      firePause: function () { }
    }
    detector.plugin = {
      fireStop: function () { },
      getAdapter: function () { return adapter1 },
      getAdsAdapter: function () { return adapter2 }
    }
    spyOn(adapter1, 'firePause')
    spyOn(adapter2, 'firePause')
    detector._firePause()
    expect(adapter1.firePause).toHaveBeenCalled()
    expect(adapter2.firePause).toHaveBeenCalled()
  })

  it('should call plugin pause, ads playing', () => {
    var detector = new BackgroundDetector()
    var adapter1 = {
      flags: {
        isStarted: false
      },
      firePause: function () { }
    }
    var adapter2 = {
      flags: {
        isStarted: true
      },
      firePause: function () { }
    }
    detector.plugin = {
      fireStop: function () { },
      getAdapter: function () { return adapter1 },
      getAdsAdapter: function () { return adapter2 }
    }
    spyOn(adapter1, 'firePause')
    spyOn(adapter2, 'firePause')
    detector._firePause()
    expect(adapter1.firePause).toHaveBeenCalled()
    expect(adapter2.firePause).toHaveBeenCalled()
  })

  it('should call expected methods for firestop', () => {
    var detector = new BackgroundDetector()
    var adapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    var adsAdapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    detector.plugin = {
      '_adapter': adapter,
      '_adsAdapter': adsAdapter,
      getAdapter: function () { return this._adapter },
      getAdsAdapter: function () { return this._adsAdapter },
      fireStop: function () { return null }
    }
    spyOn(detector.plugin._adapter, 'fireStop')
    spyOn(detector.plugin._adsAdapter, 'fireStop')
    spyOn(detector.plugin, 'fireStop')
    detector._fireStop()
    expect(detector.plugin._adapter.fireStop).toHaveBeenCalled()
    expect(detector.plugin._adsAdapter.fireStop).toHaveBeenCalled()
    expect(detector.plugin.fireStop).toHaveBeenCalled()
    detector.plugin._adapter = null
    detector._fireStop()
    expect(detector.plugin.fireStop).toHaveBeenCalled()
  }),

    it('should set the plugin to foreground state', () => {
      var detector = new BackgroundDetector()
      var adapter = {
        flags: {
          isStarted: true,
          isEnded: false
        },
        fireStop: function () { return true },
        firePause: function () { return true }
      }
      var adsAdapter = {
        flags: {
          isStarted: true,
          isEnded: false
        },
        fireStop: function () { return true },
        firePause: function () { return true },
        stopChronoView: function () { },
        startChronoView: function () { }
      }
      detector.plugin = {
        '_adapter': adapter,
        '_adsAdapter': adsAdapter,
        getAdapter: function () { return this._adapter },
        getAdsAdapter: function () { return this._adsAdapter },
        fireStop: function () { return null },
        storage: {

          getLocal: function () { return false }
        },
        _sendBeat: function () { },
        _beat: {
          start: function () { },
          chrono: {
            startTime: 0
          },
          stop: function () { }
        },
        restartViewTransform: function () { },
        infinity: {
          infinityStarted: true,
          newSession: function () { }
        }
      }
      spyOn(detector.plugin._adsAdapter, 'startChronoView')
      spyOn(detector.plugin.infinity, 'newSession')
      spyOn(detector.plugin, '_sendBeat')
      spyOn(detector.plugin._beat, 'start')
      detector._toForeground('pause')
      expect(detector.isInBackground).toBe(false)
      expect(detector.plugin._adsAdapter.startChronoView).toHaveBeenCalled()
      expect(detector.plugin.infinity.newSession).toHaveBeenCalled()
      expect(detector.plugin._sendBeat).not.toHaveBeenCalled()
      expect(detector.plugin._beat.start).not.toHaveBeenCalled()

      detector.lastBeatTime = 0
      detector.plugin.sessionExpire = new Date().getTime() + 2000000
      detector._toForeground('pause')
      expect(detector.plugin._sendBeat).toHaveBeenCalled()
      expect(detector.plugin._beat.start).toHaveBeenCalled()
    }),

    it('should set the plugin to background state', () => {
      var detector = new BackgroundDetector()
      var adapter = {
        flags: {
          isStarted: true,
          isEnded: false
        },
        fireStop: function () { return true },
        firePause: function () { return true }
      }
      var adsAdapter = {
        flags: {
          isStarted: true,
          isEnded: false
        },
        fireStop: function () { return true },
        firePause: function () { return true },
        stopChronoView: function () { }
      }
      detector.plugin = {
        '_adapter': adapter,
        '_adsAdapter': adsAdapter,
        getAdapter: function () { return this._adapter },
        getAdsAdapter: function () { return this._adsAdapter },
        fireStop: function () { return null },
        storage: {

          getLocal: function () { return false }
        },
        _sendBeat: function () { },
        _beat: {
          chrono: {
            startTime: 0
          },
          stop: function () { }
        },
        infinity: {
          infinityStarted: true
        }
      }
      spyOn(detector.plugin, '_sendBeat')
      spyOn(detector.plugin._beat, 'stop')

      detector.plugin._beat.chrono.startTime = 100
      detector._toBackground('pause')
      expect(detector.isInBackground).toBe(true)
      expect(detector.plugin._sendBeat).toHaveBeenCalled()
      expect(detector.plugin._sendBeat).not.toHaveBeenCalledWith(0)
      expect(detector.plugin._beat.stop).toHaveBeenCalled()

      detector.plugin._beat.chrono.startTime = null
      detector._toBackground('stop')
      expect(detector.isInBackground).toBe(true)
      expect(detector.plugin._sendBeat).toHaveBeenCalledTimes(2)
      expect(detector.plugin._sendBeat).toHaveBeenCalledWith(0)
      expect(detector.plugin._beat.stop).toHaveBeenCalledTimes(2)
    }),

    it('should block calls when in background', () => {
      var detector = new BackgroundDetector()
      detector.isInBackground = false
      detector._getSettings = function () {
        return null
      }
      detector.plugin = {
        options: {
          'background.enabled': false
        }
      }
      expect(detector.canBlockStartCalls()).toBe(false)

      detector.isInBackground = true
      expect(detector.canBlockStartCalls()).toBe(false)

      detector._getSettings = function () {
        return 'stop'
      }
      expect(detector.canBlockStartCalls()).toBe(false)

      detector.plugin.options['background.enabled'] = true
      expect(detector.canBlockStartCalls()).toBeTruthy()
    })
})
