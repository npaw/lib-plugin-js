describe('Device detector', () => {
  var ResizeDetector = require('../../../src/detectors/resizeScrollDetector')

  it('should stop the chrono when hiding it', () => {
    var adapter = {
      flags: {
        isStarted: true
      },
      stopChronoView: function () { },
      startChronoView: function () { },
      getIsVisible: function () { return false }
    }
    var plugin = {
      getAdsAdapter: function () {
        return adapter
      }
    }
    var detector = new ResizeDetector(plugin)
    spyOn(adapter, 'startChronoView')
    spyOn(adapter, 'stopChronoView')
    detector._changeListener()
    expect(adapter.startChronoView).not.toHaveBeenCalled()
    expect(adapter.stopChronoView).toHaveBeenCalled()
  })


  it('should start the chrono when making it visible', () => {
    var adapter = {
      flags: {
        isStarted: true
      },
      stopChronoView: function () { },
      startChronoView: function () { },
      getIsVisible: function () { return true }
    }
    var plugin = {
      getAdsAdapter: function () {
        return adapter
      }
    }
    var detector = new ResizeDetector(plugin)
    spyOn(adapter, 'startChronoView')
    spyOn(adapter, 'stopChronoView')
    detector._changeListener()
    expect(adapter.startChronoView).toHaveBeenCalled()
    expect(adapter.stopChronoView).not.toHaveBeenCalled()
  })

  it('should ignore it if there is no view started', () => {
    var adapter = {
      flags: {
        isStarted: false
      },
      stopChronoView: function () { },
      startChronoView: function () { },
      getIsVisible: function () { return true }
    }
    var plugin = {
      getAdsAdapter: function () {
        return adapter
      }
    }
    var detector = new ResizeDetector(plugin)
    spyOn(adapter, 'startChronoView')
    spyOn(adapter, 'stopChronoView')
    detector._changeListener()
    expect(adapter.startChronoView).not.toHaveBeenCalled()
    expect(adapter.stopChronoView).not.toHaveBeenCalled()
  })

  it('should get the completion rate and update the values', () => {
    var adapter = {
      flags: {
        isStarted: true
      },
      stopChronoView: function () { },
      startChronoView: function () { },
      getIsVisible: function () { return false }
    }
    var plugin = {
      getAdsAdapter: function () {
        return adapter
      },
      storage: {
        setSession: function () { return null },
        getSession: function () { return null }
      }
    }
    var detector = new ResizeDetector(plugin)
    expect(detector.getScrollDepth()).toBe(null)
    detector._loadListener()
    detector.pageHeight = 2000
    expect(detector.getScrollDepth()).not.toBe(null)
    detector.maxHeight = detector.pageHeight/2
    expect(detector.getScrollDepth()).toBe(50)
    detector.maxHeight = detector.pageHeight/10
    expect(detector.getScrollDepth()).toBe(10)
    detector._changeListener()
    expect(detector.getScrollDepth()).not.toBe(10)
  })
})
