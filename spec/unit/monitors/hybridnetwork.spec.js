describe('HybridNetwork', () => {
  var HybridNetwork = require('../../../src/monitors/hybridnetwork')
  var hybridNetwork = new HybridNetwork()

  it('should get no CDN traffic', () => {
    Streamroot = undefined
    peer5 = undefined
    expect(hybridNetwork.getCdnTraffic()).toBeNull()
  })

  it('should get no P2P traffic', () => {
    Streamroot = undefined
    peer5 = undefined
    expect(hybridNetwork.getP2PTraffic()).toBeNull()
  })

  it('should get no upload traffic', () => {
    Streamroot = undefined
    peer5 = undefined
    expect(hybridNetwork.getUploadTraffic()).toBeNull()
  })

  it('should get CDN traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          stats: {
            isP2PEnabled: true,
            cdn: 100,
            p2p: 100,
            upload: 100
          }
        }, {
          stats: {
            isP2PEnabled: false,
            cdn: 200,
            p2p: 200,
            upload: 200
          }
        }
      ]
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          isP2PEnabled: true,
          stats: {
            p2p: 100
          }
        }, {
          isP2PEnabled: false,
          stats: {
            p2p: 200
          }
        }
      ]
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  it('should get Upload traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          isP2PEnabled: true,
          stats: {
            upload: 200
          }
        }, {
          isP2PEnabled: false,
          stats: {
            upload: 100
          }
        }
      ]
    }
    expect(hybridNetwork.getUploadTraffic()).toBe(200)
  })

  it('should get Upload traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          isP2PEnabled: true
        }, {
          isP2PEnabled: false
        }
      ]
    }
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
  })

  it('should get CDN traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.getStats = function () {
      var object = {
        totalHttpDownloaded: 300
      }
      return object
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.getStats = function () {
      var object = {
        totalP2PDownloaded: 100
      }
      return object
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  it('should get Upload traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.getStats = function () {
      var object = {
        totalP2PUploaded: 200
      }
      return object
    }
    expect(hybridNetwork.getUploadTraffic()).toBe(200)
  })

  it('should get Upload traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.isEnabled = function () {
      return true
    }
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
  })

  it('should get CDN traffic value for teltoo', () => {
    Streamroot = undefined
    teltoo = {}
    teltoo.getStats = function () {
      var object = {
        totalReceivedBytes: 600,
        p2pReceivedBytes: 300
      }
      return object
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for teltoo', () => {
    Streamroot = undefined
    teltoo = {}
    teltoo.getStats = function () {
      var object = {
        totalReceivedBytes: 600,
        p2pReceivedBytes: 100
      }
      return object
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  it('should get P2P enabled value for teltoo', () => {
    Streamroot = undefined
    peer5 = undefined
    teltoo = {}
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
  })

  // New streamroot

  it('should get CDN traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  it('should get Upload traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getUploadTraffic()).toBe(100)
  })

  it('should get Upload traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
    Streamroot.instances[0].dnaUploadEnabled = false
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
    Streamroot.instances[0].dnaDownloadEnabled = false
    expect(hybridNetwork.getIsP2PEnabled()).toBeFalse()
    Streamroot.instances = null
    expect(hybridNetwork.getIsP2PEnabled()).toBeFalse()
  })

 it('should get CDN traffic value for npaw cdn balancer', () => {
    CdnBalancerStats = {
      cdn: {
        responseUUID: 'thisisanid',
        totalDownloadedBytes: 300,
        cdns: [{
          name: 'test',
          bytes: 999,
          chunks: 111,
          failures: 1
        }]
      },
      p2p: {
        downloadEnabled: true,
        downloadedBytes: 100,
        uploadedBytes: 200,
        failed_requests: {
          total: 0,
          absent: 0,
          timeout: 0,
          error: 0
        }
      }
    }
    var cdninfo = hybridNetwork.getMultiCdnInfo()
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
    expect(hybridNetwork.getUploadTraffic()).toBe(200)
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
    expect(hybridNetwork.getIsP2PEnabled()).toBeTrue()
    expect(hybridNetwork.getBalancerResponseId()).toBe('thisisanid')
    expect(hybridNetwork.getBalancerResponseId()).toBe('thisisanid')
    expect(cdninfo.test.downloaded_bytes).toBe(999)
    expect(cdninfo.test.downloaded_chunks).toBe(111)
    expect(cdninfo.test.errors).toBe(1)
    CdnBalancerStats = {}
  })
})
