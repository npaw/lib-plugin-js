describe('BrowserLoadTimes', () => {
  var BrowserLoadTimes = require('../../../src/monitors/browserLoadTimes')
  var timingObject = {
    timing: {
      domContentLoadedEventEnd: 1000,
      navigationStart: 0
    },
    getEntriesByType: function (){return null}
  }

  it('should not be able to get a timeobject', () => {
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    expect(browserLoadTimes.timeObject).toBeNull()
    expect(browserLoadTimes.playerSetup).toBeNull()
    expect(browserLoadTimes.getPageLoadTime()).toBeNull()
    expect(browserLoadTimes.getPlayerStartupTime()).toBeNull()
  })

  it('should create the object', () => {
    window.performance = timingObject
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    expect(browserLoadTimes.timeObject).toEqual(timingObject.timing)
    expect(browserLoadTimes.playerSetup).toBeNull()
  })

  it('should set a player setup time', () => {
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    expect(browserLoadTimes.playerSetup).toBeNull()
    browserLoadTimes.setPlayerSetupTime()
    expect(browserLoadTimes.playerSetup).not.toBeNull()
  })

  it('should not replace the player setup time', () => {
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    browserLoadTimes.setPlayerSetupTime()
    var initialTime = browserLoadTimes.playerSetup
    expect(browserLoadTimes.playerSetup).not.toBeNull()
    browserLoadTimes.setPlayerSetupTime()
    expect(browserLoadTimes.playerSetup).toBe(initialTime)
  })

  it('should calculate a startup time', () => {
    window.performance = timingObject
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    browserLoadTimes.setPlayerSetupTime()
    // Assuming the mock has 'navigationStart' = 0
    expect(browserLoadTimes.getPlayerStartupTime()).toBe(browserLoadTimes.playerSetup)
  })

  it('should calculate a startup time', () => {
    window.performance = timingObject
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    browserLoadTimes.setPlayerSetupTime()
    // Assuming the mock has 'navigationStart' = 0
    expect(browserLoadTimes.getPlayerStartupTime()).toBe(browserLoadTimes.playerSetup)
  })

  it ('should return a metric object', () => {
    window.performance = timingObject
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    browserLoadTimes.perfObject.getEntriesByType = function () {
      return null
    }
    expect (browserLoadTimes._getAllValues()).not.toBeFalsy()
  })

  it ('should fire the event', () => {
    window.performance = timingObject
    var browserLoadTimes = new BrowserLoadTimes({infinity: {fireEvent: function() {}}})
    spyOn(browserLoadTimes.infinity,'fireEvent')
    browserLoadTimes._windowLoaded()
    browserLoadTimes._fireLoadTimesEvent()
    expect(browserLoadTimes.infinity.fireEvent).toHaveBeenCalled()
  })

  it('should calculate a startup time', () => {
    window.performance = timingObject
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    var all = browserLoadTimes._getAllValues()
    expect(all.PageLoadTime).not.toBeUndefined()
    expect(all.DNSTime).not.toBeUndefined()
    expect(all.TCPTime).not.toBeUndefined()
    //expect(all.HandshakeTime).not.toBeUndefined()
    expect(all.DomReadyTime).not.toBeUndefined()
    expect(all.BackendTime).not.toBeUndefined()
    expect(all.FrontendTime).not.toBeUndefined()
    expect(all.VisualReady).not.toBeUndefined()
    // expect(all.TimeToInteractive).not.toBeUndefined()
    // expect(all.JsTime).not.toBeUndefined()
    // expect(all.CssTime).not.toBeUndefined()
    // expect(all.ImageTime).not.toBeUndefined()
    // expect(all.FontTime).not.toBeUndefined()
    expect(all.AvgReqLatency).not.toBeUndefined()
    expect(all.MaxReqLatency).not.toBeUndefined()
    // expect(all.SpeedIndex).not.toBeUndefined()
  })

  it('should not fail', () => {
    window.performance = null
    var browserLoadTimes = new BrowserLoadTimes({infinity: {}})
    var all = browserLoadTimes._getAllValues()
    expect(all.PageLoadTime).toBeUndefined()
    expect(all.DNSTime).toBeUndefined()
    expect(all.TCPTime).toBeUndefined()
    expect(all.HandshakeTime).toBeUndefined()
    expect(all.DomReadyTime).toBeUndefined()
    expect(all.BackendTime).toBeUndefined()
    expect(all.FrontendTime).toBeUndefined()
    expect(all.VisualReady).toBeUndefined()
    expect(all.TimeToInteractive).toBeUndefined()
    expect(all.JsTime).toBeUndefined()
    expect(all.CssTime).toBeUndefined()
    expect(all.ImageTime).toBeUndefined()
    expect(all.FontTime).toBeUndefined()
    expect(all.AvgReqLatency).toBeUndefined()
    expect(all.MaxReqLatency).toBeUndefined()
    expect(all.SpeedIndex).toBeUndefined()
  })
})
