describe('Util', () => {
  var Util = require('../../src/util.js')

  it('should strip protocols', () => {
    expect(Util.stripProtocol('http://google.com')).toBe('google.com')
    expect(Util.stripProtocol('https://google.com')).toBe('google.com')
    expect(Util.stripProtocol('//google.com')).toBe('google.com')
    expect(JSON.stringify(Util.stripProtocol({}))).toBe('{}')
  })

  it('should add protocol', () => {
    expect(Util.addProtocol('google.com', true)).toBe('https://google.com')
    expect(Util.addProtocol('google.com')).toBe('http://google.com')
    expect(Util.addProtocol('google.com', false)).toBe('http://google.com')
  })

  it('should parse numbers', () => {
    expect(Util.parseNumber(NaN, 0)).toBe(0)
    expect(Util.parseNumber(-1, 1)).toBe(1)
    expect(Util.parseNumber(Infinity, 2)).toBe(2)
    expect(Util.parseNumber(null, 3)).toBe(3)
    expect(Util.parseNumber(undefined, 4)).toBe(4)
  })

  it('should build rendition strings', () => {
    expect(Util.buildRenditionString(100, 100, -100)).toBe('100x100')
    expect(Util.buildRenditionString(100, 100, 1000000)).toBe('100x100@1Mbps')
    expect(Util.buildRenditionString(100, 100, NaN)).toBe('100x100')
    expect(Util.buildRenditionString(100, 100, 0.9)).toBe('100x100')
    expect(Util.buildRenditionString(100)).toBe('100bps')
    expect(Util.buildRenditionString(1000)).toBe('1Kbps')
    expect(Util.buildRenditionString()).toBe(null)
  })

  it('should parse metrics values, reduced object', () => {
    var obj = {
      "abc": 123,
      "def": 456
    }

    check = function (result) {
      expect(result.abc).not.toBeUndefined()
      expect(result.abc.value).toBe(123)
      expect(result.abc.oper).toBeUndefined()
      expect(result.def).not.toBeUndefined()
      expect(result.def.value).toBe(456)
      expect(result.def.oper).toBeUndefined()
    }

    check(Util.getMetricsFrom(obj))
    check(Util.getMetricsFrom(null,obj))
  })


  it('should parse metrics values, full object', () => {
    var obj = {
      "abc": {
        "value": 123,
        "oper": "SUM"
      },
      "def":  {
        "value": 456,
        "oper": "SUM"
      },
    }

    check = function (result) {
      expect(result.abc).not.toBeUndefined()
      expect(result.abc.value).toBe(123)
      expect(result.abc.oper).toBe("SUM")
      expect(result.def).not.toBeUndefined()
      expect(result.def.value).toBe(456)
      expect(result.def.oper).toBe("SUM")
    }
    
    check(Util.getMetricsFrom(obj))
    check(Util.getMetricsFrom(null,obj))
  })

  it('should calculate viewability', () => {
    var player = {
      getBoundingClientRect: function () {
        return {
          top: 600,
          bottom: 800,
          left: 900,
          right: 1100,
          height: 200,
          width: 200
        }
      }
    }

    var player2 = {
      getBoundingClientRect: function () {
        return {
          top: 700,
          bottom: 900,
          left: 900,
          right: 1100,
          height: 200,
          width: 200
        }
      }
    }

    var player3 = {
      getBoundingClientRect: function () {
        return {
          top: 1200,
          bottom: 1400,
          left: 900,
          right: 1100,
          height: 200,
          width: 200
        }
      }
    }
    // window.innerHeight = 768
    // window.innerWidth = 1024
    expect(Util.calculateAdViewability()).toBeTrue() // true by default
    expect(Util.calculateAdViewability(player)).toBeTrue() // in the screen
    expect(Util.calculateAdViewability(player2, 1)).toBeTrue() //partially out
    expect(Util.calculateAdViewability(player2)).toBeFalse() //partially out
    expect(Util.calculateAdViewability(player3)).toBeFalse() //out of the screen
    var prevWindow = window
    window = undefined
    expect(Util.calculateAdViewability()).toBeTrue()
    window = prevWindow
  })

  describe('listen all events', () => {

    it('should work with addEventListener', () => {
      var object = {
        addEventListener: () => { }
      }
      spyOn(object, 'addEventListener')

      Util.logAllEvents(object)
      expect(object.addEventListener).toHaveBeenCalled()
    })

    it('should work with on', () => {
      var object = {
        on: () => { }
      }
      spyOn(object, 'on')

      Util.logAllEvents(object)
      expect(object.on).toHaveBeenCalled()
    })

    it('should work with custom function', () => {
      var object = {
        custom: () => { }
      }
      spyOn(object, 'custom')

      Util.logAllEvents(object.custom)
      expect(object.custom).toHaveBeenCalled()
    })

    it('should work with extra events', () => {
      var object = {
        addEventListener: () => { }
      }
      spyOn(object, 'addEventListener')

      Util.logAllEvents(object, ['extraevent'])
      expect(object.addEventListener).toHaveBeenCalled()
    })

    it('should work with extra events and custom report', () => {
      var object = {
        addEventListener: () => { }
      }
      spyOn(object, 'addEventListener')

      Util.logAllEvents(object, [null, 'extraevent'], () => { })
      expect(object.addEventListener).toHaveBeenCalled()
    })

    it('Object.assign', () => {
      var obj = Util.assign({}, { a: 'b' })
      expect(obj.a).toBe('b')
    })

    it('Array.isArray', () => {
      expect(Util.isArray([])).toBe(true)
      expect(Util.isArray({})).toBe(false)
    })
  })
})
