describe('GenericAdapter Events', () => {
  var Adapter = require('../../../src/adapter/adapter')

  it('should /init', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      fireInit: function () { }
    }
    spyOn(adapter.plugin, 'fireInit')
    adapter.fireInit()
    expect(adapter.plugin.fireInit).toHaveBeenCalled()
  })

  it ('should not /init', () => {
    var adapter = new Adapter(true)
    adapter.plugin = null
    var err = null
    try {
    adapter.fireInit()
    } catch (error) {
      err = error
    }
    expect(err).toBeNull()
  })

  it('should /start', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.START, (e) => {
      expect(e.type).toBe(Adapter.Event.START)
      done()
    })
    adapter.fireStart()

    expect(adapter.flags.isStarted).toBe(true)
    expect(adapter.chronos.join.startTime).not.toBe(0)
    expect(adapter.chronos.total.startTime).not.toBe(0)
  })

  it('should not /start', () => {
    var adapter = new Adapter(true)
    adapter.flags.isStarted = true
    adapter.fireStart()
    expect(adapter.chronos.join.startTime).toBeFalsy()
    expect(adapter.chronos.total.startTime).toBeFalsy()
  })

  it('should /join', (done) => {
    var adapter = new Adapter(true)
    adapter.monitor = {
      start: function() {},
      stop: function() {}
    }
    spyOn(adapter.monitor,'start')
    adapter.on(Adapter.Event.JOIN, (e) => {
      expect(e.type).toBe(Adapter.Event.JOIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()

    expect(adapter.flags.isJoined).toBe(true)
    expect(adapter.chronos.join.stopTime).not.toBe(0)
    expect(adapter.monitor.start).toHaveBeenCalled()
  })

  it('should /pause', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.PAUSE, (e) => {
      expect(e.type).toBe(Adapter.Event.PAUSE)
      done()
    })

    adapter.fireStart()
    adapter.fireJoin()
    adapter.firePause()

    expect(adapter.flags.isPaused).toBe(true)
    expect(adapter.chronos.pause.startTime).not.toBe(0)
  })

  it('should /resume', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.RESUME, (e) => {
      expect(e.type).toBe(Adapter.Event.RESUME)
      done()
    })
    adapter.monitor = {
      start: function() {},
      stop: function() {},
      skipNextTick: function() {}
    }
    adapter.fireStart()
    adapter.fireJoin()
    adapter.firePause()
    spyOn(adapter.monitor,'skipNextTick')
    adapter.fireResume()

    expect(adapter.monitor.skipNextTick).toHaveBeenCalled()
    expect(adapter.flags.isPaused).toBe(false)
    expect(adapter.chronos.pause.stopTime).not.toBe(0)
  })

  it('should convert buffer from seek', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.BUFFER_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.fireBufferBegin({}, true)

    expect(adapter.flags.isBuffering).toBe(true)
    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.buffer.startTime).not.toBe(0)
    expect(adapter.chronos.seek.startTime).toBe(0)
  })

  it('should /buffer-begin', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BUFFER_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()

    expect(adapter.flags.isBuffering).toBe(true)
    expect(adapter.chronos.buffer.startTime).not.toBe(0)
  })

  it('should /buffer-end', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BUFFER_END, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_END)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.fireBufferEnd()

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.chronos.buffer.stopTime).not.toBe(0)
  })

  it('should not /buffer-end, but cancel buffer', () => {
    var adapter = new Adapter(true)
    adapter.monitor = {
      start: function() {},
      stop: function() {},
      skipNextTick: function () {}
    }
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    spyOn(adapter.monitor,'skipNextTick')
    adapter.cancelBuffer()
    expect(adapter.monitor.skipNextTick).toHaveBeenCalled()
    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.chronos.buffer.stopTime).not.toBe(0)
  })

  it('should convert seek from buffer', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.SEEK_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.fireSeekBegin({}, true)

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.flags.isSeeking).toBe(true)
    expect(adapter.chronos.buffer.startTime).toBe(0)
    expect(adapter.chronos.seek.startTime).not.toBe(0)
  })

  it('should /seek-begin', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.SEEK_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()

    expect(adapter.flags.isSeeking).toBe(true)
    expect(adapter.chronos.seek.startTime).not.toBe(0)
  })

  it('should NOT /seek', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return true },
      options: {
        'content.isLive.noSeek': true
      }
    }

    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()

    expect(adapter.flags.isSeeking).toBe(false)

    adapter.fireSeekEnd()

    expect(adapter.flags.isSeeking).toBe(false)
  })

  it('should /seek-end', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.on(Adapter.Event.SEEK_END, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_END)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.fireSeekEnd()

    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.seek.stopTime).not.toBe(0)
  })

  it('should not /seek-end, but cancel seek', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      getIsLive: function () { return false }
    }
    adapter.monitor = {
      start: function() {},
      stop: function() {},
      skipNextTick: function () {}
    }

    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    spyOn(adapter.monitor,'skipNextTick')
    adapter.cancelSeek()
    expect(adapter.monitor.skipNextTick).toHaveBeenCalled()

    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.seek.stopTime).not.toBe(0)
  })

  it('should /stop', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })
    adapter.monitor = {
      start: function() {},
      stop: function() {}
    }
    spyOn(adapter.monitor,'stop')
    adapter.plugin = {
      _adapter: null,
      _isStopReady: function () {
        return true
      }
    }

    adapter.fireStart()
    adapter.fireStop()

    expect(adapter.monitor.stop).toHaveBeenCalled()
    expect(adapter.flags.isStarted).toBe(false)
    expect(adapter.chronos.total.fireStopTime).not.toBe(0)
  })

  it ('should not /stop', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      _adapter: null,
      _isStopReady: function () {
        return true
      }
    }
    spyOn(adapter,'emit')
    expect(adapter.flags.isStarted).toBe(false)
    adapter.fireStop()
    expect(adapter.emit).not.toHaveBeenCalled()
  })

  it('should /error', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })
    adapter.fireError()
  })

  it('should /error removing code set', () => {
    var adapter = new Adapter(true)
    spyOn(adapter,'emit')
    adapter.fireError({code: 'shouldnt appear', test: 'abc'})
    expect(adapter.emit).toHaveBeenCalledWith('error',Object({ params: Object({ test: 'abc' }) }))
  })

  it('should /fatalError', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })
    adapter.monitor = {
      start: function() {},
      stop: function() {}
    }
    spyOn(adapter.monitor,'stop')
    adapter.fireFatalError()
    expect(adapter.monitor.stop).toHaveBeenCalled()
  })

  it('should /error when calling fatalError, disabled fatality with errors.nonFatal', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      options : {
        'errors.nonFatal': ["1"]
      },
      _isStopReady: function (){return true}
    }
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).not.toBe(Adapter.Event.STOP)
      fail()
    })

    adapter.fireFatalError("1")
  })

  it('should /error and /stop when calling normal error, enabled fatality with errors.fatal', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      options : {
        'errors.fatal': ["1"]
      },
      _isStopReady:
      function (){return true}
    }
    spyOn(adapter, 'fireStop')
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })
    adapter.fireError("1")
    expect(adapter.fireStop).toHaveBeenCalled()
  })

  it('should ignore error or fatalerror, using errors.ignore', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      options : {
        'errors.ignore': ["1"]
      },
      _isStopReady:
      function (){return true}
    }
    spyOn(adapter, 'emit')
    adapter.fireError("1")
    adapter.fireFatalError("1")
    expect(adapter.emit).not.toHaveBeenCalled()
  })

  it('should /stop (casted)', (done) => {
    var adapter = new Adapter(true)

    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })

    adapter.fireStart()
    adapter.fireCasted()

    expect(adapter.flags.isStarted).toBe(false)
    expect(adapter.chronos.total.fireStopTime).not.toBe(0)
  })

  it('should /adClick', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.CLICK, (e) => {
      expect(e.type).toBe(Adapter.Event.CLICK)
      done()
    })

    adapter.fireStart()
    adapter.fireClick()
  })

  it('should /adClick with url', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.CLICK, (e) => {
      expect(e.type).toBe(Adapter.Event.CLICK)
      expect(e.data.params.url).toBe('fakeurl.com')
      done()
    })

    adapter.fireStart()
    adapter.fireClick('fakeurl.com')
  })

  it('should /adQuartile', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.QUARTILE, (e) => {
      expect(e.type).toBe(Adapter.Event.QUARTILE)
      done()
    })

    adapter.fireStart()
    adapter.fireQuartile(2)
  })

  it('should /adStop with skip', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })

    adapter.fireStart()
    adapter.fireSkip()
  })

  it('should /adBreakStart and /adBreakStop', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      isBreakStarted: false,
      _isStopReady: function () { return true }
    }
    adapter.on(Adapter.Event.PODSTART, (e) => {
      expect(e.type).toBe(Adapter.Event.PODSTART)
    })
    adapter.on(Adapter.Event.PODSTOP, (e) => {
      expect(e.type).toBe(Adapter.Event.PODSTOP)
      done()
    })

    adapter.fireStart()
    adapter.fireBreakStart()
    adapter.fireStop()
    adapter.fireBreakStop()
  })

  it('should /adManifest manually with error code', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.MANIFEST, (e) => {
      expect(e.type).toBe(Adapter.Event.MANIFEST)
      expect(e.data.params.errorType).toBe('errorcode')
      expect(e.data.params.errorMessage).toBe('errormsg')
      done()
    })
    adapter.fireManifest('errorcode', 'errormsg')
  })

  it('should /adStop when calling fireSkip, no params', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      expect(e.data.params.skipped).toBe(true)
      done()
    })
    adapter.fireStart()
    adapter.fireSkip()
  })

  it('should /adStop when calling fireSkip, additional params', (done) => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      _isStopReady: function () {
        return true
      }
    }
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      expect(e.data.params.key).toBe('value')
      done()
    })
    adapter.fireStart()
    adapter.fireSkip({ 'key': 'value' })
  })

  it('should /event with right params', (done) => {
    var adapter = new Adapter(true)
    var dimensions = {
      'dim1': 'a',
      'dim2': 'b'
    }
    var values = {
      'val1': 1,
      'val2': 2
    }
    var tldimensions = {
      'tl1': 1,
      'tl2': 2
    }
    var name = 'eventCustomName'
    adapter.on(Adapter.Event.VIDEO_EVENT, (e) => {
      expect(e.type).toBe(Adapter.Event.VIDEO_EVENT)
      expect(e.data.params.name).toBe(name)
      expect(e.data.params.dimensions).toBe(dimensions)
      expect(e.data.params.values).toBe(values)
      expect(e.data.params.tl1).toBe(1)
      expect(e.data.params.tl2).toBe(2)
      done()
    })
    adapter.fireEvent(name, dimensions, values, tldimensions)
  })

  it('should /event with default params', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.VIDEO_EVENT, (e) => {
      expect(e.type).toBe(Adapter.Event.VIDEO_EVENT)
      expect(e.data.params.name).toBe('')
      expect(e.data.params.dimensions).toEqual({})
      expect(e.data.params.values).toEqual({})
      done()
    })
    adapter.fireEvent()
  })

  it('should not /start if in background', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      backgroundDetector: {
        canBlockStartCalls: function () { return true }
      }
    }
    spyOn(adapter, 'emit')
    adapter.fireStart()
    expect(adapter.flags.isStarted).toBe(false)
    expect(adapter.emit).not.toHaveBeenCalled()
  })

  it('should /start when calling /join, if init was sent, and set the monitor', () => {
    var adapter = new Adapter(true)
    adapter.plugin = {
      isInitiated: true,
      backgroundDetector: {
        canBlockStartCalls: function () { return true }
      }
    }
    spyOn(adapter, 'fireStart')
    adapter.fireJoin()
    expect(adapter.fireStart).toHaveBeenCalled()
  })
})
