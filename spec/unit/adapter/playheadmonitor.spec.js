describe('PlayheadMonitor', () => {
  var PlayheadMonitor = require('../../../src/adapter/playheadmonitor.js')

  describe('seek-detector', () => {
    var adapter
    var pm
    beforeEach((done) => {
      adapter = {
        playhead: 0,
        getPlayhead: function () { return this.playhead },
        on: function (e) { },
        flags: {},
        fireSeekBegin: function (e) {
          pm.stop()
          clearInterval(int)
          done()
        },
        fireSeekEnd: function () { },
        fireBufferBegin: function () { },
        fireBufferEnd: function () { }
      }

      spyOn(adapter, 'fireSeekBegin').and.callThrough()

      pm = new PlayheadMonitor(adapter, PlayheadMonitor.Type.SEEK, 200)
      pm.canBeUsed = function () { return true }
      pm.start()

      var int = setInterval(() => {
        adapter.playhead += 300
      }, 100)
    })

    it('should detect seeks', () => {
      expect(adapter.fireSeekBegin).toHaveBeenCalled()
    })

    it('should skip tick', () => {
      pm.skipNextTick()
      expect(pm._lastPlayhead).toBe(0)
    })
  })

  describe('buffer-detector', () => {
    var adapter
    var pm
    beforeEach((done) => {
      adapter = {
        playhead: 1,
        getPlayhead: function () { return this.playhead },
        getPlayrate: function () { return 1.1 },
        on: function (e) { },
        flags: {},
        fireSeekBegin: function () { },
        fireSeekEnd: function () { },
        fireBufferBegin: function (e) {
          pm.stop()
          done()
        },
        fireBufferEnd: function () { }
      }

      spyOn(adapter, 'fireBufferBegin').and.callThrough()

      pm = new PlayheadMonitor(adapter, PlayheadMonitor.Type.BUFFER, 200)
      pm.canBeUsed = function () { return true }
      pm.start()
    })

    it('should detect buffers', () => {
      expect(adapter.fireBufferBegin).toHaveBeenCalled()
    })
  })
})

describe('monitor disable option', () => {
  var PlayheadMonitor = require('../../../src/adapter/playheadmonitor.js')
  it('should selective start, vod and enabled', () => {
    var adapter = {
      playhead: 0,
      getPlayhead: function () { return this.playhead },
      plugin: {
        getIsLive: function () {
          return false
        },
        getPlayheadMonitorEnabled: function () { return true },
        options: {
          'content.isLive.noMonitor': true
        }
      }
    }
    var pm = new PlayheadMonitor(adapter, PlayheadMonitor.Type.SEEK, 200)
    spyOn(pm._timer, 'start').and.callThrough()
    pm.start()
    expect(pm._timer.start).toHaveBeenCalled()
  })

  it('should selective start, live and enabled', () => {
    var adapter = {
      playhead: 0,
      getPlayhead: function () { return this.playhead },
      plugin: {
        getIsLive: function () {
          return true
        },
        getPlayheadMonitorEnabled: function () { return true },
        options: {
          'content.isLive.noMonitor': false
        }
      }
    }
    var pm = new PlayheadMonitor(adapter, PlayheadMonitor.Type.SEEK, 200)
    spyOn(pm._timer, 'start').and.callThrough()
    pm.start()
    expect(pm._timer.start).toHaveBeenCalled()
  })

  it('should selective start, live and disabled', () => {
    var adapter = {
      playhead: 0,
      getPlayhead: function () { return this.playhead },
      plugin: {
        getIsLive: function () {
          return true
        },
        getPlayheadMonitorEnabled: function () { return true },
        options: {
          'content.isLive.noMonitor': true
        }
      }
    }
    var pm = new PlayheadMonitor(adapter, PlayheadMonitor.Type.SEEK, 200)
    spyOn(pm._timer, 'start').and.callThrough()
    pm.start()
    expect(pm._timer.start).not.toHaveBeenCalled()
  })
})
