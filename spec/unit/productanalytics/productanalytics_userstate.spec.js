describe('ProductAnalyticsUserState', () => {
  var ProductAnalyticsUserState = require('../../../src/productanalytics/productanalytics_userstate')
  var userState

  beforeEach(() => {
  })

  // ------------------------------------------------------------------------------------------------------------------------------
  // CONSTRUCTOR
  // ------------------------------------------------------------------------------------------------------------------------------

  it('initializes attributes to default values when no settings are supplied to the constructor', () => {
    userState = new ProductAnalyticsUserState()
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    expect(userState._fireAdapterEvent).toEqual(null)
    expect(userState._pluginOptionsHandler).toEqual(null)
    expect(userState._dimension).toEqual('content.customDimension.9')
    expect(userState._timerId).toEqual(null)
    expect(userState._timerInterval).toEqual(30000)
    expect(userState._started).toEqual(false)
  })

  // ------------------------------------------------------------------------------------------------------------------------------
  // STATE
  // ------------------------------------------------------------------------------------------------------------------------------

  it('does not start passive timer when state is set to passive for the first time', () => {
    userState = new ProductAnalyticsUserState()
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    userState.setActive('start')
    expect(userState._timerId).not.toEqual(null)
  })

  it('expects state to transition to PASSIVE after 30 seconds', () => {
    jasmine.clock().install()

    userState = new ProductAnalyticsUserState()
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    userState.setActive('start')
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)
    jasmine.clock().tick(userState._timerInterval + 500)
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)

    jasmine.clock().uninstall()
  })

  it('expects _fireAdapterEvent to be called upon a state change', () => {
    jasmine.clock().install()

    userState = new ProductAnalyticsUserState(9, 30000, function(){}, function(){})
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    userState.setActive('start')
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)

    spyOn(userState, '_fireAdapterEvent')
    jasmine.clock().tick(userState._timerInterval + 500)
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    expect(userState._fireAdapterEvent).toHaveBeenCalled()

    jasmine.clock().uninstall()
  })

  it('expects state to become ACTIVE after a setActive call triggered while in PASSIVE state', () => {
    jasmine.clock().install()

    userState = new ProductAnalyticsUserState(9, 30000, function(){}, function(){})
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    userState.setActive('start')
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)

    spyOn(userState, '_fireAdapterEvent')
    jasmine.clock().tick(userState._timerInterval + 500)
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    expect(userState._fireAdapterEvent).toHaveBeenCalled()

    userState.setActive('start')
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)

    jasmine.clock().uninstall()
  })

  it('expects timer to be released after calling dispose', () => {
    jasmine.clock().install()

    userState = new ProductAnalyticsUserState(9, 30000, function(){}, function(){})
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.PASSIVE)
    userState.setActive('start')
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)

    userState.dispose()

    spyOn(userState, '_fireAdapterEvent')
    jasmine.clock().tick(userState._timerInterval + 500)
    expect(userState._state).toEqual(ProductAnalyticsUserState.States.ACTIVE)
    expect(userState._fireAdapterEvent).not.toHaveBeenCalled()

    expect(userState._started).toEqual(false)
    expect(userState._timerId).toEqual(null)

    jasmine.clock().uninstall()
  })
})
