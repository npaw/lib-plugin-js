describe('ProductAnalytics', () => {
  var ProductAnalytics = require('../../../src/productanalytics/productanalytics')
  var Log = require('../../../src/log')

  var productAnalytics

  beforeEach(() => {
  })

  // -----------------------------------------------------------------------------------------------
  // CONSTRUCTOR
  // -----------------------------------------------------------------------------------------------

  it('does not fail when no plugin is supplied to constructor', () => {
    productAnalytics = new ProductAnalytics()
    expect(productAnalytics).toBeDefined()
  })

  it('does not fail when window.location is undefined', () => {

    var windowBackup = window

    window = {}

    productAnalytics = new ProductAnalytics()
    expect(productAnalytics).toBeDefined()

    window = windowBackup
  })

  // -----------------------------------------------------------------------------------------------
  // INITIALIZE
  // -----------------------------------------------------------------------------------------------

  it('initializes settings to default when no settings are supplied', () => {
    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize()
    expect(productAnalytics._productAnalyticsSettings.autoTrackNavigation).toBe(productAnalytics._productAnalyticsSettingsDefault.autoTrackNavigation)
    expect(productAnalytics._productAnalyticsSettings.autoTrackAttribution).toBe(productAnalytics._productAnalyticsSettingsDefault.autoTrackAttribution)
    expect(productAnalytics._productAnalyticsSettings.highlightContentAfter).toBe(productAnalytics._productAnalyticsSettingsDefault.highlightContentAfter)
    expect(productAnalytics._productAnalyticsSettings.enableStateTracking).toBe(productAnalytics._productAnalyticsSettingsDefault.enableStateTracking)
    expect(productAnalytics._productAnalyticsSettings.activeStateTimeout).toBe(productAnalytics._productAnalyticsSettingsDefault.activeStateTimeout)
    expect(productAnalytics._productAnalyticsSettings.activeStateDimension).toBe(productAnalytics._productAnalyticsSettingsDefault.activeStateDimension)
    expect(productAnalytics._productAnalyticsSettings.pageRules).toBe(productAnalytics._productAnalyticsSettingsDefault.pageRules)
  })

  it('initializes overwrites default settings', () => {

    var rules = [{rule: '^URL$', page: 'Test'}]

    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize({
      autoTrackNavigation: false,
      autoTrackAttribution: false,
      highlightContentAfter: 3000,
      enableStateTracking: true,
      activeStateTimeout: 10000,
      activeStateDimension: 5,
      pageRules: rules
      })

    expect(productAnalytics._productAnalyticsSettings.autoTrackNavigation).toBe(false)
    expect(productAnalytics._productAnalyticsSettings.autoTrackAttribution).toBe(false)
    expect(productAnalytics._productAnalyticsSettings.highlightContentAfter).toBe(3000)
    expect(productAnalytics._productAnalyticsSettings.enableStateTracking).toBe(true)
    expect(productAnalytics._productAnalyticsSettings.activeStateTimeout).toBe(10000)
    expect(productAnalytics._productAnalyticsSettings.activeStateDimension).toBe(5)
    expect(productAnalytics._productAnalyticsSettings.pageRules).toBe(rules)
  })

  it('sets default setting values when invalid settings are supplied', () => {

    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize({
      autoTrackNavigation: 'false',
      autoTrackAttribution: 'false',
      highlightContentAfter: 300,
      enableStateTracking: 'true',
      activeStateTimeout: '10000',
      activeStateDimension: '5',
      pageRules: null
      })

    expect(productAnalytics._productAnalyticsSettings.autoTrackNavigation).toBe(productAnalytics._productAnalyticsSettingsDefault.autoTrackNavigation)
    expect(productAnalytics._productAnalyticsSettings.autoTrackAttribution).toBe(productAnalytics._productAnalyticsSettingsDefault.autoTrackAttribution)
    expect(productAnalytics._productAnalyticsSettings.highlightContentAfter).toBe(productAnalytics._productAnalyticsSettingsDefault.highlightContentAfter)
    expect(productAnalytics._productAnalyticsSettings.enableStateTracking).toBe(productAnalytics._productAnalyticsSettingsDefault.enableStateTracking)
    expect(productAnalytics._productAnalyticsSettings.activeStateTimeout).toBe(productAnalytics._productAnalyticsSettingsDefault.activeStateTimeout)
    expect(productAnalytics._productAnalyticsSettings.activeStateDimension).toBe(productAnalytics._productAnalyticsSettingsDefault.activeStateDimension)
    expect(productAnalytics._productAnalyticsSettings.pageRules).toEqual(productAnalytics._productAnalyticsSettingsDefault.pageRules)
  })

  it('should identify pages properly', () => {

    var rules = [{rule: '\/movies$', page: 'Movies'}]

    productAnalytics = new ProductAnalytics()
    productAnalytics._url = new URL('https://test.com/movies')

    productAnalytics.initialize({
      autoTrackAttribution: false,
      autoTrackNavigation: false,
      pageRules: rules
      })

    expect(productAnalytics._page).toEqual(rules[0].page)
  })

  it('should identify pages properly even if a rule is not properly constructed', () => {

    var rules = [{rule: '\/(ms$', page: 'Movies'}, {rule: '\/movies$', page: 'Movies'}]

    productAnalytics = new ProductAnalytics()
    productAnalytics._url = new URL('https://test.com/movies')

    productAnalytics.initialize({
      autoTrackAttribution: false,
      autoTrackNavigation: false,
      pageRules: rules
      })

    expect(productAnalytics._page).toEqual(rules[0].page)
  })

  // -----------------------------------------------------------------------------------------------
  // ADAPTER
  // -----------------------------------------------------------------------------------------------

  it('does not initialize userState when plugin is unset', () => {
    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize()
    productAnalytics.adapterAfterSet()
    expect(productAnalytics._userState).toBe(null)
  })

  it('does not initialize userState when plugin.setOptions is unavailable', () => {
    productAnalytics = new ProductAnalytics({})
    productAnalytics.initialize()
    productAnalytics.adapterAfterSet()
    expect(productAnalytics._userState).toBe(null)
  })

  it('does initialize userState when plugin.setOptions is available', () => {
    productAnalytics = new ProductAnalytics({setOptions: function(){}})
    productAnalytics.initialize({enableStateTracking: true})
    productAnalytics.adapterAfterSet()
    expect(productAnalytics._userState).not.toBe(null)
  })

  it('removes userState on adapterBeforeRemove', () => {
    productAnalytics = new ProductAnalytics({setOptions: function(){}})
    productAnalytics.initialize()
    productAnalytics.adapterBeforeRemove()
    expect(productAnalytics._userState).toBe(null)
  })

  // -----------------------------------------------------------------------------------------------
  // SESSION
  // -----------------------------------------------------------------------------------------------

  it('should not newSession when product analytics is uninitialized', () => {
    spyOn(Log, 'warn')

    productAnalytics = new ProductAnalytics()
    productAnalytics.newSession()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot start a new session since Product Analytics is uninitialized.')
  })

  it('should not newSession when infinity is unavailable', () => {
    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize()
    expect(productAnalytics.newSession()).toEqual(false)
  })

  it('should not endSession when product analytics is uninitialized', () => {

    spyOn(Log, 'warn')

    productAnalytics = new ProductAnalytics()
    productAnalytics.endSession()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot end session since Product Analytics is uninitialized.')
  })

  it('should not endSession when infinity is unavailable', () => {
    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize()
    expect(productAnalytics.endSession()).toEqual(false)
  })

  // -----------------------------------------------------------------------------------------------
  // LOGIN / LOGOUT
  // -----------------------------------------------------------------------------------------------

  it('should not loginSuccessful when product analytics is uninitialized / infinity is unavailable', () => {
    var userId = '000001'

    setupTest('_fireEvent')

    productAnalytics.loginSuccessful(userId)
    checkUninitialized(productAnalytics._fireEvent, 'log in successfully')

    productAnalytics.initialize()
    productAnalytics.loginSuccessful(userId)
    checkInfinity(productAnalytics._fireEvent, 'log in successfully')
  })

  it('should not loginUnsuccessful when product analytics is uninitialized / infinity is unavailable', () => {

    setupTest('_fireEvent')

    productAnalytics.loginUnsuccessful()
    checkUninitialized(productAnalytics._fireEvent, 'log in unsuccessful')

    productAnalytics.initialize()
    productAnalytics.loginUnsuccessful()
    checkInfinity(productAnalytics._fireEvent, 'log in unsuccessful')
  })

  it('should not logout when product analytics is uninitialized / infinity is unavailable', () => {

    productAnalytics = new ProductAnalytics()

    spyOn(Log, 'warn')
    productAnalytics.logout()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot log out since Product Analytics is uninitialized.')

    productAnalytics.initialize()
    productAnalytics.logout()
    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot log out since infinity is unavailable.')
  })

  // -----------------------------------------------------------------------------------------------
  // PROFILE
  // -----------------------------------------------------------------------------------------------

  it('should not userProfileCreated when product analytics is uninitialized / infinity is unavailable', () => {
    var profileId = '000009'

    setupTest('_fireEvent')

    productAnalytics.userProfileCreated(profileId)
    checkUninitialized(productAnalytics._fireEvent, 'create user profile')

    productAnalytics.initialize()
    productAnalytics.userProfileCreated(profileId)
    checkInfinity(productAnalytics._fireEvent, 'create user profile')
  })

  it('should not userProfileSelected when product analytics is uninitialized / infinity is unavailable', () => {
    var profileId = '000009'

    setupTest('_fireEvent')

    productAnalytics.userProfileSelected(profileId)
    checkUninitialized(productAnalytics._fireEvent, 'select user profile')

    productAnalytics.initialize()
    productAnalytics.userProfileSelected(profileId)
    checkInfinity(productAnalytics._fireEvent, 'select user profile')
  })

  it('should not userProfileDeleted when product analytics is uninitialized / infinity is unavailable', () => {

    setupTest('_fireEvent')

    productAnalytics.userProfileDeleted()
    checkUninitialized(productAnalytics._fireEvent, 'delete user profile')

    productAnalytics.initialize()
    productAnalytics.userProfileDeleted()
    checkInfinity(productAnalytics._fireEvent, 'delete user profile')
  })

  // -----------------------------------------------------------------------------------------------
  // NAVIGATION
  // -----------------------------------------------------------------------------------------------

  it('should not trackNavByName when product analytics is uninitialized / infinity is unavailable', () => {
    var page = 'Home'

    setupTest('_trackNavigation')

    productAnalytics.trackNavByName(page)
    checkUninitialized(productAnalytics._trackNavigation, 'track navigation')

    productAnalytics.initialize({autoTrackNavigation: false})
    productAnalytics.trackNavByName(page)
    checkInfinity(productAnalytics._trackNavigation, 'track navigation')
  })

  it('should not trackNavByRoute when product analytics is uninitialized  / infinity is unavailable', () => {
    var route = 'https://test.com/movies'

    setupTest('_trackNavigation')

    productAnalytics.trackNavByRoute(route)
    checkUninitialized(productAnalytics._trackNavigation, 'track navigation')

    productAnalytics.initialize({autoTrackNavigation: false})
    productAnalytics.trackNavByRoute(route)
    checkInfinity(productAnalytics._trackNavigation, 'track navigation')
  })

  // -----------------------------------------------------------------------------------------------
  // ATTRIBUTION
  // -----------------------------------------------------------------------------------------------

  it('should not trackAttribution when product analytics is uninitialized', () => {
    var utmSource = 'source'
    var utmMedium = 'medium'
    var utmCampaign = 'campaign'
    var utmTerm = 'term'
    var utmContent = 'content'

    setupTest('_trackAttribution')

    productAnalytics.trackAttribution(utmSource, utmMedium, utmCampaign, utmTerm, utmContent)
    checkUninitialized(productAnalytics._trackAttribution, 'track attribution')

    productAnalytics.initialize({autoTrackAttribution: false})
    productAnalytics.trackAttribution(utmSource, utmMedium, utmCampaign, utmTerm, utmContent)
    checkInfinity(productAnalytics._trackAttribution, 'track attribution')
  })

  // -----------------------------------------------------------------------------------------------
  // SECTION
  // -----------------------------------------------------------------------------------------------

  it('should not trackSectionVisible when product analytics is uninitialized', () => {
    var section = 'featured'
    var sectionOrder = 1

    setupTest('_fireEvent')

    productAnalytics.trackSectionVisible(section, sectionOrder)
    checkUninitialized(productAnalytics._fireEvent, 'track section visible')

    productAnalytics.initialize()
    productAnalytics.trackSectionVisible(section, sectionOrder)
    checkInfinity(productAnalytics._fireEvent, 'track section visible')
  })

  it('should not trackSectionHidden when product analytics is uninitialized', () => {
    var section = 'featured'
    var sectionOrder = 1

    setupTest('_fireEvent')

    productAnalytics.trackSectionHidden(section, sectionOrder)
    checkUninitialized(productAnalytics._fireEvent, 'track section hidden')

    productAnalytics.initialize()
    productAnalytics.trackSectionHidden(section, sectionOrder)
    checkInfinity(productAnalytics._fireEvent, 'track section hidden')
  })

  // -----------------------------------------------------------------------------------------------
  // CONTENT
  // -----------------------------------------------------------------------------------------------

  it('should not contentFocusIn when product analytics is uninitialized', () => {
    var section = 'featured'
    var sectionOrder = 1
    var column = 5
    var row = 3
    var contentId = 'contentCode'

    setupTest('_fireEvent')

    productAnalytics.contentFocusIn(section, sectionOrder, column, row, contentId)
    checkUninitialized(productAnalytics._fireEvent, 'track content highlight')

    productAnalytics.initialize()
    productAnalytics.contentFocusIn(section, sectionOrder, column, row, contentId)
    checkInfinity(productAnalytics._fireEvent, 'track content highlight')
  })

  it('should not trackContentClick when product analytics is uninitialized', () => {
    var section = 'featured'
    var sectionOrder = 1
    var column = 5
    var row = 3
    var contentId = 'contentCode'

    setupTest('_fireEvent')

    productAnalytics.trackContentClick(section, sectionOrder, column, row, contentId)
    checkUninitialized(productAnalytics._fireEvent, 'track content click')

    productAnalytics.initialize()
    productAnalytics.trackContentClick(section, sectionOrder, column, row, contentId)
    checkInfinity(productAnalytics._fireEvent, 'track content click')
  })

  // -----------------------------------------------------------------------------------------------
  // CONTENT PLAYBACK
  // -----------------------------------------------------------------------------------------------

  it('should not trackPlay when product analytics is uninitialized', () => {
    var contentId = 'contentCode'

    setupTest('_fireAdapterEvent')

    productAnalytics.trackPlay(contentId)
    checkUninitialized(productAnalytics._fireAdapterEvent, 'track play')

    productAnalytics.initialize()
    productAnalytics.trackPlay(contentId)
    checkInfinity(productAnalytics._fireAdapterEvent, 'track play')
  })

  it('should not trackPlayerInteraction when product analytics is uninitialized', () => {
    var eventName = 'pause'

    setupTest('_fireAdapterEvent')

    productAnalytics.trackPlayerInteraction(eventName)
    checkUninitialized(productAnalytics._fireAdapterEvent, 'track player interaction')

    productAnalytics.initialize()
    productAnalytics.trackPlayerInteraction(eventName)
    checkInfinity(productAnalytics._fireAdapterEvent, 'track player interaction')
  })

  // -----------------------------------------------------------------------------------------------
  // CONTENT SEARCH
  // -----------------------------------------------------------------------------------------------

  it('should not trackSearchQuery when product analytics is uninitialized', () => {
    var query = 'search query'

    setupTest('_fireEvent')

    productAnalytics.trackSearchQuery(query)
    checkUninitialized(productAnalytics._fireEvent, 'track search query')

    productAnalytics.initialize()
    productAnalytics.trackSearchQuery(query)
    checkInfinity(productAnalytics._fireEvent, 'track search query')
  })

  it('should not trackSearchResult when product analytics is uninitialized', () => {
    var resultCount = 5

    setupTest('_fireEvent')

    productAnalytics.trackSearchResult(resultCount)
    checkUninitialized(productAnalytics._fireEvent, 'track search result')

    productAnalytics.initialize()
    productAnalytics.trackSearchResult(resultCount)
    checkInfinity(productAnalytics._fireEvent, 'track search result')
  })

  it('should not trackSearchClick when product analytics is uninitialized', () => {
    var section = 'Search Section'
    var sectionOrder = 3
    var contentId = 'contentCode'
    var column = 5
    var row = 3

    setupTest('_fireEvent')

    productAnalytics.trackSearchClick(section, sectionOrder, column, row, contentId)
    checkUninitialized(productAnalytics._fireEvent, 'track search click')

    productAnalytics.initialize()
    productAnalytics.trackSearchClick(section, sectionOrder, column, row, contentId)
    checkInfinity(productAnalytics._fireEvent, 'track search click')
  })

  // -----------------------------------------------------------------------------------------------
  // EXTERNAL APPLICATIONS
  // -----------------------------------------------------------------------------------------------

  it('should not trackExternalAppLaunch when product analytics is uninitialized', () => {
    var appName = 'Application Name'

    setupTest('_fireEvent')

    productAnalytics.trackExternalAppLaunch(appName)
    checkUninitialized(productAnalytics._fireEvent, 'track external application launch')

    productAnalytics.initialize()
    productAnalytics.trackExternalAppLaunch(appName)
    checkInfinity(productAnalytics._fireEvent, 'track external application launch')
  })

  it('should not trackExternalAppExit when product analytics is uninitialized', () => {
    var appName = 'Application Name'

    setupTest('_fireEvent')

    productAnalytics.trackExternalAppExit(appName)
    checkUninitialized(productAnalytics._fireEvent, 'track external application exit')

    productAnalytics.initialize()
    productAnalytics.trackExternalAppExit()
    checkInfinity(productAnalytics._fireEvent, 'track external application exit')
  })

  // -----------------------------------------------------------------------------------------------
  // ENGAGEMENT
  // -----------------------------------------------------------------------------------------------

  it('should not trackEngagementEvent when product analytics is uninitialized', () => {
    var eventName = 'Event Name'
    var contentId = 'contentCode'

    setupTest('_fireEvent')

    productAnalytics.trackEngagementEvent(eventName, contentId)
    checkUninitialized(productAnalytics._fireEvent, 'track engagement event')

    productAnalytics.initialize()
    productAnalytics.trackEngagementEvent(eventName, contentId)
    checkInfinity(productAnalytics._fireEvent, 'track engagement event')
  })

  // -----------------------------------------------------------------------------------------------
  // CUSTOM EVENT
  // -----------------------------------------------------------------------------------------------

  it('should not trackEvent when product analytics is uninitialized', () => {
    var eventName = 'Event Name'

    setupTest('_fireEvent')

    productAnalytics.trackEvent(eventName)
    checkUninitialized(productAnalytics._fireEvent, 'track custom event')

    productAnalytics.initialize()
    productAnalytics.trackEvent(eventName)
    checkInfinity(productAnalytics._fireEvent, 'track custom event')
  })

  // -----------------------------------------------------------------------------------------------
  // INTERNAL
  // -----------------------------------------------------------------------------------------------

  it('should not fire events when _infinity is undefined', () => {
    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize()
    expect(productAnalytics._fireEvent('eventName', {}, {}, {})).toEqual(false)
  })

  it('should not fire adapter events when _infinity is undefined', () => {
    productAnalytics = new ProductAnalytics()
    productAnalytics.initialize()
    expect(productAnalytics._fireAdapterEvent('eventName', {}, {}, {})).toEqual(false)
  })

  // -----------------------------------------------------------------------------------------------
  // HELPERS
  // -----------------------------------------------------------------------------------------------

  function setupTest(method){
    productAnalytics = new ProductAnalytics()

    spyOn(Log, 'warn')
    spyOn(productAnalytics, method)
  }

  function checkUninitialized(func, message){
    expect(func).not.toHaveBeenCalled()

    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot ' + message + ' since Product Analytics is uninitialized.')
  }

  function checkInfinity(func, message){
    expect(func).not.toHaveBeenCalled()

    expect(Log.warn).toHaveBeenCalled()
    expect(Log.warn).toHaveBeenCalledWith('Cannot ' + message + ' since infinity is unavailable.')
  }

})
