describe('ProductAnalyticsConfig', () => {
  var ProductAnalyticsConfig = require('../../../src/productanalytics/productanalytics_config')

  var productAnalyticsConfig
  var npawtmConfig = {
    'domains':['testapp.npaw.com'],
    'urls':{
      '/movies':{'page':'Movies'}},
    'regexes':[
      {regex: '\/moviez$', page: 'Moviez'}
    ],
    "events":{
      "Movies":[
        {'selector':'#top-menu > li > a[href=\"/movies\"]','name':'moviesss [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/\"]','name':'home [link]','events':[]},
        {'selector':'#top-menu > li > a[href=\"/tv-shows\"]','name':'tvshows [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/videoclub\"]','name':'videoclub [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/catchup-tv\"]','name':'catchuptv [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/live-channels\"]','name':'livechannels [link]','events':['click']}],
      "Moviez":[
        {'selector':'#top-menu > li > a[href=\"/movies\"]','name':'moviesss [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/\"]','name':'home [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/tv-shows\"]','name':'tvshows [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/videoclub\"]','name':'videoclub [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/catchup-tv\"]','name':'catchuptv [link]','events':['click']},
        {'selector':'#top-menu > li > a[href=\"/live-channels\"]','name':'livechannels [link]','events':['click']}],
      }}

  beforeEach(() => {
  })

  // ------------------------------------------------------------------------------------------------------------------------------
  // CONSTRUCTOR
  // ------------------------------------------------------------------------------------------------------------------------------

  it('properly sets attributes when no arguments are supplied to the constructor', () => {
    productAnalyticsConfig = new ProductAnalyticsConfig('function')
    expect(productAnalyticsConfig._fireEvent).toBe(null)
    expect(productAnalyticsConfig._npawtmConfig).toBe(null)
  })

  it('properly sets attributes when arguments are supplied to the constructor', () => {
    window.npawtmConfig = npawtmConfig
    productAnalyticsConfig = new ProductAnalyticsConfig(function(){})
    expect(productAnalyticsConfig._fireEvent).not.toBe(null)
    expect(productAnalyticsConfig._npawtmConfig).not.toBe(null)
  })

  // ------------------------------------------------------------------------------------------------------------------------------
  // IDENTIFY
  // ------------------------------------------------------------------------------------------------------------------------------

  it('properly identifies pages using direct match', () => {
    var l = new URL('https://testapp.npaw.com/movies')

    window.npawtmConfig = npawtmConfig
    productAnalyticsConfig = new ProductAnalyticsConfig(function(){})
    expect(productAnalyticsConfig.identify(l)).toBe('Movies')
  })

  it('properly identifies pages using regular expressions', () => {
    var l = new URL('https://testapp.npaw.com/moviez')

    window.npawtmConfig = npawtmConfig
    productAnalyticsConfig = new ProductAnalyticsConfig(function(){})
    expect(productAnalyticsConfig.identify(l)).toBe('Moviez')
  })

  it('properly bind events', function() {
    var events
    var page
    var html
    var l = new URL('https://testapp.npaw.com/movies')

    html = document.createElement("ul")
    html.id = "top-menu"
    html.innerHTML  = '<li><a href="/">Home</a></li>'
    html.innerHTML += '<li><a href="/movies">Movies</a></li>'
    document.body.appendChild(html)

    window.npawtmConfig = npawtmConfig
    productAnalyticsConfig = new ProductAnalyticsConfig(function(){})
    page = productAnalyticsConfig.identify(l)
    events = productAnalyticsConfig._getEvents(page)
    expect(function(){
      productAnalyticsConfig._bindElements(events)
    }).not.toThrow()

    document.body.removeChild(html)
  })

  // ------------------------------------------------------------------------------------------------------------------------------
  // ELEMENT EVENT
  // ------------------------------------------------------------------------------------------------------------------------------

  it('does fireEvent if a handler is supplied', function() {
    var events
    var page
    var l = new URL('https://testapp.npaw.com/movies')

    window.npawtmConfig = npawtmConfig
    productAnalyticsConfig = new ProductAnalyticsConfig(function(){})
    page = productAnalyticsConfig.identify(l)
    expect(page).toEqual('Movies')
    events = productAnalyticsConfig._getEvents(page)

    spyOn(productAnalyticsConfig, '_fireEvent')
    productAnalyticsConfig._elementEvent(events[0], 'click', {} )
    expect(productAnalyticsConfig._fireEvent).toHaveBeenCalled()
  })
})
