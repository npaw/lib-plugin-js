describe('HlsTransform', () => {
  var LocTransform = require('../../../../../../src/comm/transform/resourceparsers/formatparsers/locationheaderparser')
  var lht

  beforeEach(() => {
    lht = new LocTransform()
  })

  it('should accept to execute', () => {
    expect(lht.shouldExecute()).toBe(true)
  })

  it('should parse, no previous manifest', () => {
    lht.parse('initialResource')
    expect(lht.getResource()).toBe('initialResource')
  })

  it('should parse, with previous manifest', () => {
    spyOn(lht, '_parseWithManifest')
    lht.parse('initialResource', 'false manifest')
    expect(lht.getResource()).toBeNull()
    expect(lht._parseWithManifest).toHaveBeenCalled()
  })

  it('should parse, no location, gets nothing', () => {
    var manifest = {
      getResponseHeaders: function () {
        return 'this is not a loc - ation'
      }
    }
    lht._parseWithManifest(manifest)
    expect(lht.getResource()).toBeNull()
  })

  it('should parse, gets location,', () => {
    var manifest = {
      getResponseHeaders: function () {
        return 'Location: http://www.actualUrl.com'
      }
    }
    lht._parseWithManifest(manifest)
    expect(lht.getResource()).toBe('http://www.actualUrl.com')
  })
})
