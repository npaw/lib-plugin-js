/* global PerformanceObserver */
var YouboraObject = require('../object')
var RUMSpeedIndex = require('./speedIndex')

/**
 * This static class provides information about the load times of the page
 *
 * @constructs YouboraObject
 * @extends youbora.YouboraObject
 * @memberof youbora
 *
 */
var BrowserLoadTimes = YouboraObject.extend({
  constructor: function (plugin) {
    this.infinity = plugin.infinity
    this.timeObject = null
    this.playerSetup = null
    this.perfObject = null
    this.myTimesObject = {}

    if (typeof window !== 'undefined' && typeof window.addEventListener === 'function') {
      window.addEventListener('load', this._windowLoaded.bind(this))
      if (window.performance && window.performance.timing) {
        try {
          if (typeof window.performance.getEntriesByType === 'function') {
            this.perfObject = window.performance
          }
        } catch (err) {
          // Nothing
        }
        this.timeObject = window.performance.timing
      }
    }
    try {
      if (typeof PerformanceObserver === 'function') {
        var observer = new PerformanceObserver(function (list, obj) {
          var entries = list.getEntries()
          this.myTimesObject.largestContentfulPaint = entries[entries.length - 1].renderTime
        }.bind(this))
        if (PerformanceObserver.supportedEntryTypes.indexOf('largest-contentful-paint') > -1) {
          observer.observe({ entryTypes: ['largest-contentful-paint'] })
        }
      }
    } catch (err) {
      // Cant use PerformanceObserver, entryTypes...
    }
  },

  _windowLoaded: function () {
    this.myTimesObject.onLoad = new Date().getTime()
    this._getEnoughFPS()
    setTimeout(this._fireLoadTimesEvent.bind(this), 1000)
  },

  _fireLoadTimesEvent: function () {
    this._getLastMetrics()
    this.infinity.fireEvent('loadTimes', {}, this._getAllValues())
  },

  _getAllValues: function () {
    var ret = {
      // Time between the navigation starts and the page loadEventEnd
      PageLoadTime: this.getPageLoadTime(),
      // Domain lookup time
      DNSTime: this.getDnsTime(),
      // Connection time
      TCPTime: this.getTcpTime(),
      // Handshake (https) connection time
      HandshakeTime: this.getHandshakeTime(),
      // Time between navigation starts and DOM is ready
      DomReadyTime: this.getDomReadyTime(),
      // Time between navigation start and beginning of the backend response
      BackendTime: this.getBackendTime(),
      // Time between navigation starts and page load event
      FrontendTime: this.getFrontendTime(),
      // The maximum of First Paint / First Contentful Paint and domContentLoadedEventEnd
      VisualReady: this.getTimeToVisuallyReady(),
      // https://developer.mozilla.org/en-US/docs/Glossary/Time_to_interactive
      TimeToInteractive: this.getTimeToInteractive(),
      // Accumulated time to download all the JS files
      JsTime: this.getJSTime(),
      // Accumulated time to download all the CSS files
      CssTime: this.getCSSTime(),
      // Accumulated time to download all the images
      ImageTime: this.getImageTime(),
      // Accumulated time to download all the fonts
      FontTime: this.getFontTime(),
      // Average latency of all the file requests
      AvgReqLatency: this.getAvgReqLatency(),
      // Highest latency of all the file requests
      MaxReqLatency: this.getMaxReqLatency(),
      // https://developer.mozilla.org/en-US/docs/Glossary/First_paint
      FirstPaint: this.getFirstPaint(),
      // https://developer.mozilla.org/en-US/docs/Glossary/First_contentful_paint
      FirstContentfulPaint: this.getFirstContentfulPaint(),
      // https://rockcontent.com/blog/largest-contentful-paint/
      LargestContentfulPaint: this.getLargestContentfulPaint(),
      // https://developer.mozilla.org/en-US/docs/Glossary/Speed_index
      SpeedIndex: this.getSpeedIndex()
    }
    for (var key in ret) {
      if (ret[key] === null || ret[key] === undefined || ret[key] < 0) {
        delete ret[key]
      } else {
        ret[key] = Math.round(ret[key])
      }
    }
    return ret
  },

  // Getters

  getPageLoadTime: function () {
    var ret = null
    if (this.timeObject) {
      ret = this.timeObject.loadEventEnd - this.timeObject.navigationStart
    }
    return ret
  },

  getPlayerStartupTime: function () {
    var ret = null
    if (this.timeObject && this.playerSetup) {
      ret = this.playerSetup - this.timeObject.navigationStart
    }
    return ret
  },

  getDnsTime: function () {
    var ret = null
    if (this.timeObject) {
      ret = this.timeObject.domainLookupEnd - this.timeObject.domainLookupStart
    }
    return ret
  },

  getTcpTime: function () {
    var ret = null
    if (this.timeObject) {
      ret = this.timeObject.connectEnd - this.timeObject.connectStart
    }
    return ret
  },

  getHandshakeTime: function () {
    var ret = null
    if (this.timeObject && this.timeObject.secureConnectionStart) {
      ret = this.timeObject.connectEnd - this.timeObject.secureConnectionStart
    }
    return ret
  },

  getDomReadyTime: function () {
    var ret = null
    if (this.timeObject) {
      ret = this.timeObject.domComplete - this.timeObject.navigationStart
    }
    return ret
  },

  getBackendTime: function () {
    var ret = null
    if (this.timeObject) {
      ret = this.timeObject.responseStart - this.timeObject.navigationStart
    }
    return ret
  },

  getFrontendTime: function () {
    var ret = null
    if (this.timeObject) {
      ret = this.myTimesObject.onLoad - this.timeObject.responseStart
    }
    return ret
  },

  getTimeToVisuallyReady: function () {
    var ret = this.myTimesObject.firstPaint || 0
    if (this.timeObject) {
      return Math.max(
        ret,
        this.timeObject.domContentLoadedEventEnd - this.timeObject.navigationStart || 0
      // this.myTimesObject.heroImages - this.timeObject.navigationStart || 0
      )
    }
    return ret || null
  },

  getTimeToInteractive: function () {
    if (this.myTimesObject.fps && this.getTimeToVisuallyReady()) {
      return Math.max(this.myTimesObject.fps, this.getTimeToVisuallyReady())
    } else {
      setTimeout(function () { this.getTimeToInteractive() }.bind(this), 500)
    }
  },

  getJSTime: function () {
    return this._getXTime('script')
  },

  getCSSTime: function () {
    return this._getXTime('css')
  },

  getImageTime: function () {
    return this._getXTime('img')
  },

  getFontTime: function () {
    return this._getXTime('css', ['.woff', '.otf', '.ttf'])
  },

  getAvgReqLatency: function () {
    try {
      if (this.perfObject && typeof this.perfObject.getEntriesByType === 'function') {
        var count = 0
        var latency = 0
        var scripts = this.perfObject.getEntriesByType('resource')
        for (var i in scripts) {
          if (scripts[i].requestStart && scripts[i].responseStart) {
            latency += scripts[i].responseStart - scripts[i].requestStart
          }
          count++
        }
        return latency / count
      }
    } catch (err) {
      // Nothing
    }
    return null
  },

  getFirstPaint: function () {
    return this.myTimesObject ? this.myTimesObject.firstPaint : null
  },

  getFirstContentfulPaint: function () {
    return this.myTimesObject ? this.myTimesObject.firstContentfulPaint : null
  },

  getLargestContentfulPaint: function () {
    return this.myTimesObject ? this.myTimesObject.largestContentfulPaint : null
  },

  getMaxReqLatency: function () {
    try {
      if (this.perfObject && typeof this.perfObject.getEntriesByType === 'function') {
        var scripts = this.perfObject.getEntriesByType('resource')
        var latency = 0
        for (var i in scripts) {
          if (scripts[i].requestStart && scripts[i].responseStart) {
            latency = Math.max(latency, scripts[i].responseStart - scripts[i].requestStart)
          }
        }
        return latency
      }
    } catch (err) {
      // Nothing
    }
    return null
  },

  getSpeedIndex: function () {
    var ret = null
    if (typeof window !== 'undefined' && window.performance && typeof window.performance.getEntriesByType === 'function') {
      try {
        ret = RUMSpeedIndex()
      } catch (e) {
        // nothing
      }
    }
    return ret
  },

  _getXTime: function (type, validExtensions) {
    var ret = 0
    try {
      if (this.perfObject && typeof this.perfObject.getEntriesByType === 'function') {
        var scripts = this.perfObject.getEntriesByType('resource')
        for (var i in scripts) {
          if (scripts[i].initiatorType === type) {
            if (!validExtensions) {
              ret += scripts[i].duration
            } else {
              var valid = false
              for (var ext in validExtensions) {
                if (scripts[i].name.indexOf(validExtensions[ext] > 0)) {
                  valid = true
                }
              }
              if (valid) {
                ret += scripts[i].duration
              }
            }
          }
        }
      }
    } catch (err) {
    // Nothing
    }
    return Math.round(ret) || null
  },

  _getLastMetrics: function () {
    var firstPaint = null
    var contentfulPaint = null
    try {
      if (this.perfObject && typeof this.perfObject.getEntriesByType === 'function') {
        var entries = this.perfObject.getEntriesByType('paint')
        for (var i in entries) {
          if (entries[i].name === 'first-paint') {
            firstPaint = entries[i].startTime
          } else if (entries[i].name === 'first-contentful-paint') {
            contentfulPaint = entries[i].startTime
          }
        }
      }
    } catch (err) {
    // Nothing
    }
    // First paint
    if (!firstPaint && this.timeObject) {
      firstPaint = this.timeObject.msFirstPaint - this.timeObject.navigationStart
    }
    // TODO first paint for firefox
    this.myTimesObject.firstPaint = firstPaint
    // First contentful paint
    this.myTimesObject.firstContentfulPaint = contentfulPaint
    // Others
    this.getTimeToInteractive()
  },

  _getEnoughFPS: function () {
    if (this.timeObject && typeof window !== 'undefined') {
      var req = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function () {}
      this.preFPS = new Date().getTime()
      req(function () {
        var now = new Date().getTime()
        if (now < this.preFPS + 50) {
          this.myTimesObject.fps = now - this.timeObject.navigationStart
        } else {
          setTimeout(function () { return this._getEnoughFPS() }.bind(this), 50)
        }
      }.bind(this))
    } else {
      return true
    }
  },

  // Setters

  setPlayerSetupTime: function () {
    this.playerSetup = this.playerSetup || new Date().getTime()
  }
})

module.exports = BrowserLoadTimes
