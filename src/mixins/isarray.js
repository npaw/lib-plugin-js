/**
 * See Array.isArray.
 * @memberof youbora.Util
 */
module.exports = function (obj) {
  return Object.prototype.toString.call(obj) === '[object Array]'
}
