var Log = require('../log')
var Util = require('../util')
var Constants = require('../constants')

// This file is designed to add extra functionalities to Plugin class

var PluginAdsGettersMixin = {
  /**
   * Returns ads's PlayerVersion
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPlayerVersion: function () {
    return this._safeGetterAdsAdapter('getPlayerVersion') || ''
  },

  /**
   * Returns ad's position
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPosition: function () {
    var ret = Constants.AdPosition.Preroll
    if (this._adsAdapter) {
      try {
        var temporalRet = this._adsAdapter.getPosition()
        if (Constants.AdPosition.Preroll === temporalRet ||
          Constants.AdPosition.Midroll === temporalRet ||
          Constants.AdPosition.Postroll === temporalRet) {
          ret = temporalRet
        }
      } catch (err) {
        Log.warn('An error occured while calling getAdPosition', err)
      }
    }
    if (!ret && this._adapter) {
      ret = (this._adapter.flags.isJoined) ? Constants.AdPosition.Midroll : Constants.AdPosition.Preroll
    }
    return ret
  },

  getAdNumber: function () {
    return this.requestBuilder.lastSent.adNumber || 0
  },

  getAdNumberInBreak: function () {
    return this.requestBuilder.lastSent.adNumberInBreak || 0
  },

  getBreakNumber: function () {
    return this.requestBuilder.lastSent.breakNumber || 0
  },

  /**
   * Returns ad's AdPlayhead
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPlayhead: function () {
    var ret = this._safeGetterAdsAdapter('getPlayhead')
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns ad's AdDuration
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdDuration: function () {
    var ret = this._safeGetterAdsAdapter('getDuration', 'ad.duration')
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns ad's AdBitrate
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdBitrate: function () {
    var ret = this._safeGetterAdsAdapter('getBitrate')
    if (!ret || ret === -1) {
      ret = this.getWebkitAdBitrate()
    }
    return Util.parseNumber(ret, -1)
  },

  /**
   * Returns bitrate as per webkitVideoDecodedByteCount
   *
   * @param {Object} tag Video tag DOM reference.
   * @returns {number}
   *
   * @memberof youbora.Plugin.prototype
   */
  getWebkitAdBitrate: function () {
    if (this._adsAdapter && this._adsAdapter.tag && this._adsAdapter.tag.webkitVideoDecodedByteCount) {
      var bitrate = this._adsAdapter.tag.webkitVideoDecodedByteCount
      if (this._lastWebkitAdBitrate) {
        var delta = this._adsAdapter.tag.webkitVideoDecodedByteCount - this._lastWebkitAdBitrate
        bitrate = Math.round(((delta) / this.viewTransform.response.pingTime) * 8)
      }
      this._lastWebkitAdBitrate = this._adsAdapter.tag.webkitVideoDecodedByteCount
      return bitrate !== 0 ? bitrate : -1
    }
  },

  /**
   * Returns ad's AdTitle
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdTitle: function () {
    return this._safeGetterAdsAdapter('getTitle', 'ad.title')
  },

  /**
   * Returns ad's AdResource
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdResource: function () {
    return this._safeGetterAdsAdapter('getResource', 'ad.resource')
  },

  /**
   * Returns ad's campaign
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdCampaign: function () {
    return this._safeGetterAdsAdapter('getCampaign', 'ad.campaign')
  },

  /**
  * Returns ad's campaign
  *
  * @memberof youbora.Plugin.prototype
  */
  getAdCreativeId: function () {
    return this._safeGetterAdsAdapter('getCreativeId', 'ad.creativeId')
  },

  /**
  * Returns ad's provider
  *
  * @memberof youbora.Plugin.prototype
  */
  getAdProvider: function () {
    return this._safeGetterAdsAdapter('getProvider', 'ad.provider')
  },

  /**
   * Returns ads adapter getVersion or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdAdapterVersion: function () {
    return this._safeGetterAdsAdapter('getVersion')
  },

  /**
   * Returns ad's AdMetadata
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdMetadata: function () {
    return this.options['ad.metadata']
  },

  /**
   * Returns the type of ad insertion (clientside, serverside)
   * @returns {string} ad insertion type
   */
  getAdInsertionType: function () {
    return this._safeGetterAdsAdapter('getAdInsertionType')
  },

  getGivenBreaks: function () {
    return this._safeGetterAdsAdapter('getGivenBreaks', 'ad.givenBreaks')
  },

  getExpectedBreaks: function () {
    var ret = null
    var expBreaks = this.options['ad.expectedBreaks']
    var expPattern = this.options['ad.expectedPattern']
    if (expBreaks) {
      ret = expBreaks
    } else if (expPattern) {
      ret = 0
      ret = expPattern.pre ? expPattern.pre.length : 0
      ret += expPattern.mid ? expPattern.mid.length : 0
      ret += expPattern.post ? expPattern.post.length : 0
    } else if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getExpectedBreaks()
      } catch (err) {
        Log.warn('An error occured while calling expectedBreaks', err)
      }
    }
    return ret
  },

  getExpectedPattern: function () {
    return this._safeGetterAdsAdapter('getExpectedPattern', 'ad.expectedPattern')
  },

  getBreaksTime: function () {
    return this._safeGetterAdsAdapter('getBreaksTime', 'ad.breaksTime')
  },

  getGivenAds: function () {
    return this._safeGetterAdsAdapter('getGivenAds', 'ad.givenAds')
  },

  getExpectedAds: function () {
    var ret = null
    try {
      if (this._adsAdapter) {
        var pattern = this.options['ad.expectedPattern']
        if (pattern && this.getAdPosition()) {
          var list = []
          if (pattern.pre) list = list.concat(pattern.pre)
          if (pattern.mid) list = list.concat(pattern.mid)
          if (pattern.post) list = list.concat(pattern.post)
          if (list.length > 0) {
            var position = this.requestBuilder.lastSent.breakNumber
            if (position > list.length) position = list.length
            ret = list[position - 1]
          }
        } else {
          ret = this._adsAdapter.getExpectedAds()
        }
      }
    } catch (err) {
      Log.warn('An error occured while calling expectedAds', err)
    }
    return ret
  },

  getAdsExpected: function () {
    var ret = null
    try {
      ret = (this.getExpectedPattern() || this.getGivenAds()) || false
    } catch (err) {
      Log.warn('An error occured while calling givenAds or expectedPattern', err)
    }
    return ret
  },

  // ----------------------------------------- CHRONOS ------------------------------------------

  /**
   * Returns AdJoinDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdJoinDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.join.getDeltaTime(false) : -1
  },

  /**
   * Returns AdBufferDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdBufferDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.buffer.getDeltaTime(false) : -1
  },

  /**
   * Returns AdPauseDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPauseDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.pause.getDeltaTime(false) : 0
  },

  /**
   * Returns total totalAdDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdTotalDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.total.getDeltaTime(false) : -1
  },

  getAdViewedDuration: function () {
    return this._getTimeMaxOrAcum(true)
  },

  getAdViewability: function () {
    return this._getTimeMaxOrAcum()
  },

  _getTimeMaxOrAcum: function (acum) {
    var maxTime = 0
    if (this._adsAdapter) {
      this._adsAdapter.chronos.viewedMax.forEach(function (chrono) {
        if (acum) {
          maxTime += chrono.getDeltaTime(false)
        } else {
          maxTime = Math.max(chrono.getDeltaTime(false), maxTime)
        }
      })
    }
    return maxTime
  },

  getAudioEnabled: function () {
    return this._safeGetterAdsAdapter('getAudioEnabled')
  },

  getIsSkippable: function () {
    return this._safeGetterAdsAdapter('getIsSkippable')
  },

  getIsFullscreen: function () {
    return this._safeGetterAdsAdapter('getIsFullscreen')
  },

  _safeGetterAdsAdapter: function (func, option) {
    var ret = null
    if (option && this.options[option]) {
      ret = this.options[option]
    } else {
      try {
        if (this._adsAdapter && typeof this._adsAdapter[func] === 'function') {
          ret = this._adsAdapter[func]()
        }
      } catch (err) {
        Log.warn('An error occured while calling ' + func, err)
      }
    }
    return ret
  }
}

module.exports = PluginAdsGettersMixin
