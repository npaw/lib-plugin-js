var YouboraObject = require('../object')
var Log = require('../log')

var startParams = [
  'accountCode',
  'username',
  'anonymousUser',
  'profileId',
  'rendition',
  'deviceInfo',
  'player',
  'title',
  'title2',
  'live',
  'segmentDuration',
  'mediaDuration',
  'mediaResource',
  'parsedResource',
  'transactionCode',
  'properties',
  'cdn',
  'playerVersion',
  'param1',
  'param2',
  'param3',
  'param4',
  'param5',
  'param6',
  'param7',
  'param8',
  'param9',
  'param10',
  'param11',
  'param12',
  'param13',
  'param14',
  'param15',
  'param16',
  'param17',
  'param18',
  'param19',
  'param20',
  'dimensions',
  'playerStartupTime',
  'obfuscateIp',
  'privacyProtocol',
  'p2pEnabled',
  'pluginVersion',
  'pluginInfo',
  'isp',
  'connectionType',
  'ip',
  'referer',
  'userType',
  'streamingProtocol',
  'transportFormat',
  'householdId',
  'adsBlocked',
  'adsExpected',
  'deviceUUID',
  'libVersion',
  'nodeHost',
  'nodeType',
  'appName',
  'appReleaseVersion',
  'package',
  'saga',
  'tvshow',
  'season',
  'titleEpisode',
  'channel',
  'imdbID',
  'gracenoteID',
  'contentType',
  'genre',
  'contentLanguage',
  'subtitles',
  'cost',
  'price',
  'playbackType',
  'email',
  'drm',
  'videoCodec',
  'audioCodec',
  'codecSettings',
  'codecProfile',
  'containerFormat',
  'contentId',
  'contractedResolution',
  'linkedViewId',
  'edid',
  'cdnBalancerResponseUUID',
  'triggeredEvents'
]

var adStartParams = [
  'player',
  'playhead',
  'adTitle',
  'position',
  'adDuration',
  'adCampaign',
  'adCreativeId',
  'adProvider',
  'adResource',
  'adPlayerVersion',
  'adProperties',
  'adAdapterVersion',
  'adInsertionType',
  'extraparam1',
  'extraparam2',
  'extraparam3',
  'extraparam4',
  'extraparam5',
  'extraparam6',
  'extraparam7',
  'extraparam8',
  'extraparam9',
  'extraparam10',
  'fullscreen',
  'audio',
  'skippable',
  'adNumber',
  'adNumberInBreak',
  'breakNumber'
]

var RequestBuilder = YouboraObject.extend(
  /** @lends youbora.RequestBuilder.prototype */
  {
    /**
     * This class helps building params associated with each event: /start, /joinTime...
     *
     * @constructs RequestBuilder
     * @extends youbora.YouboraObject
     * @memberof youbora
     *
     * @param {Plugin} plugin A Plugin instance
     */
    constructor: function (plugin) {
      this._plugin = plugin
      this._adNumber = 0
      this._adNumberInBreak = 0

      /** Stores a list of the last params fetched */
      this.lastSent = {}
    },

    /**
     * Adds to params all the entities specified in paramList, unless they are already set.
     *
     * @param {Object} params Object of params key:value.
     * @param {Array.string} paramList A list of params to fetch.
     * @param {bool} onlyDifferent If true, only fetches params that have changed
     */
    fetchParams: function (params, paramList, onlyDifferent) {
      params = params || {}
      paramList = paramList || []
      for (var i = 0; i < paramList.length; i++) {
        var param = paramList[i]

        if (params[param]) { continue }
        var getterName = RequestBuilder.getters[param]

        if (this._plugin[getterName]) {
          var value = this._plugin[getterName]()
          if (value !== null && (!onlyDifferent || this.lastSent[param] !== value)) {
            params[param] = value
            this.lastSent[param] = value
          }
        } else {
          Log.warn('Trying to call undefined getter ' + param + ':' + getterName)
        }
      }
      return params
    },

    getGetters: function () {
      return RequestBuilder.getters
    },

    buildBody: function (service) {
      var body = null
      return this.fetchParams(body, RequestBuilder.bodyParams[service], false)
    },

    /**
     * Adds to params object all the entities specified in paramList, unless they are already set.
     *
     * @param {Object} params Object of params key:value.
     * @param {string} service The name of the service. Use {@link Plugin.Service} enum.
     */
    buildParams: function (params, service) {
      params = params || {}
      this.fetchParams(params, RequestBuilder.params[service], false)
      this.fetchParams(params, RequestBuilder.differentParams[service], true)
      return params
    },

    /**
     * Creates an adnumber if it does not exist and stores it in lastSent. If it already exists,
     * it is incremented by 1.
     *
     * @returns {number} adNumber
     */
    getNewAdNumber: function () {
      var adNumber = this.lastSent.adNumber
      if (adNumber && this.lastSent.position === this._plugin.getAdPosition()) {
        adNumber += 1
      } else {
        adNumber = 1
      }
      this.lastSent.adNumber = adNumber
      return adNumber
    },

    /**
     * Creates an adnumberinbreak incrementing by 1.
     *
     * @returns {number} adNumber
     */
    getNewAdNumberInBreak: function () {
      this._adNumberInBreak++
      this.lastSent.adNumberInBreak = this._adNumberInBreak
      return this._adNumberInBreak
    },

    /**
     * Creates an breaknumber if it does not exist and stores it in lastSent. If it already exists,
     * it is incremented by 1.
     * Also resets adnumberinbreak
     * @returns {number} breakNumber
     */
    getNewBreakNumber: function () {
      var breakNumber = 1
      this._adNumberInBreak = 0
      if (this.lastSent.breakNumber) {
        breakNumber = this.lastSent.breakNumber + 1
      }
      this.lastSent.breakNumber = breakNumber
      return breakNumber
    },

    /**
     * Return changed entities since last check
     *
     * @returns {Object} params
     */
    getChangedEntities: function () {
      return this.fetchParams({}, RequestBuilder.differentParams.entities, true)
    }
  },
  /** @lends youbora.RequestBuilder */
  {
    // Static Members

    /** List of params used by each service */
    params: {
      '/data': ['system', 'pluginVersion', 'requestNumber', 'username'],

      '/init': startParams,
      '/start': startParams,
      '/joinTime': ['joinDuration', 'playhead', 'bitrate', 'triggeredEvents'],
      '/pause': ['playhead', 'triggeredEvents'],
      '/resume': ['pauseDuration', 'playhead', 'triggeredEvents'],
      '/seek': ['seekDuration', 'playhead', 'triggeredEvents'],
      '/bufferUnderrun': ['bufferDuration', 'playhead', 'triggeredEvents'],
      '/error': ['player', 'playhead', 'triggeredEvents'].concat(startParams),
      '/stop': ['bitrate', 'totalBytes', 'playhead', 'pauseDuration', 'metrics', 'cdnDownloadedTraffic', 'multiCdnInfo', 'p2pDownloadedTraffic', 'uploadTraffic', 'triggeredEvents'],

      '/infinity/video/event': ['playhead'],

      '/adInit': adStartParams,
      '/adStart': adStartParams,
      '/adJoin': ['playhead', 'position', 'adJoinDuration', 'adPlayhead', 'adNumber', 'adNumberInBreak', 'breakNumber'],
      '/adPause': ['playhead', 'position', 'adPlayhead', 'adNumber', 'adNumberInBreak', 'breakNumber'],
      '/adResume': ['playhead', 'position', 'adPlayhead', 'adPauseDuration', 'adNumber', 'adNumberInBreak', 'breakNumber'],
      '/adBufferUnderrun': ['playhead', 'position', 'adPlayhead', 'adBufferDuration', 'adNumber', 'adNumberInBreak', 'breakNumber'],
      '/adStop': ['playhead', 'position', 'adPlayhead', 'adBitrate', 'adTotalDuration', 'pauseDuration', 'adViewedDuration', 'adViewability', 'adNumber', 'adNumberInBreak', 'breakNumber'],
      '/adClick': ['playhead', 'position', 'adPlayhead', 'adNumber', 'adNumberInBreak', 'breakNumber'],
      '/adError': ['playhead'].concat(adStartParams),
      '/adManifest': ['givenBreaks', 'expectedBreaks', 'expectedPattern', 'breaksTime'],
      '/adBreakStart': ['position', 'givenAds', 'expectedAds', 'breakNumber', 'adInsertionType'],
      '/adBreakStop': ['position', 'breakNumber'],
      '/adQuartile': ['position', 'adViewedDuration', 'adViewability', 'adNumber', 'adNumberInBreak', 'breakNumber'],

      '/ping': ['droppedFrames', 'playrate', 'cdnDownloadedTraffic', 'multiCdnInfo', 'p2pDownloadedTraffic', 'uploadTraffic', 'latency', 'packetLoss', 'packetSent', 'metrics', 'totalBytes', 'segmentDuration'],

      '/infinity/session/start': [
        'accountCode',
        'username',
        'userType',
        'anonymousUser',
        'profileId',
        'route',
        'page',
        'referer',
        'referral',
        'screenResolution',
        'language',
        'deviceInfo',
        'adsBlocked',
        'deviceUUID',
        'libVersion',
        'appName',
        'appReleaseVersion',
        'isp',
        'connectionType',
        'ip',
        'obfuscateIp',
        'privacyProtocol',
        'dimensions',
        'param1',
        'param2',
        'param3',
        'param4',
        'param5',
        'param6',
        'param7',
        'param8',
        'param9',
        'param10',
        'param11',
        'param12',
        'param13',
        'param14',
        'param15',
        'param16',
        'param17',
        'param18',
        'param19',
        'param20',
        'edid'
      ],
      '/infinity/session/stop': ['sessionMetrics'],
      '/infinity/session/nav': ['route', 'page', 'scrollDepth'],
      '/infinity/session/beat': ['sessionMetrics'],
      '/infinity/session/event': ['accountCode'],

      '/offlineEvents': {}
    },

    /** Values for request body */
    bodyParams: {
      '/offlineEvents': ['viewJson']
    },

    /** List of params used by each service (only if they are different) */
    differentParams: {
      entities: [
        'rendition',
        'title',
        'title2',
        'param1',
        'param2',
        'param3',
        'param4',
        'param5',
        'param6',
        'param7',
        'param8',
        'param9',
        'param10',
        'param11',
        'param12',
        'param13',
        'param14',
        'param15',
        'param16',
        'param17',
        'param18',
        'param19',
        'param20',
        'cdn',
        'nodeHost',
        'nodeType',
        'nodeTypeString',
        'subtitles',
        'contentLanguage',
        'contentId'
      ]
    },

    /** List of params and its related getter */
    getters: {
      requestNumber: 'getRequestNumber',
      playhead: 'getPlayhead',
      playrate: 'getPlayrate',
      fps: 'getFramesPerSecond',
      segmentDuration: 'getSegmentDuration',
      droppedFrames: 'getDroppedFrames',
      mediaDuration: 'getDuration',
      bitrate: 'getBitrate',
      totalBytes: 'getTotalBytes',
      throughput: 'getThroughput',
      rendition: 'getRendition',
      title: 'getTitle',
      title2: 'getTitle2',
      live: 'getIsLive',
      mediaResource: 'getResource',
      parsedResource: 'getParsedResource',
      transactionCode: 'getTransactionCode',
      properties: 'getMetadata',
      playerVersion: 'getPlayerVersion',
      player: 'getPlayerName',
      cdn: 'getCdn',
      pluginVersion: 'getPluginVersion',
      libVersion: 'getLibVersion',
      userType: 'getUserType',
      streamingProtocol: 'getStreamingProtocol',
      transportFormat: 'getTransportFormat',
      obfuscateIp: 'getObfuscateIp',
      privacyProtocol: 'getPrivacyProtocol',
      householdId: 'getHouseholdId',
      latency: 'getLatency',
      packetLoss: 'getPacketLoss',
      packetSent: 'getPacketSent',
      metrics: 'getVideoMetrics',
      dimensions: 'getCustomDimensions',

      param1: 'getExtraparam1',
      param2: 'getExtraparam2',
      param3: 'getExtraparam3',
      param4: 'getExtraparam4',
      param5: 'getExtraparam5',
      param6: 'getExtraparam6',
      param7: 'getExtraparam7',
      param8: 'getExtraparam8',
      param9: 'getExtraparam9',
      param10: 'getExtraparam10',
      param11: 'getExtraparam11',
      param12: 'getExtraparam12',
      param13: 'getExtraparam13',
      param14: 'getExtraparam14',
      param15: 'getExtraparam15',
      param16: 'getExtraparam16',
      param17: 'getExtraparam17',
      param18: 'getExtraparam18',
      param19: 'getExtraparam19',
      param20: 'getExtraparam20',

      extraparam1: 'getAdExtraparam1',
      extraparam2: 'getAdExtraparam2',
      extraparam3: 'getAdExtraparam3',
      extraparam4: 'getAdExtraparam4',
      extraparam5: 'getAdExtraparam5',
      extraparam6: 'getAdExtraparam6',
      extraparam7: 'getAdExtraparam7',
      extraparam8: 'getAdExtraparam8',
      extraparam9: 'getAdExtraparam9',
      extraparam10: 'getAdExtraparam10',

      position: 'getAdPosition',
      adNumber: 'getAdNumber',
      adNumberInBreak: 'getAdNumberInBreak',
      breakNumber: 'getBreakNumber',
      adPlayhead: 'getAdPlayhead',
      adDuration: 'getAdDuration',
      adCampaign: 'getAdCampaign',
      adCreativeId: 'getAdCreativeId',
      adBitrate: 'getAdBitrate',
      adTitle: 'getAdTitle',
      adResource: 'getAdResource',
      adPlayerVersion: 'getAdPlayerVersion',
      adProperties: 'getAdMetadata',
      adAdapterVersion: 'getAdAdapterVersion',
      givenBreaks: 'getGivenBreaks',
      expectedBreaks: 'getExpectedBreaks',
      expectedPattern: 'getExpectedPattern',
      breaksTime: 'getBreaksTime',
      givenAds: 'getGivenAds',
      expectedAds: 'getExpectedAds',
      adsExpected: 'getAdsExpected',
      adViewedDuration: 'getAdViewedDuration',
      adViewability: 'getAdViewability',
      fullscreen: 'getIsFullscreen',
      audio: 'getAudioEnabled',
      skippable: 'getIsSkippable',
      adProvider: 'getAdProvider',
      adInsertionType: 'getAdInsertionType',

      pluginInfo: 'getPluginInfo',

      isp: 'getIsp',
      connectionType: 'getConnectionType',
      ip: 'getIp',

      deviceInfo: 'getDeviceInfo',
      edid: 'getEDID',

      system: 'getAccountCode',
      accountCode: 'getAccountCode',
      username: 'getUsername',
      anonymousUser: 'getAnonymousUser',
      profileId: 'getProfileId',

      joinDuration: 'getJoinDuration',
      bufferDuration: 'getBufferDuration',
      seekDuration: 'getSeekDuration',
      pauseDuration: 'getPauseDuration',

      adJoinDuration: 'getAdJoinDuration',
      adBufferDuration: 'getAdBufferDuration',
      adPauseDuration: 'getAdPauseDuration',
      adTotalDuration: 'getAdTotalDuration',

      referer: 'getReferer',
      referral: 'getReferral',
      language: 'getLanguage',
      screenResolution: 'getScreenResolution',

      nodeHost: 'getNodeHost',
      nodeType: 'getNodeType',
      nodeTypeString: 'getNodeTypeString',

      route: 'getReferer',
      page: 'getPageName',

      playerStartupTime: 'getPlayerStartupTime',
      pageLoadTime: 'getPageLoadTime',

      cdnDownloadedTraffic: 'getCdnTraffic',
      multiCdnInfo: 'getMultiCdnInfo',
      p2pDownloadedTraffic: 'getP2PTraffic',
      p2pEnabled: 'getIsP2PEnabled',
      uploadTraffic: 'getUploadTraffic',
      cdnBalancerResponseUUID: 'getBalancerResponseId',

      viewJson: 'getOfflineView',
      deviceUUID: 'getDeviceUUID',
      sessionMetrics: 'getSessionMetrics',
      scrollDepth: 'getScrollDepth',

      adsBlocked: 'getIsBlocked',
      linkedViewId: 'getLinkedViewId',

      appName: 'getAppName',
      appReleaseVersion: 'getAppReleaseVersion',
      package: 'getPackage',
      saga: 'getSaga',
      tvshow: 'getTvShow',
      season: 'getSeason',
      titleEpisode: 'getEpisodeTitle',
      channel: 'getChannel',
      drm: 'getDRM',
      videoCodec: 'getVideoCodec',
      audioCodec: 'getAudioCodec',
      codecSettings: 'getCodecSettings',
      codecProfile: 'getCodecProfile',
      containerFormat: 'getContainerFormat',
      contentId: 'getID',
      imdbID: 'getImdbId',
      gracenoteID: 'getGracenoteID',
      contentType: 'getType',
      genre: 'getGenre',
      contentLanguage: 'getVideoLanguage',
      subtitles: 'getSubtitles',
      contractedResolution: 'getContractedResolution',
      cost: 'getCost',
      price: 'getPrice',
      playbackType: 'getPlaybackType',
      email: 'getEmail'
    }

  }
)

module.exports = RequestBuilder
