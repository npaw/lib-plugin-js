var Log = require('../log')
var Util = require('../util')
var version = require('../version')
var TransportFormat = require('../constants/transportFormat.js')
var StreamingProtocol = require('../constants/streamingProtocols')

// This file is designed to add extra functionalities to Plugin class

var PluginContentGetterMixin = {
  /**
       * Returns content's playhead
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayhead: function () {
    var ret = this._safeGetterAdapter('getPlayhead')
    return Util.parseNumber(ret, 0)
  },

  /**
       * Returns content's PlayRate
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayrate: function () {
    var ret = 0
    if (this._adapter && this._adapter.flags && !this._adapter.flags.isPaused) {
      try {
        ret = this._adapter.getPlayrate()
      } catch (err) {
        Log.warn('An error occured while calling getPlayrate', err)
      }
    }
    return ret
  },

  /**
       * Returns content's FramesPerSecond
       *
       * @memberof youbora.Plugin.prototype
       */
  getFramesPerSecond: function () {
    return this._safeGetterAdapter('getFramesPerSecond', 'content.fps')
  },

  /**
   * Returns content's segment duration
   *
   * @memberof youbora.Plugin.prototype
   */
  getSegmentDuration: function () {
    return this.hybridNetwork.getSegmentDuration() || this._safeGetterAdapter('getSegmentDuration', 'content.segmentDuration')
  },

  /**
       * Returns content's DroppedFrames
       *
       * @memberof youbora.Plugin.prototype
       */
  getDroppedFrames: function () {
    var ret = this._safeGetterAdapter('getDroppedFrames')
    if (!ret) {
      ret = this.getWebkitDroppedFrames()
    }
    return Util.parseNumber(ret, 0)
  },

  /**
       * Returns dropped frames as per webkitDroppedFrameCount
       *
       * @returns {number}
       *
       * @memberof youbora.Plugin.prototype
       */
  getWebkitDroppedFrames: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitDroppedFrameCount) {
      return this._adapter.tag.webkitDroppedFrameCount
    }
    return null
  },

  /**
       * Returns content's Duration
       *
       * @memberof youbora.Plugin.prototype
       */
  getDuration: function () {
    var ret = this._safeGetterAdapter('getDuration', 'content.duration') || null
    return Util.parseNumber(Math.round(ret), null)
  },

  /**
       * Returns content's Bitrate
       *
       * @memberof youbora.Plugin.prototype
       */
  getBitrate: function () {
    var ret = this._safeGetterAdapter('getBitrate', 'content.bitrate')
    if (!ret || ret === -1) {
      ret = this.getWebkitBitrate()
    }
    return Util.parseNumber(ret, -1)
  },

  /**
       * Returns content's total bytes downloaded
       *
       * @memberof youbora.Plugin.prototype
       */
  getTotalBytes: function () {
    return this.options['content.sendTotalBytes'] ? this._safeGetterAdapter('getTotalBytes', 'content.totalBytes') : null
  },

  /**
       * Returns bitrate as per webkitVideoDecodedByteCount
       *
       * @param {Object} tag Video tag DOM reference.
       * @returns {number}
       *
       * @memberof youbora.Plugin.prototype
       */
  getWebkitBitrate: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitVideoDecodedByteCount) {
      var bitrate = this._adapter.tag.webkitVideoDecodedByteCount
      if (this._lastWebkitBitrate) {
        var delta = this._adapter.tag.webkitVideoDecodedByteCount - this._lastWebkitBitrate
        bitrate = Math.round(((delta) / this.viewTransform.response.pingTime) * 8)
      }
      this._lastWebkitBitrate = this._adapter.tag.webkitVideoDecodedByteCount
      return bitrate !== 0 ? bitrate : -1
    }
  },

  /**
       * Returns content's Throughput
       *
       * @memberof youbora.Plugin.prototype
       */
  getThroughput: function () {
    var ret = this._safeGetterAdapter('getThroughput', 'content.throughput')
    return Util.parseNumber(ret, -1)
  },

  /**
       * Returns content's Rendition
       *
       * @memberof youbora.Plugin.prototype
       */
  getRendition: function () {
    return this._safeGetterAdapter('getRendition', 'content.rendition')
  },

  /**
       * Returns content's Title
       *
       * @memberof youbora.Plugin.prototype
       */
  getTitle: function () {
    return this._safeGetterAdapter('getTitle', 'content.title')
  },

  /**
       * Returns content's Title2
       *
       * @memberof youbora.Plugin.prototype
       */
  getTitle2: function () {
    return this._safeGetterAdapter('getTitle2', 'content.program')
  },

  /**
       * Returns content's IsLive
       *
       * @memberof youbora.Plugin.prototype
       */
  getIsLive: function () {
    var ret = this.options['content.isLive']
    ret = this._convertStringToBoolean(ret)

    if (!ret && ret !== false) {
      ret = this._safeGetterAdapter('getIsLive') || false
    }
    return ret
  },

  /**
       * Returns content's Resource after being parsed by the resourceTransform
       *
       * @memberof youbora.Plugin.prototype
       */
  getResource: function () {
    return this._safeGetterAdapter('getResource', 'content.resource')
  },

  getParsedResource: function () {
    var ret = null
    if (!this.resourceTransform.isBlocking()) {
      ret = this.resourceTransform.getResource()
    }
    ret = ret || this._safeGetterAdapter('getURLToParse')
    return (ret === this.getResource()) ? null : ret
  },

  /**
       * Returns content's TransactionCode
       *
       * @memberof youbora.Plugin.prototype
       */
  getTransactionCode: function () {
    return this.options['content.transactionCode']
  },

  /**
       * Returns content's Metadata
       *
       * @memberof youbora.Plugin.prototype
       */
  getMetadata: function () {
    return this.options['content.metadata']
  },

  /**
       * Returns content's PlayerVersion
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayerVersion: function () {
    return this._safeGetterAdapter('getPlayerVersion') || ''
  },

  /**
       * Returns content's PlayerName
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayerName: function () {
    return this._safeGetterAdapter('getPlayerName') || ''
  },

  /**
       * Returns content's Cdn
       *
       * @memberof youbora.Plugin.prototype
       */
  getCdn: function () {
    var ret = null
    if (!this.resourceTransform.isBlocking()) {
      ret = this.resourceTransform.getCdnName()
    }
    return ret || this.options['content.cdn']
  },

  /**
       * Returns content's PluginVersion
       *
       * @memberof youbora.Plugin.prototype
       */
  getPluginVersion: function () {
    var ret = this.getAdapterVersion()
    if (!ret) ret = version + '-adapterless-js'

    return ret
  },

  /**
       * Returns content's PluginVersion
       *
       * @memberof youbora.Plugin.prototype
       */
  getLibVersion: function () {
    return version
  },

  /**
       * Returns ads adapter getVersion or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getAdapterVersion: function () {
    return this._safeGetterAdapter('getVersion')
  },

  /**
       * Returns cdn traffic received in bytes or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getCdnTraffic: function () {
    return this._safeGetterAdapter('getCdnTraffic') || this.hybridNetwork.getCdnTraffic()
  },

  /**
   * Returns multi cdn traffic received in bytes or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getMultiCdnInfo: function () {
    return this.hybridNetwork.getMultiCdnInfo()
  },

  /**
   * Returns cdn balancer response id
   *
   * @memberof youbora.Plugin.prototype
   */
  getBalancerResponseId: function () {
    var ret = this.hybridNetwork.getBalancerResponseId()
    if (!ret && this.options['cdnbalancer.uuid']) {
      return this.options['cdnbalancer.uuid']
    }
    return ret
  },

  /**
       * Returns p2p traffic received in bytes or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getP2PTraffic: function () {
    return this._safeGetterAdapter('getP2PTraffic') || this.hybridNetwork.getP2PTraffic()
  },

  /**
       * Returns p2p traffic sent in bytes or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getUploadTraffic: function () {
    return this._safeGetterAdapter('getUploadTraffic') || this.hybridNetwork.getUploadTraffic()
  },

  /**
       * Returns if p2p plugin is enabled or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getIsP2PEnabled: function () {
    return this._safeGetterAdapter('getIsP2PEnabled') || this.hybridNetwork.getIsP2PEnabled()
  },

  /** Returns streaming protocol (DASH, HLS, HDS...) */
  getStreamingProtocol: function () {
    var ret = this.options['content.streamingProtocol']
    // if ret is not in the list of possible options, ret = null
    if (ret) {
      for (var i in StreamingProtocol) {
        if (StreamingProtocol[i] === ret) {
          return ret
        }
      }
    }
    return null
  },

  /** Returns transport format (TS, MP4) */
  getTransportFormat: function () {
    var ret = this.options['content.transportFormat']
    if (!ret && this.options['parse.manifest'] && !this.resourceTransform.isBlocking()) {
      ret = this.resourceTransform.getTransportFormat()
    }
    // if ret is not in the list of possible options, ret = null
    if (ret) {
      for (var i in TransportFormat) {
        if (TransportFormat[i] === ret) {
          return ret
        }
      }
    }
    return null
  },

  /** Returns household id */
  getHouseholdId: function () {
    return this._safeGetterAdapter('getHouseholdId')
  },

  /**
       * Returns latency of a live video, or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getLatency: function () {
    return this.getIsLive() ? this._safeGetterAdapter('getLatency') : null
  },

  /**
       * Returns the amount of packets lost, or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getPacketLoss: function () {
    return this._safeGetterAdapter('getPacketLoss')
  },

  /**
       * Returns the amount of packets sent, or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getPacketSent: function () {
    return this._safeGetterAdapter('getPacketSent')
  },

  /**
       * Returns a json with the metrics to be reported in pings when changed
       *
       * @memberof youbora.Plugin.prototype
       */
  getVideoMetrics: function () {
    return Util.getMetricsFrom(this._adapter ? this._adapter.getMetrics() : null, this.options['content.metrics'])
  },

  getPlayerStartupTime: function () {
    return this.browserLoadTimes.getPlayerStartupTime()
  },

  // ----------------------------------------- CHRONOS ------------------------------------------

  /**
       * Returns JoinDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getJoinDuration: function () {
    return this._adapter ? this._adapter.chronos.join.getDeltaTime(false) : -1
  },

  /**
       * Returns BufferDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getBufferDuration: function () {
    return this._adapter ? this._adapter.chronos.buffer.getDeltaTime(false) : -1
  },

  /**
       * Returns SeekDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getSeekDuration: function () {
    return this._adapter ? this._adapter.chronos.seek.getDeltaTime(false) : -1
  },

  /**
       * Returns pauseDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getPauseDuration: function () {
    return this._adapter ? this._adapter.chronos.pause.getDeltaTime(false) : 0
  },

  /**
       * Returns content's package
       *
       * @memberof youbora.Plugin.prototype
       */
  getPackage: function () {
    return this.options['content.package']
  },

  /**
       * Returns content's saga
       *
       * @memberof youbora.Plugin.prototype
       */
  getSaga: function () {
    return this.options['content.saga']
  },

  /**
       * Returns content's tv show
       *
       * @memberof youbora.Plugin.prototype
       */
  getTvShow: function () {
    return this.options['content.tvShow']
  },

  /**
       * Returns content's season
       *
       * @memberof youbora.Plugin.prototype
       */
  getSeason: function () {
    return this.options['content.season']
  },

  /**
       * Returns content's episode title
       *
       * @memberof youbora.Plugin.prototype
       */
  getEpisodeTitle: function () {
    return this.options['content.episodeTitle']
  },

  /**
       * Returns content's channel
       *
       * @memberof youbora.Plugin.prototype
       */
  getChannel: function () {
    return this.options['content.channel']
  },

  /**
       * Returns content's id
       *
       * @memberof youbora.Plugin.prototype
       */
  getID: function () {
    return this.options['content.id']
  },

  /**
       * Returns content's IMDB id
       *
       * @memberof youbora.Plugin.prototype
       */
  getImdbId: function () {
    return this.options['content.imdbId']
  },

  /**
       * Returns content's gracenote id
       *
       * @memberof youbora.Plugin.prototype
       */
  getGracenoteID: function () {
    return this.options['content.gracenoteId']
  },

  /**
       * Returns content's type
       *
       * @memberof youbora.Plugin.prototype
       */
  getType: function () {
    return this.options['content.type']
  },

  /**
       * Returns content's genre
       *
       * @memberof youbora.Plugin.prototype
       */
  getGenre: function () {
    return this.options['content.genre']
  },

  /**
       * Returns content's language
       *
       * @memberof youbora.Plugin.prototype
       */
  getVideoLanguage: function () {
    return this.options['content.language']
  },

  /**
       * Returns content's subtitles
       *
       * @memberof youbora.Plugin.prototype
       */
  getSubtitles: function () {
    return this.options['content.subtitles']
  },

  /**
       * Returns content's contracted resolution
       *
       * @memberof youbora.Plugin.prototype
       */
  getContractedResolution: function () {
    return this.options['content.contractedResolution']
  },

  /**
       * Returns content's cost
       *
       * @memberof youbora.Plugin.prototype
       */
  getCost: function () {
    return this.options['content.cost']
  },

  /**
       * Returns content's price
       *
       * @memberof youbora.Plugin.prototype
       */
  getPrice: function () {
    return this.options['content.price']
  },

  /**
       * Returns content's playback type
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlaybackType: function () {
    var ret = this.options['content.playbackType']
    if (!ret) {
      var isLive = this.options['content.isLive']
      if (typeof isLive !== 'boolean') {
        isLive = this._safeGetterAdapter('getIsLive')
      }
      if (typeof isLive === 'boolean') {
        ret = isLive ? 'Live' : 'VoD'
      }
    }
    return ret
  },

  /**
       * Returns content's DRM
       *
       * @memberof youbora.Plugin.prototype
       */
  getDRM: function () {
    return this.options['content.drm']
  },

  /**
       * Returns content's video codec
       *
       * @memberof youbora.Plugin.prototype
       */
  getVideoCodec: function () {
    return this._safeGetterAdapter('getVideoCodec', 'content.encoding.videoCodec')
  },

  /**
       * Returns content's audio codec
       *
       * @memberof youbora.Plugin.prototype
       */
  getAudioCodec: function () {
    return this._safeGetterAdapter('getAudioCodec', 'content.encoding.audioCodec')
  },

  /**
       * Returns content's codec settings
       *
       * @memberof youbora.Plugin.prototype
       */
  getCodecSettings: function () {
    return this.options['content.encoding.codecSettings']
  },

  /**
       * Returns content's codec profile
       *
       * @memberof youbora.Plugin.prototype
       */
  getCodecProfile: function () {
    return this.options['content.encoding.codecProfile']
  },

  /**
       * Returns content's container format
       *
       * @memberof youbora.Plugin.prototype
       */
  getContainerFormat: function () {
    return this.options['content.encoding.containerFormat']
  },

  /**
     * Returns linked view id string
     *
     * @memberof youbora.Plugin.prototype
     */
  getLinkedViewId: function () {
    return this.options.linkedViewId
  },

  _safeGetterAdapter: function (functionName, optionName) {
    var ret = null
    if (optionName && this.options[optionName]) {
      ret = this.options[optionName]
    } else {
      try {
        if (this._adapter && typeof this._adapter[functionName] === 'function') {
          ret = this._adapter[functionName]()
        }
      } catch (err) {
        Log.warn('An error occured while calling ' + functionName, err)
      }
    }
    return ret
  },

  _convertStringToBoolean: function (value) {
    if (value === 'true') {
      return true
    }
    if (value === 'false' || typeof value === 'string') {
      return false
    }

    return value
  }
}

module.exports = PluginContentGetterMixin
