// This file is designed to add extra functionalities to Plugin class

var Util = require('../util')

var PluginInfinityGettersMixin = {
  getContext: function () {
    return 'Default'
  },

  getScrollDepth: function () {
    var ret = this.storage.getSession('pageScrollDepth')
    this.storage.removeSession('pageScrollDepth')
    return ret
  },

  getSession: function () {
    var ret = this.storage.getStorages('session')
    // Some tvs set the value as 'undefined' as string when deleting the value
    if (ret === 'undefined') {
      ret = undefined
    }
    if (!ret) {
      var data = this.getStoredData()
      if (data) {
        try {
          ret = JSON.parse(data).q.c
        } catch (err) {
          // nothing
        }
      }
    }
    return ret
  },

  getStorageHost: function () {
    var ret = null
    var data = this.getStoredData()
    if (data) {
      try {
        ret = JSON.parse(data).q.h
      } catch (err) {
        // nothing
      }
    }
    return ret ? Util.addProtocol(ret, this.options['app.https']) : ret
  },

  getStoredData: function () {
    return this.storage.getStorages('data')
  },

  getDataTime: function () {
    return this.storage.getStorages('dataTime')
  },

  getLastActive: function () {
    return this.storage.getStorages('lastactive')
  },

  setStoredData: function (data) {
    this.storage.setStorages('data', data)
  },

  setSession: function (session) {
    this.storage.setStorages('session', session)
  },

  setDataTime: function (time) {
    this.storage.setStorages('dataTime', time)
  },

  setLastActive: function (last) {
    this.storage.setStorages('lastactive', last)
  },

  getPageName: function () {
    if (typeof document !== 'undefined' && document.title) {
      return document.title
    }
  },

  getPageLoadTime: function () {
    return this.browserLoadTimes.getPageLoadTime()
  },

  getIsSessionExpired: function () {
    var now = new Date().getTime()
    return !this.getSession() || (this.infinity.getFirstActive() < now - this.sessionExpire)
  },

  getIsDataExpired: function () {
    var now = new Date().getTime()
    return !this.storage.isEnabled() || !this.getStoredData() || (this.infinity.getFirstActive() < now - this.sessionExpire)
  }
}

module.exports = PluginInfinityGettersMixin
