var Log = require('../log')
var Constants = require('../constants')
var Util = require('../util')
var Adapter = require('../adapter/adapter')

// This file is designed to add extra functionalities to Plugin class

/** @lends youbora.Plugin.prototype */
var PluginContentMixin = {
  /**
   * Sets an adapter for video content.
   *
   * @param {Adapter} adapter
   *
   * @memberof youbora.Plugin.prototype
   */
  setAdapter: function (adapter) {
    if (this.browserLoadTimes) this.browserLoadTimes.setPlayerSetupTime()
    if (adapter.plugin) {
      Log.warn('Adapters can only be added to a single plugin')
    } else {
      this.removeAdapter()

      this._adapter = adapter
      adapter.plugin = this
      adapter.setIsAds(false)

      // Register listeners
      this.contentAdapterListeners = {}
      this.contentAdapterListeners[Adapter.Event.START] = this._startListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.JOIN] = this._joinListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.PAUSE] = this._pauseListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.RESUME] = this._resumeListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.SEEK_BEGIN] = this._seekBufferBeginListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.SEEK_END] = this._seekEndListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.BUFFER_BEGIN] = this._seekBufferBeginListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.BUFFER_END] = this._bufferEndListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.ERROR] = this._errorListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.STOP] = this._stopListener.bind(this)
      this.contentAdapterListeners[Adapter.Event.VIDEO_EVENT] = this._videoEventListener.bind(this)

      for (var key in this.contentAdapterListeners) {
        this._adapter.on(key, this.contentAdapterListeners[key])
      }

      // We have to trigger an ('adapterSet') event in here so that ProductAnalytics can listen to it and call the appropriate methods

      if (this.productAnalytics) {
        this.productAnalytics.adapterAfterSet()
      }
    }
  },

  /**
   * Returns current adapter or null.
   *
   * @returns {Adapter}
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdapter: function () {
    return this._adapter
  },

  /**
   * Removes the current adapter. Fires stop if needed. Calls adapter.dispose().
   *
   * @memberof youbora.Plugin.prototype
   * */
  removeAdapter: function () {
    if (this._adapter) {
      // We have to trigger an ('adapterBeforeRemove') event in here so that ProductAnalytics can listen to it and call the appropriate methods

      if (this.productAnalytics) {
        this.productAnalytics.adapterBeforeRemove()
      }

      this._adapter.dispose()
      this._adapter.plugin = null

      if (this.contentAdapterListeners) {
        for (var key in this.contentAdapterListeners) {
          this._adapter.off(key, this.contentAdapterListeners[key])
        }
        delete this.contentAdapterListeners
      }

      this._adapter = null
    }
  },

  // ---------------------------------------- LISTENERS -----------------------------------------
  _startListener: function (e) {
    if (!this.isInitiated) {
      this.viewTransform.nextView()

      if (this.options && this.options['session.splitViews']) {
        e.data.params.viewIndex = this.viewTransform._viewIndex
      }

      this._initComm()
      this._startPings()
    } else if (this.initChrono.startTime !== 0) {
      this._adapter.chronos.join.startTime = this.initChrono.startTime
    }
    try {
      // If exists resourceTransform, init with the resource
      if (this._adapter && this.resourceTransform) {
        var resource = this._getResourceForTransform()
        this.resourceTransform.init(resource)
      }
    } catch (e) {}
    var params = e.data.params || {}
    var allParamsReady = this.getResource() && typeof this.getIsLive() === 'boolean' &&
      (this.getIsLive() || (typeof this.getDuration() === 'number' && this.getDuration() > 0)) && this.getTitle()
    allParamsReady = this.options.forceInit ? false : (allParamsReady && this._isExtraMetadataReady())
    if (allParamsReady && !this.isInitiated) { // start
      this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, params)
      this._adSavedError()
      this._adSavedManifest()
      Log.notice(Constants.Service.START + ' ' + (params.title || params.mediaResource))
      this.isStarted = true
      // chrono if had no adapter when inited
    } else if (!this.isInitiated) { // init
      this.isInitiated = true
      this._adapter.chronos.join.start()
      this._send(Constants.WillSendEvent.WILL_SEND_INIT, Constants.Service.INIT, params)
      this._adSavedError()
      this._adSavedManifest()
      Log.notice(Constants.Service.INIT + ' ' + (params.title || params.mediaResource))
    }
  },

  _retryStart: function (e) {
    if (this._isExtraMetadataReady()) {
      this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, {})
      this.startDelayed = false
    }
  },

  _joinListener: function (e) {
    var params = {}
    Util.assign(params, e.data.params || {})
    if (!this._adsAdapter || !this._adsAdapter.flags.isStarted) {
      if (this._adapter) {
        this._adapter.chronos.join.startTime = Math.min(
          this._adapter.chronos.join.startTime + (this._totalPrerollsTime || 0),
          new Date().getTime()
        )
        this._totalPrerollsTime = 0
      }
      if (this.isInitiated && !this.isStarted) { // start if just inited
        if (this._isExtraMetadataReady()) {
          this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, params)
        } else {
          this.startDelayed = true
        }
        this._adSavedError()
        this._adSavedManifest()
        Log.notice(Constants.Service.START + ' ' + (params.title || params.mediaResource))
        this.isStarted = true
      }
      params = e.data.params || {}
      if (this._adsAdapter && this.isBreakStarted) {
        this._adsAdapter.fireBreakStop()
      }
      this._send(Constants.WillSendEvent.WILL_SEND_JOIN, Constants.Service.JOIN, params)
      Log.notice(Constants.Service.JOIN + ' ' + params.joinDuration + 'ms')
    } else { // If it is currently showing ads, join is invalidated
      if (this._adapter.monitor) this._adapter.monitor.stop()
      this._adapter.flags.isJoined = false
      this._adapter.chronos.join.stopTime = 0
    }
  },

  _pauseListener: function (e) {
    if (this._adapter) {
      if (this._adapter.flags.isBuffering ||
        this._adapter.flags.isSeeking ||
        (this._adsAdapter && this._adsAdapter.flags.isStarted)) {
        this._adapter.chronos.pause.reset()
      }
    }

    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_PAUSE, Constants.Service.PAUSE, params)
    Log.notice(Constants.Service.PAUSE + ' at ' + params.playhead + 's')
  },

  _resumeListener: function (e) {
    if (this._adsAdapter && this.isBreakStarted && !this._adsAdapter.flags.isStarted) {
      this._adsAdapter.fireBreakStop()
    }
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_RESUME, Constants.Service.RESUME, params)
    Log.notice(Constants.Service.RESUME + ' ' + params.pauseDuration + 'ms')
    this._adapter.chronos.pause.reset()
  },

  _seekBufferBeginListener: function (e) {
    if (this._adapter && this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    if (e.type && e.type.includes('buffer')) {
      Log.notice(e.type + ', eventsTriggered: ' + this._adapter.fireEventsStruct.buffer)
    } else {
      Log.notice(e.type + ', eventsTriggered: ' + this._adapter.fireEventsStruct.seek)
    }
  },

  _seekEndListener: function (e) {
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_SEEK, Constants.Service.SEEK, params)
    Log.notice(Constants.Service.SEEK +
      ' to ' + params.playhead +
      ' in ' + params.seekDuration + 'ms' +
      ((params.triggeredEvents) ? ', eventsTriggered: ' + params.triggeredEvents : '')
    )
  },

  _bufferEndListener: function (e) {
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_BUFFER, Constants.Service.BUFFER, params)
    Log.notice(Constants.Service.BUFFER +
      ' to ' + params.playhead +
      ' in ' + params.bufferDuration + 'ms' +
      ((params.triggeredEvents) ? ', eventsTriggered: ' + params.triggeredEvents : '')
    )
  },

  _errorListener: function (e) {
    if (!this._blockError(e.data.params)) {
      this.fireError(e.data.params || {})
      this._adSavedError()
      this._adSavedManifest()
    }
  },

  _blockError: function (errorParams) {
    var now = Date.now()
    var sameError = this._lastErrorParams
      ? this._lastErrorParams.errorCode === errorParams.errorCode && this._lastErrorParams.msg === errorParams.msg
      : false
    if (sameError && this._lastErrorTime + 5000 > now) {
      this._lastErrorTime = now
      return true
    }
    this._lastErrorTime = now
    this._lastErrorParams = errorParams
    return false
  },

  _stopListener: function (e) {
    this.fireStop(e.data.params || {})
  },

  _isStopReady: function (e) {
    var ret = false
    // this solution is only for the case of:
    if (!this.requestBuilder.lastSent.live && // VOD, live have no postrolls
      this._adsAdapter && // having ads adapter connected
      this._adapter && // playhead close to the end of the content (or 0 because is already restarted)
      (!this._adapter.getPlayhead() || this._adapter.getPlayhead() >= this.requestBuilder.lastSent.mediaDuration - 1)) {
      var expectedPostrolls = 0
      var pat = this.options['ad.expectedPattern']
      // We can get the expectedPostrolls from the expected pattern if it has postrolls defined
      if (pat && pat.post && pat.post[0]) {
        expectedPostrolls = pat.post[0]
        // If not, while playing postrolls after adbreakstart we can get the givenAds
      } else if (this.requestBuilder.lastSent.breaksTime) {
        if (this.requestBuilder.lastSent.position === Constants.AdPosition.Postroll) {
          expectedPostrolls = this.requestBuilder.lastSent.givenAds
        }
        // Or before playing postrolls, at least, we can check using breaksTime (from adManifest event) if we expect at least 1 postroll
        if (!expectedPostrolls && this.requestBuilder.lastSent.breaksTime) {
          var breaks = this.requestBuilder.lastSent.breaksTime
          if (breaks.length > 0 && this.requestBuilder.lastSent.mediaDuration) { // If there is no duration probably is a live content, so no postrolls
            var lastTimePosition = Math.round(breaks[breaks.length - 1])
            if (lastTimePosition + 1 >= this.requestBuilder.lastSent.mediaDuration) {
              expectedPostrolls = 1
            }
          }
        }
        // If none of the previous solutions found anything, we assume we have no postrolls
      } else {
        ret = true
      }
      // Finally, if the number of played postrolls is the same (or more) than the expected, we can close the view
      if (expectedPostrolls <= this.playedPostrolls) {
        ret = true
      }
    } else {
      ret = true
    }
    return ret
  },

  _videoEventListener: function (e) {
    this._send(Constants.WillSendEvent.WILL_SEND_VIDEO_EVENT, Constants.Service.VIDEO_EVENT, e.data.params)
  },

  _isExtraMetadataReady: function (e) {
    // If the option is disabled, always is ready
    if (!this.options.waitForMetadata || this.options.pendingMetadata.length < 1) return true
    // If for existing parameters, one of them is false (no value for it), return false
    var getters = this.requestBuilder.getGetters()
    return this.options.pendingMetadata.map(function (element) {
      if (getters.hasOwnProperty(element)) { // eslint-disable-line no-prototype-builtins
        return !!this[getters[element]]()
      }
    }.bind(this)).indexOf(false) < 0
  }
}

module.exports = PluginContentMixin
