var DataExtractor = require('./dataextractor')
var YouboraObject = require('../object')
var MD5 = require('./md5')

var HashGenerator = YouboraObject.extend({
  constructor: function (plugin) {
    this.plugin = plugin
    this.dataExtractor = new DataExtractor()
    this.key = this.getPreviousKey()
  },

  generateHashKey: function () {
    if (!this.key) {
      var data = this.dataExtractor.getAllData()
      this.key = this._hashFunction(data)
      this.plugin.storage.setLocal('youboraDeviceUUID', this.key)
    }
  },

  getKey: function () {
    if (!this.key) this.generateHashKey()
    return this.key
  },

  _hashFunction: function (inputString) {
    var outputString = inputString
    if (typeof inputString !== 'string') {
      outputString = JSON.stringify(inputString)
    }
    return MD5(outputString)
  },

  _reset: function () {
    this.key = undefined
  },

  getPreviousKey: function () {
    return this.plugin.storage.getLocal('youboraDeviceUUID')
  }

})

module.exports = HashGenerator
