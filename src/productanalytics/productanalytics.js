var YouboraObject = require('../object')
var Log = require('../log')

var ProductAnalyticsConfig = require('./productanalytics_config')
var ProductAnalyticsUserState = require('./productanalytics_userstate')

var YouboraProductAnalytics = YouboraObject.extend(
  /** @lends youbora.ProductAnalytics.prototype */
  {
    /**
     * This class is the base of productanalytics. Every plugin will have an instance.
     *
     * @param {youbora.Plugin} plugin Parent plugin.
     *
     * @constructs youbora.ProductAnalytics
     * @extends youbora.Emitter
     * @memberof youbora
     */
    constructor: function (plugin) {
      /** Parent {@link youbora.Plugin} reference. */
      this._plugin = (typeof (plugin) === 'undefined' ? null : plugin)
      this._infinity = (this._plugin && this._plugin.infinity ? this._plugin.infinity : null)

      this._initialized = false

      // Product analytics attributes
      this._productAnalyticsSettingsDefault =
      {
        autoTrackNavigation: true,
        autoTrackAttribution: true,
        highlightContentAfter: 1000,
        enableStateTracking: false,
        activeStateTimeout: 30000,
        activeStateDimension: 9,
        pageRules: []
      }

      this._productAnalyticsSettings = Object.assign({}, this._productAnalyticsSettingsDefault)
      this._page = null

      this._userState = null
      this._config = null
      this._searchQuery = null
      this._url = null

      if (typeof (window) !== 'undefined' && typeof (window.location) !== 'undefined') {
        try {
          this._url = new URL(window.location)
        } catch (ex) {
        }
      }

      this._contentHighlighted = null
      this._contentHighlightTimeout = null
      this._pendingVideoEvents = []

      if (!this._plugin) {
        Log.warn('Plugin reference unset.')
      }

      if (!this._infinity) {
        Log.warn('Infinity reference unset.')
      }

      if (!this._url) {
        Log.warn('URL reference unset.')
      }
    },

    /**
      * Initializes product analytics
      * @param {Object} productAnalyticsSettings product analytics settings
      * @param {Object} [npawtmConfigValue] configuration settings
      */
    initialize: function (productAnalyticsSettings) {
      var query

      // Load settings

      this._productAnalyticsSettings = Object.assign({}, this._productAnalyticsSettingsDefault)

      if (typeof (productAnalyticsSettings) === 'object') {
        Object.assign(this._productAnalyticsSettings, productAnalyticsSettings)
      }

      // Validate settings

      if (typeof (this._productAnalyticsSettings.autoTrackNavigation) !== 'boolean') {
        this._productAnalyticsSettings.autoTrackNavigation = this._productAnalyticsSettingsDefault.autoTrackNavigation
      }

      if (typeof (this._productAnalyticsSettings.autoTrackAttribution) !== 'boolean') {
        this._productAnalyticsSettings.autoTrackAttribution = this._productAnalyticsSettingsDefault.autoTrackAttribution
      }

      if (typeof (this._productAnalyticsSettings.highlightContentAfter) !== 'number') {
        this._productAnalyticsSettings.highlightContentAfter = this._productAnalyticsSettingsDefault.highlightContentAfter
      } else if (this._productAnalyticsSettings.highlightContentAfter < 1000) {
        Log.warn('Invalid higlightContentAfter value. Using default value instead.')
        this._productAnalyticsSettings.highlightContentAfter = this._productAnalyticsSettingsDefault.highlightContentAfter
      }

      if (typeof (this._productAnalyticsSettings.enableStateTracking) !== 'boolean') {
        this._productAnalyticsSettings.enableStateTracking = this._productAnalyticsSettingsDefault.enableStateTracking
      }

      if (typeof (this._productAnalyticsSettings.activeStateTimeout) !== 'number') {
        this._productAnalyticsSettings.activeStateTimeout = this._productAnalyticsSettingsDefault.activeStateTimeout
      }

      if (typeof (this._productAnalyticsSettings.activeStateDimension) !== 'number' || this._productAnalyticsSettings.activeStateDimension < 1 || this._productAnalyticsSettings.activeStateDimension > 9) {
        this._productAnalyticsSettings.activeStateDimension = this._productAnalyticsSettingsDefault.activeStateDimension
      }

      if (typeof (this._productAnalyticsSettings.pageRules) === 'undefined' || !Array.isArray(this._productAnalyticsSettings.pageRules)) {
        this._productAnalyticsSettings.pageRules = this._productAnalyticsSettingsDefault.pageRules
      }

      // Identify current page

      this._config = new ProductAnalyticsConfig(this._fireEvent.bind(this))
      this._identify()

      // Set as initialized

      this._initialized = true

      /*
        SPA: initialize will be called once at the beginning of the execution. Here we will start a brand new session.
        Non-SPA: initialize will be called on every page load. We must not close the previous session since, otherwise, we will have one-page sessions.
        This is why we are calling here `this._infinity.begin` instead of `this.newSession`.
      */

      if (this._infinity) {
        this._infinity.begin({
          route: (this._url === null ? '' : this._url.href),
          page: this._page
        })
      } else {
        Log.warn('Cannot open a new session since infinity is unavailable')
      }

      // Track navigation

      if (this._productAnalyticsSettings.autoTrackNavigation) {
        if (typeof (window) === 'undefined') {
          Log.warn('Cannot track user state since DOM is not available.')
        } else {
          window.removeEventListener('pageshow', this._trackNavigationCached)
          window.addEventListener('pageshow', this._trackNavigationCached.bind(this))
        }

        this._trackNavigation()
      }

      // Track attribution

      if (this._productAnalyticsSettings.autoTrackAttribution) {
        query = (this._url === null ? '' : this._url.search)
        this._trackAttribution(new URLSearchParams(query))
      }
    },

    /**
     * Identify current page
     * @private
     */

    _identify: function () {
      var regex
      var r
      var i

      // Try to identify using config approach

      if (this._config === null) {
        this._page = null
      } else {
        this._page = this._config.identify(this._url)
      }

      // Try to identify using pageRules setting

      for (i = 0; i < this._productAnalyticsSettings.pageRules.length && this._page === null && this._url !== null; i++) {
        r = this._productAnalyticsSettings.pageRules[i]

        if (typeof (r.rule) !== 'undefined' && typeof (r.page) !== 'undefined') {
          try {
            regex = new RegExp(r.rule)
          } catch (ex) {
            regex = null
            Log.warn('Error processing rule', r.rule, r.page)
          }

          if (regex !== null && this._url.pathname.match(regex) !== null) {
            this._page = this._url.pathname.replace(regex, r.page)
          }
        }
      }

      // If identication has failed, set pathname as page name

      if (this._page === null && this._url !== null) {
        this._page = this._url.pathname
      }
    },

    // ---------------------------------------------------------------------------------------------
    // ADAPTER
    // ---------------------------------------------------------------------------------------------

    /**
      * Track adapter start
      * @private
      */

    _adapterTrackStart: function () {
      if (this._initialized) {
        this.trackPlayerInteraction('start', {}, {}, true)
      }
    },

    /**
      * Execute after adapter is set to plugin
      */

    adapterAfterSet: function () {
      var adapter

      if (this._initialized) {
        // Set active state

        if (this._productAnalyticsSettings.enableStateTracking) {
          if (this._userState !== null) {
            Log.warn('userState is already initialized.')
          }

          if (!this._plugin) {
            Log.warn('Cannot track user state since plugin is unavailable.')
          } else if (!this._plugin.setOptions) {
            Log.warn('Cannot track user state since plugin options are unavailable.')
          } else {
            this._userState = new ProductAnalyticsUserState(this._productAnalyticsSettings.activeStateDimension, this._productAnalyticsSettings.activeStateTimeout, this._fireAdapterEvent.bind(this), this._plugin.setOptions.bind(this._plugin))
          }
        } else {
          this._userState = null
        }

        // Track player interaction

        adapter = this._adapterGet()

        if (adapter) {
          adapter.on('start', this._adapterTrackStart.bind(this))
        } else {
          Log.warn('Cannot bind adapter start since it is unavailable.')
        }
      }
    },

    /**
      * Execute before removing adapter from plugin
      */

    adapterBeforeRemove: function () {
      var adapter

      adapter = this._adapterGet()

      if (adapter) {
        adapter.off('start', this._adapterTrackStart)
      } else if (this._initialized) {
        Log.warn('Cannot unbind adapter start since it is unavailable.')
      }

      if (this._userState) {
        this._userState.dispose()
      }

      this._userState = null
      this._pendingVideoEvents = []
    },

    /**
      * Get adapter
      * @private
      */

    _adapterGet: function () {
      var adapter

      if (!this._plugin) {
        adapter = null
      } else if (typeof (this._plugin.getAdapter) !== 'function') {
        adapter = null
      } else {
        adapter = this._plugin.getAdapter()
      }

      return (typeof (adapter) === 'undefined' ? null : adapter)
    },

    // ---------------------------------------------------------------------------------------------
    // SESSION
    // ------------------------------------------------------------------------------------------------------------------------------

    /**
      * New user session
      */

    newSession: function () {
      var executed = false

      if (!this._initialized) {
        Log.warn('Cannot start a new session since Product Analytics is uninitialized.')
      } else if (!this._infinity) {
        Log.warn('Cannot start a new session since infinity is unavailable.')
      } else {
        // Close previous session and open a new one
        this._infinity.newSession()
        executed = true
      }

      return executed
    },

    /**
      * Ends user session
      */

    endSession: function () {
      var executed = false

      if (!this._initialized) {
        Log.warn('Cannot end session since Product Analytics is uninitialized.')
      } else if (!this._infinity) {
        Log.warn('Cannot end session since infinity is unavailable.')
      } else {
        this._infinity.fireSessionStop()
        executed = true
      }

      return executed
    },

    // ---------------------------------------------------------------------------------------------
    // LOGIN / LOGOUT
    // ---------------------------------------------------------------------------------------------

    /**
      * Successful login
      * @param {string} userId The unique identifier of the user
      * @param {string} [profileId] The unique identifier of the profile
      * @param {string} [profileType] The profile type
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    loginSuccessful: function (userId, profileId, profileType, dimensions, metrics) {
      var options

      if (!this._checkState('log in successfully')) {
        // Product Analytics is not ready for sending events
      } else if (typeof (userId) !== 'string' && typeof (userId) !== 'number') {
        Log.warn('Cannot log in successfully since userId is unavailable.')
      } else if (userId === '') {
        Log.warn('Cannot log in successfully since userId is unset.')
      } else {
        // Send user login event

        this._fireEvent(
          'User Login Successful',
          YouboraProductAnalytics.EventTypes.user,
          {
            username: userId
          },
          dimensions,
          metrics)

        // Send profile selection event (in case it has been supplied)

        if (typeof (profileId) === 'undefined' || profileId === '') {
          profileId = null
        }

        if (profileId !== null) {
          this._userProfileSelectedEvent(profileId, profileType, dimensions, metrics)
        }

        // Set the userId (and profileId) options

        options = { username: userId }

        if (profileId !== null) {
          options['user.profileId'] = profileId
        }

        this._plugin.setOptions(options)

        // Close the current session and start a new one

        this.newSession()
      }
    },

    /**
      * Login unsuccessful
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    loginUnsuccessful: function (dimensions, metrics) {
      if (!this._checkState('log in unsuccessful')) {
        // Product Analytics is not ready for sending events
      } else {
        // Send an event informing that we are closing the session because of a profile change
        this._fireEvent('User Login Unsuccessful', YouboraProductAnalytics.EventTypes.user, {}, dimensions, metrics)
      }
    },

    /**
      * Logout
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    logout: function (dimensions, metrics) {
      var options

      if (!this._checkState('log out')) {
        // Product Analytics is not ready for sending events
      } else {
        // Send an event informing that we are closing the session

        this._fireEvent('User Logout', YouboraProductAnalytics.EventTypes.user, {}, dimensions, metrics)

        // Set the userId (and profileId) options

        options = { username: null }
        options['user.profileId'] = null
        this._plugin.setOptions(options)

        // Close the current session and start a new one

        this.newSession()
      }
    },

    // ---------------------------------------------------------------------------------------------
    // PROFILE
    // ---------------------------------------------------------------------------------------------

    /**
      * User profile created
      * @param {string} profileId The unique identifier of the profile
      * @param {string} [profileType] The profile type
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    userProfileCreated: function (profileId, profileType, dimensions, metrics) {
      var dimensionsInternal

      if (!this._checkState('create user profile')) {
        // Product Analytics is not ready for sending events
      } else if (typeof (profileId) !== 'string' && typeof (profileId) !== 'number') {
        Log.warn('Cannot create user profile since profileId is unavailable.')
      } else if (profileId === '') {
        Log.warn('Cannot create user profile since profileId is unset.')
      } else {
        dimensionsInternal = this._getUserProfileDimensions(profileId, profileType)
        this._fireEvent('User Profile Created', YouboraProductAnalytics.EventTypes.userProfile, dimensionsInternal, dimensions, metrics)
      }
    },

    /**
      * User profile selected
      * @param {string} profileId The unique identifier of the profile
      * @param {string} [profileType] The profile type
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    userProfileSelected: function (profileId, profileType, dimensions, metrics) {
      var options

      if (!this._checkState('select user profile')) {
        // Product Analytics is not ready for sending events
      } else if (typeof (profileId) !== 'string' && typeof (profileId) !== 'number') {
        Log.warn('Cannot select user profile since profileId is unavailable.')
      } else if (profileId === '') {
        Log.warn('Cannot select user profile since profileId is unset.')
      } else {
        // Send an event informing that we are closing the session because of a profile change

        this._userProfileSelectedEvent(profileId, profileType, dimensions, metrics)

        // Set the profileId option

        options = { 'user.profileId': profileId }
        this._plugin.setOptions(options)

        // Close the current session and open a new one

        this.newSession()
      }
    },

    /**
      * Fire user profile selected event
      * @param {string} profileId The unique identifier of the profile
      * @param {string} [profileType] The profile type
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      * @private
      */

    _userProfileSelectedEvent: function (profileId, profileType, dimensions, metrics) {
      var dimensionsInternal

      dimensionsInternal = this._getUserProfileDimensions(profileId, profileType)
      this._fireEvent('User Profile Selected', YouboraProductAnalytics.EventTypes.userProfile, dimensionsInternal, dimensions, metrics)
    },

    /**
      * User profile deleted
      * @param {string} profileId The unique identifier of the profile
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    userProfileDeleted: function (profileId, dimensions, metrics) {
      var dimensionsInternal

      if (!this._checkState('delete user profile')) {
        // Product Analytics is not ready for sending events
      } else if (typeof (profileId) !== 'string' && typeof (profileId) !== 'number') {
        Log.warn('Cannot delete user profile since profileId is unavailable.')
      } else if (profileId === '') {
        Log.warn('Cannot delete user profile since profileId is unset.')
      } else {
        dimensionsInternal = this._getUserProfileDimensions(profileId, null)
        this._fireEvent('User Profile Deleted', YouboraProductAnalytics.EventTypes.userProfile, dimensionsInternal, dimensions, metrics)
      }
    },

    /**
     * Get user profile dimensions
     * @param {string} profileId
     * @param {string} profileType
     * @returns
     * @private
     */

    _getUserProfileDimensions: function (profileId, profileType) {
      var dimensions

      dimensions = {
        profileId: profileId
      }

      if (typeof (profileType) !== 'undefined' && profileType !== null) {
        dimensions.profileType = profileType
      }

      return dimensions
    },

    // ---------------------------------------------------------------------------------------------
    // NAVIGATION
    // ---------------------------------------------------------------------------------------------

    /**
      * Tracks navigation by route
      * @param {string} pageName The unique name to identify a page of the application.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackNavByRoute: function (route, dimensions, metrics) {
      if (!this._checkState('track navigation')) {
        // Product Analytics is not ready for sending events
      } else if (typeof route !== 'string' || route === '') {
        Log.warn('Cannot track navigation since route has not been supplied.')
      } else {
        var url

        try {
          url = new URL(route)
        } catch (ex) {
          url = null
        }

        if (url === null) {
          Log.warn('Cannot track navigation since route is invalid.')
        } else {
          this._url = url
          this._identify()
          this._trackNavigation(dimensions, metrics)
        }
      }
    },

    /**
      * Tracks navigation
      * @param {string} pageName The unique name to identify a page of the application.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackNavByName: function (pageName, dimensions, metrics) {
      if (!this._checkState('track navigation')) {
        // Product Analytics is not ready for sending events
      } else if (typeof pageName !== 'string' || pageName === '') {
        Log.warn('Cannot track navigation since page has not been supplied.')
      } else {
        this._page = pageName
        this._trackNavigation(dimensions, metrics)
      }
    },

    /**
      * Automatically tracks navigation when page is cached by browser
      * @param {Object} event
      * @private
      */

    _trackNavigationCached: function (event) {
      if (event.persisted) {
        this._trackNavigation()
      }
    },

    /**
      * Tracks navigation (either manually or automatically)
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      * @private
      */
    _trackNavigation: function (dimensions, metrics) {
      var route
      var path
      var host

      // Get UTM Options

      if (this._url === null) {
        route = ''
        path = ''
        host = ''
      } else {
        route = this._url.href
        path = this._url.pathname
        host = this._url.hostname
      }

      if (!this._checkState('track navigation')) {
        // Product Analytics is not ready for sending events
      } else {
        this._fireEvent(
          'Navigation ' + this._page,
          YouboraProductAnalytics.EventTypes.navigation,
          {
            route: path,
            routeDomain: host,
            fullRoute: route
          },
          dimensions,
          metrics)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // ATTRIBUTION
    // ---------------------------------------------------------------------------------------------

    /**
      * Tracks attribution
      * @param {string} utmSource The UTM Source parameter. It is commonly used to identify a search engine, newsletter, or other source (i.e., Google, Facebook, etc.).
      * @param {string} utmMedium The UTM Medium parameter. It is commonly used to identify a medium such as email or cost-per-click (cpc).
      * @param {string} utmCampaign The UTM Campaign parameter. It is commonly used for campaign analysis to identify a specific product promotion or strategic campaign (i.e., spring sale).
      * @param {string} utmTerm The UTM Term parameter. It is commonly used with paid search to supply the keywords for ads (i.e., Customer, NonBuyer, etc.).
      * @param {string} utmContent The UTM Content parameter. It is commonly used for A/B testing and content-targeted ads to differentiate ads or links that point to the same URL (i.e., Banner1, Banner2, etc.)
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackAttribution: function (utmSource, utmMedium, utmCampaign, utmTerm, utmContent, dimensions, metrics) {
      if (!this._checkState('track attribution')) {
        // Product Analytics is not ready for sending events
      } else if (this._productAnalyticsSettings.autoTrackAttribution) {
        Log.warn('Automatic attribution tracking is enabled: this request won\'t be processed.')
      } else {
        var params

        params = {}

        if (typeof utmSource !== 'undefined') params.utm_source = utmSource
        if (typeof utmMedium !== 'undefined') params.utm_medium = utmMedium
        if (typeof utmCampaign !== 'undefined') params.utm_campaign = utmCampaign
        if (typeof utmTerm !== 'undefined') params.utm_term = utmTerm
        if (typeof utmContent !== 'undefined') params.utm_content = utmContent

        this._trackAttribution(new URLSearchParams(params), dimensions, metrics)
      }
    },

    /**
      * Tracks attribution (either manually or automatically)
      * @param {URLSearchParams} params Object where to look for UTM params.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      * @private
      */
    _trackAttribution: function (params, dimensions, metrics) {
      // Get UTM parameters

      var utmParams = this._getUTMParams(params)
      var url = (this._url === null ? '' : this._url.href)

      // Track attribution

      if (!this._checkState('track attribution')) {
        // Product Analytics is not ready for sending events
      } else if (utmParams) {
        this._fireEvent(
          'Attribution',
          YouboraProductAnalytics.EventTypes.attribution,
          Object.assign(
            {
              url: url
            }, utmParams),
          dimensions,
          metrics)
      }
    },

    /**
     * Retrieves UTM params from querystring (utm_source, utm_medium, utm_campaign, utm_term, utm_content) and returns an object containing them but
     * with key formatted as specified.
     * @param {URLSearchParams} params Object where to look for UTM params
     * @returns {{}}
     * @private
     */
    _getUTMParams: function (params) {
      var utmParams
      var keys
      var key

      keys = {
        utm_source: 'utmSource',
        utm_medium: 'utmMedium',
        utm_campaign: 'utmCampaign',
        utm_term: 'utmTerm',
        utm_content: 'utmContent'
      }

      utmParams = {}

      // Look for UTM params and add them to the newly constructed object converting key as specified

      params.forEach(function (paramValue, paramKey) {
        key = keys[paramKey.toLowerCase()]

        if (typeof (key) !== 'undefined') {
          utmParams[key] = paramValue
        }
      })

      return (Object.keys(utmParams).length > 0 ? utmParams : null)
    },

    // ---------------------------------------------------------------------------------------------
    // SECTION
    // ---------------------------------------------------------------------------------------------

    /**
      * Section goes into viewport.
      * @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
      * @param {integer} sectionOrder The section order within the page.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackSectionVisible: function (section, sectionOrder, dimensions, metrics) {
      if (!this._checkState('track section visible')) {
        // Product Analytics is not ready for sending events
      } else if (typeof section !== 'string' || section === '') {
        Log.warn('Cannot track section visible since no section has been supplied.')
      } else if (typeof sectionOrder !== 'number' || sectionOrder < 1) {
        Log.warn('Cannot track section visible since sectionOrder is invalid.')
      } else {
        this._fireEvent(
          'Section Visible',
          YouboraProductAnalytics.EventTypes.section,
          {
            sectionOrder: sectionOrder,
            section: section
          },
          dimensions,
          metrics)
      }
    },

    /**
      * Section goes out of viewport.
      * @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
      * @param {integer} sectionOrder The section order within the page.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackSectionHidden: function (section, sectionOrder, dimensions, metrics) {
      if (!this._checkState('track section hidden')) {
        // Product Analytics is not ready for sending events
      } else if (typeof section !== 'string' || section === '') {
        Log.warn('Cannot track section hidden since no section has been supplied.')
      } else if (typeof sectionOrder !== 'number' || sectionOrder < 1) {
        Log.warn('Cannot track section hidden since sectionOrder is invalid.')
      } else {
        this._fireEvent('Section Hidden',
          YouboraProductAnalytics.EventTypes.section,
          {
            sectionOrder: sectionOrder,
            section: section
          },
          dimensions,
          metrics)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // CONTENT
    // ---------------------------------------------------------------------------------------------

    /**
      * Sends a content highlight event if content is focused during, at least, highlightContentAfter ms.
      * @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
      * @param {integer} sectionOrder The section order within the page.
      * @param {integer} column Used to indicate the column number where content is placed in a grid layout The first column is number 1.
      * @param {integer} row Used to indicate the row number where content is placed in a grid layout. The first row is number 1. In the case of a horizontal list instead of a grid, the row parameter should be set to 1.
      * @param {string} contentID The unique content identifier of the content linked.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    contentFocusIn: function (section, sectionOrder, column, row, contentID, dimensions, metrics) {
      this.contentFocusOut()

      if (!this._checkState('track content highlight')) {
        // Product Analytics is not ready for sending events
      } else if (typeof section !== 'string' || section === '') {
        Log.warn('Cannot track content highlight since no section has been supplied.')
      } else if (typeof sectionOrder !== 'number' || sectionOrder < 1) {
        Log.warn('Cannot track content highlight since sectionOrder is invalid.')
      } else if (typeof column !== 'number' || column < 1) {
        Log.warn('Cannot track content highlight since column is invalid.')
      } else if (typeof row !== 'number' || row < 1) {
        Log.warn('Cannot track content highlight since row is invalid.')
      } else if (typeof contentID !== 'string' || contentID === '') {
        Log.warn('Cannot track content highlight since no contentID has been supplied.')
      } else {
        this._contentHighlighted = { sectionOrder: sectionOrder, section: section, column: column, row: row, contentID: contentID, dimensions: dimensions, metrics: metrics }
        this._contentHighlightTimeout = setTimeout(this._trackContentHiglight.bind(this), this._productAnalyticsSettings.highlightContentAfter)
      }
    },

    /**
      * Content loses focus
      * @private
      */

    contentFocusOut: function () {
      if (this._contentHighlightTimeout !== null) {
        clearTimeout(this._contentHighlightTimeout)
      }

      this._contentHighlighted = null
      this._contentHighlightTimeout = null
    },

    /**
      * Sends a content highlight event using selected content info
      * @private
      */

    _trackContentHiglight: function () {
      if (this._contentHighlighted) {
        this._fireEvent(
          'Section Content Highlight',
          YouboraProductAnalytics.EventTypes.section,
          {
            sectionOrder: this._contentHighlighted.sectionOrder,
            section: this._contentHighlighted.section,
            column: this._contentHighlighted.column,
            row: this._contentHighlighted.row,
            contentId: this._contentHighlighted.contentID
          },
          this._contentHighlighted.dimensions,
          this._contentHighlighted.metrics)
      } else {
        Log.warn('Cannot highlight content since no content is selected')
      }
    },

    /**
      * Tracks the location of user clicks.
      * @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
      * @param {integer} sectionOrder The section order within the page.
      * @param {integer} column Used to indicate the column number where content is placed in a grid layout The first column is number 1.
      * @param {integer} row Used to indicate the row number where content is placed in a grid layout. The first row is number 1. In the case of a horizontal list instead of a grid, the row parameter should be set to 1.
      * @param {string} contentID The unique content identifier of the content linked.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackContentClick: function (section, sectionOrder, column, row, contentID, dimensions, metrics) {
      if (!this._checkState('track content click')) {
        // Product Analytics is not ready for sending events
      } else if (typeof section !== 'string' || section === '') {
        Log.warn('Cannot track content click since no section has been supplied.')
      } else if (typeof sectionOrder !== 'number' || sectionOrder < 1) {
        Log.warn('Cannot track content click since sectionOrder is invalid.')
      } else if (typeof column !== 'number' || column < 1) {
        Log.warn('Cannot track content click since column is invalid.')
      } else if (typeof row !== 'number' || row < 1) {
        Log.warn('Cannot track content click since row is invalid.')
      } else if (typeof contentID !== 'string' || contentID === '') {
        Log.warn('Cannot track content click since no contentID has been supplied.')
      } else {
        this._fireEvent(
          'Section Content Click',
          YouboraProductAnalytics.EventTypes.section,
          {
            sectionOrder: sectionOrder,
            section: section,
            column: column,
            row: row,
            contentId: contentID
          },
          dimensions,
          metrics)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // CONTENT PLAYBACK
    // ---------------------------------------------------------------------------------------------

    /**
      * Tracks when a content starts playing be it automatically or through a user interaction.
      * @param {string} contentID The unique content identifier of the content being played.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackPlay: function (contentID, dimensions, metrics) {
      var eventName = 'Content Play'
      if (!this._checkState('track play')) {
        // Product Analytics is not ready for sending events
      } else if (typeof contentID !== 'string' || contentID === '') {
        Log.warn('Cannot track play since no contentID has been supplied.')
      } else if (!this._plugin.isStarted) {
        this._pendingVideoEvents.push({ eventName: eventName, contentID: contentID, dimensions: dimensions, metrics: metrics })
      } else {
        this._trackPlayerEventsPending()
        this._trackPlayerEvent(eventName, contentID, dimensions, metrics)
      }
    },

    /**
      * Tracks content watching events.
      * TODO: add (2nd) argument to tell whether user state must be updated or not
      * @param {string} eventName The name of the interaction (i.e., Pause, Seek, Skip Intro, Skip Ads, Switch Language, etc.).
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      * @param {boolean} [startEvent] Internal param informing that current interaction is responsible of first player start
      */

    trackPlayerInteraction: function (eventName, dimensions, metrics, startEvent) {
      var eventNameComplete = 'Content Play ' + eventName
      var contentID = null

      if (!this._checkState('track player interaction')) {
        // Product Analytics is not ready for sending events
      } else if (typeof (eventName) !== 'string' || eventName === '') {
        Log.warn('Cannot track player interaction since no interaction name has been supplied.')
      } else if (!this._plugin.isStarted) {
        this._pendingVideoEvents.push({ eventName: eventNameComplete, contentID: contentID, dimensions: dimensions, metrics: metrics, startEvent: startEvent })
      } else {
        this._trackPlayerEventsPending()
        this._trackPlayerEvent(eventNameComplete, contentID, dimensions, metrics, startEvent)
      }
    },

    /**
     * Track player pending events
     */

    _trackPlayerEventsPending: function () {
      this._pendingVideoEvents.forEach(function (event) {
        this._trackPlayerEvent(event.eventName, event.contentID, event.dimensions, event.metrics, event.startEvent)
      }.bind(this))

      this._pendingVideoEvents = []
    },

    /**
     * Track player event
     * @param {*} eventName
     * @param {*} contentID
     * @param {*} dimensions
     * @param {*} metrics
     * @param {*} startEvent
     */

    _trackPlayerEvent: function (eventName, contentID, dimensions, metrics, startEvent) {
      // Prepare arguments

      if (typeof (startEvent) !== 'boolean') {
        startEvent = false
      }

      // Prepare dimensions

      var dimensionsInternal = {
      }

      if (contentID !== null) {
        dimensionsInternal.contentId = contentID
      }

      // Fire event

      this._fireAdapterEvent(
        eventName,
        dimensionsInternal,
        dimensions,
        metrics)

      // Transition state to active

      if (this._userState !== null && !startEvent) {
        this._userState.setActive(eventName)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // CONTENT SEARCH
    // ---------------------------------------------------------------------------------------------

    /**
      * Tracks search query events.
      * @param {string} searchQuery The search term entered by the user.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackSearchQuery: function (searchQuery, dimensions, metrics) {
      if (!this._checkState('track search query')) {
        // Product Analytics is not ready for sending events
      } else if (typeof searchQuery !== 'string' || searchQuery === '') {
        Log.warn('Cannot track search query since no searchQuery has been supplied.')
      } else {
        this._searchQuery = searchQuery
        this._fireEvent(
          'Search Query',
          YouboraProductAnalytics.EventTypes.search,
          {
            query: this._searchQuery
          },
          dimensions,
          metrics)
      }
    },

    /**
      * Tracks search result events.
      * @param {integer} resultCount The number of search results returned by a search query.
      * @param {String} [searchQuery] The search term entered by the user.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */
    trackSearchResult: function (resultCount, searchQuery, dimensions, metrics) {
      if (!this._checkState('track search result')) {
        // Product Analytics is not ready for sending events
      } else if (typeof resultCount !== 'number') {
        Log.warn('Cannot track search result since no resultCount has been supplied.')
      } else {
        var query = (typeof searchQuery === 'string' && searchQuery !== '' ? searchQuery : this._searchQuery)

        this._fireEvent(
          'Search Results',
          YouboraProductAnalytics.EventTypes.search,
          {
            query: query,
            resultCount: resultCount
          },
          dimensions,
          metrics)
      }
    },

    /**
      * Tracks user interactions with search results.
      * @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
      * @param {integer} sectionOrder The section order within the page.
      * @param {integer} column The content placement column. It is commonly used to indicate the column number where content is placed in a grid layout (i.e.1, 2, etc..).
      * @param {integer} row The content placement row. It is commonly used to indicate the row number where content is placed in a grid layout (i.e.1, 2, etc..).
      * @param {string} contentID The content identifier. It is used for internal content unequivocally identification (i.e., AAA000111222).
      * @param {String} [searchQuery] The search term entered by the user.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackSearchClick: function (section, sectionOrder, column, row, contentID, searchQuery, dimensions, metrics) {
      if (!this._checkState('track search click')) {
        // Product Analytics is not ready for sending events
      } else if (typeof column !== 'number') {
        Log.warn('Cannot track search click since no column has been supplied.')
      } else if (typeof row !== 'number') {
        Log.warn('Cannot track search click since no row has been supplied.')
      } else if (typeof contentID !== 'string' || contentID === '') {
        Log.warn('Cannot track search click since no contentID has been supplied.')
      } else {
        var query = (typeof searchQuery === 'string' && searchQuery !== '' ? searchQuery : this._searchQuery)

        if (typeof section !== 'string' || section === '') {
          section = 'Search'
        }

        if (typeof sectionOrder !== 'number' || sectionOrder < 1) {
          sectionOrder = 1
        }

        this._fireEvent(
          'Search Result Click',
          YouboraProductAnalytics.EventTypes.search,
          {
            sectionOrder: sectionOrder,
            section: section,
            query: query,
            column: column,
            row: row,
            contentId: contentID
          },
          dimensions,
          metrics)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // EXTERNAL APPLICATIONS
    // ---------------------------------------------------------------------------------------------

    /**
      * Tracks external app start events.
      * @param {string} appName The name of the application being used to deliver the content to the end-user (i.e., Netflix).
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackExternalAppLaunch: function (appName, dimensions, metrics) {
      if (!this._checkState('track external application launch')) {
        // Product Analytics is not ready for sending events
      } else if (typeof appName !== 'string' || appName === '') {
        Log.warn('Cannot track external application launch since no appName has been supplied.')
      } else {
        this._fireEvent(
          'External Application Launch',
          YouboraProductAnalytics.EventTypes.externalApplication,
          {
            appName: appName
          },
          dimensions,
          metrics)
      }
    },

    /**
      * Tracks external app stop events.
      * @param {string} appName The name of the application being used to deliver the content to the end-user (i.e., Netflix).
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackExternalAppExit: function (appName, dimensions, metrics) {
      if (!this._checkState('track external application exit')) {
        // Product Analytics is not ready for sending events
      } else if (typeof appName !== 'string' || appName === '') {
        Log.warn('Cannot track external application exit since no appName has been supplied.')
      } else {
        this._fireEvent(
          'External Application Exit',
          YouboraProductAnalytics.EventTypes.externalApplication,
          {
            appName: appName
          }, dimensions, metrics)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // ENGAGEMENT
    // ---------------------------------------------------------------------------------------------

    /**
      * Tracks engagement events.
      * @param {string} eventName The name of the engagement event (i.e., Share, Save, Rate, etc.).
      * @param {string} contentID The unique content identifier of the content the user is engaging with.
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackEngagementEvent: function (eventName, contentID, dimensions, metrics) {
      if (!this._checkState('track engagement event')) {
        // Product Analytics is not ready for sending events
      } else if (typeof eventName !== 'string' || eventName === '') {
        Log.warn('Cannot track engagement event since no eventName has been supplied.')
      } else if (typeof contentID !== 'string' || contentID === '') {
        Log.warn('Cannot track engagement event since no contentID has been supplied.')
      } else {
        this._fireEvent(
          'Engagement ' + eventName,
          YouboraProductAnalytics.EventTypes.engagement,
          {
            contentId: contentID
          },
          dimensions,
          metrics)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // CUSTOM EVENT
    // ---------------------------------------------------------------------------------------------

    /**
      * Track custom event
      * @param {string} eventName Name of the event to track
      * @param {Object} [dimensions] Dimensions to track
      * @param {Object} [metrics] Metrics to track
      */

    trackEvent: function (eventName, dimensions, metrics) {
      if (!this._checkState('track custom event')) {
        // Product Analytics is not ready for sending events
      } else if (typeof eventName !== 'string' || eventName === '') {
        Log.warn('Cannot track custom event since no eventName has been supplied.')
      } else {
        this._fireEvent(
          'Custom ' + eventName,
          YouboraProductAnalytics.EventTypes.custom,
          {},
          dimensions,
          metrics)
      }
    },

    // ---------------------------------------------------------------------------------------------
    // INTERNAL
    // ---------------------------------------------------------------------------------------------

    /**
      * Fires an event
      * @param {string} eventName Name of the event to be fired
      * @param {EventTypes} eventType Type of the event being tracked
      * @param {Object} dimensionsInternal Dimensions supplied by user
      * @param {Object} dimensionsUser Specific event dimensions
      * @param {Object} metrics Metrics to track
      * @private
      */

    _fireEvent: function (eventName, eventType, dimensionsInternal, dimensionsUser, metrics) {
      var dimensions
      var fired = false

      Log.notice(eventName)

      // Extract top level dimensions from custom dimensions

      dimensions = this._buildDimensions(eventType, dimensionsInternal, dimensionsUser)

      // Track event

      if (this._infinity) {
        this._infinity.fireEvent(eventName, dimensions.custom, metrics, dimensions.top)
        fired = true
      } else {
        Log.warn('Cannot fire', eventName, 'since infinity is unavailable.')
      }

      return fired
    },

    /**
      * Fires an adapter event (in case it is available)
      * @param {string} eventName Event name
      * @param {Object} dimensionsInternal Dimensions supplied by user
      * @param {Object} dimensionsUser Specific event dimensions
      * @param {Object} metrics Metrics to track
      * @private
      */

    _fireAdapterEvent: function (eventName, dimensionsInternal, dimensionsUser, metrics) {
      var dimensions
      var adapter
      var fired = false

      Log.notice(eventName)

      adapter = this._adapterGet()

      if (adapter) {
        dimensions = this._buildDimensions(YouboraProductAnalytics.EventTypes.contentPlayback, dimensionsInternal, dimensionsUser)
        adapter.fireEvent(eventName, dimensions.custom, metrics, dimensions.top)
        fired = true
      } else {
        Log.warn('Cannot fire ' + eventName + ' event since adapter is unavailable.')
      }

      return fired
    },

    /**
      * Builds a list of top level and custom dimensions
      * @param {EventTypes} eventType Type of the event being tracked
      * @param {Object} dimensionsInternal Object containing list of internal dimensions
      * @param {Object} dimensionsUser Object containing list of custom dimensions
      * @private
      */

    _buildDimensions: function (eventType, dimensionsInternal, dimensionsUser) {
      var dimensionsTopLevel
      var dimensionsCustom
      var topKeysDelete
      var topKeys

      // Build custom event dimensions

      dimensionsCustom = { page: this._page }

      if (typeof dimensionsInternal !== 'undefined') {
        dimensionsCustom = Object.assign(dimensionsCustom, dimensionsInternal)
      }

      if (typeof dimensionsUser !== 'undefined') {
        dimensionsCustom = Object.assign(dimensionsCustom, dimensionsUser)
      }

      dimensionsCustom = Object.assign(dimensionsCustom, { eventSource: 'Product Analytics', eventType: eventType })

      // List of Top Level Dimension keys

      topKeys = ['contentid', 'contentId', 'contentID', 'utmSource', 'utmMedium', 'utmCampaign', 'utmTerm', 'utmContent', 'profileId', 'profile_id', 'username']
      topKeysDelete = ['contentid', 'contentId', 'contentID', 'profileId', 'profile_id', 'username']

      // Create object with top level dimensions

      dimensionsTopLevel = {}

      for (var key in dimensionsCustom) {
        if (Object.prototype.hasOwnProperty.call(dimensionsCustom, key) && topKeys.indexOf(key) !== -1) {
          dimensionsTopLevel[key] = dimensionsCustom[key]
        }
      }

      // Remove top level dimensions from custom dimensions list

      topKeysDelete.forEach(function (key) {
        delete dimensionsCustom[key]
      })

      return { custom: dimensionsCustom, top: dimensionsTopLevel }
    },

    /**
      * Check state before sending an event
      * @param {String} message Message to show in case validation fails
      * @private
      */

    _checkState: function (message) {
      var valid
      if (!this._initialized) {
        valid = false
        Log.warn('Cannot ' + message + ' since Product Analytics is uninitialized.')
      } else if (this._infinity == null) {
        valid = false
        Log.warn('Cannot ' + message + ' since infinity is unavailable.')
      } else if (!this._infinity.isActive()) {
        valid = false
        Log.warn('Cannot ' + message + ' since session is closed.')
      } else {
        valid = true
      }
      return valid
    }
  },
  /** @lends youbora.ProductAnalyticsUserState */
  {
    // Static Members //
    /**
     * Event Type enumeration
     * @enum
     * @event
     */
    EventTypes: {
      userProfile: 'User Profile',
      user: 'User',
      navigation: 'Navigation',
      attribution: 'Attribution',
      section: 'Section',
      contentPlayback: 'Content Playback',
      search: 'Search',
      externalApplication: 'External Application',
      engagement: 'Engagement',
      custom: 'Custom Event'
    }
  }
)

module.exports = YouboraProductAnalytics
