var Log = require('../log')
var YouboraObject = require('../object')

var YouboraProductAnalyticsUserState = YouboraObject.extend(
  /** @lends youbora.ProductAnalyticsUserState.prototype */
  {
    // ------------------------------------------------------------------------------------------------------------------------------
    // INITIALIZATION
    // ------------------------------------------------------------------------------------------------------------------------------

    /**
     * This class handles Product Analytics user state management.
     *
     * @param {number} dimension Custom dimension where to store the user state.
     * @param {number} interval Interval (in ms) to transition from active to passive state.
     * @param {function} fireAdapterEvent Handler for firing adapter events
     * @param {function} pluginOptionsHandler Handler for setting plugin options
     *
     * @constructs youbora.ProductAnalyticsUserState
     * @extends youbora.Object
     * @memberof youbora
     */

    constructor: function (dimension, interval, fireAdapterEvent, pluginOptionsHandler) {
      if (typeof (fireAdapterEvent) !== 'function') {
        Log.warn('Fire Adapter Event unset')
        fireAdapterEvent = null
      }

      if (typeof (pluginOptionsHandler) !== 'function') {
        Log.warn('Plugin Options Handler unset')
        pluginOptionsHandler = null
      }

      if (typeof (dimension) !== 'number' || dimension < 1 || dimension > 9) {
        dimension = 9
      }

      if (typeof (interval) !== 'number') {
        interval = 30000
      }

      this._fireAdapterEvent = fireAdapterEvent
      this._pluginOptionsHandler = pluginOptionsHandler
      this._dimension = 'content.customDimension.' + dimension
      this._timerId = null
      this._timerInterval = interval
      this._started = false

      /*
      We must set state (customDimension) at the very beginning. Otherwise, customDimension will have "unknown" value during the time
      between page load and player first start.
      */

      this._storeState(YouboraProductAnalyticsUserState.States.PASSIVE)
    },

    /**
      * Release resources
      */

    dispose: function () {
      this._timerStop()
      this._started = false
    },

    // ------------------------------------------------------------------------------------------------------------------------------
    // STATE MANAGEMENT
    // ------------------------------------------------------------------------------------------------------------------------------

    /**
      * Set active state
      * @param {string} eventName
      * @param {boolean} playerStarted
      */

    setActive: function (eventName) {
      var state = YouboraProductAnalyticsUserState.States.ACTIVE

      if (this._state !== state) {
        this._fireEvent(state, eventName)
        this._storeState(state)
      }

      this._timerStart()
    },

    /**
      * Fire switch state event
      * @private
      */

    _fireEvent: function (state, eventName) {
      var dimensions

      Log.notice('User changing from state ' + this._state + ' to ' + state)

      dimensions = {
        newState: state,
        triggerEvent: eventName,
        stateFromTo: this._state + ' to ' + state
      }

      // Fire the event

      if (this._fireAdapterEvent) {
        switch (state) {
          case YouboraProductAnalyticsUserState.States.ACTIVE:
            this._fireAdapterEvent('Content Playback State Switch to Active', dimensions)
            break
          case YouboraProductAnalyticsUserState.States.PASSIVE:
            this._fireAdapterEvent('Content Playback State Switch to Passive', dimensions)
            break
        }
      } else {
        Log.warn('Cannot fire user state event since handler is unavailable')
      }
    },

    /**
      * Store state
      * @param {string} state
      * @private
      */

    _storeState: function (state) {
      var options

      this._state = state

      options = {}
      options[this._dimension] = this._state

      if (this._pluginOptionsHandler) {
        this._pluginOptionsHandler(options)
      } else {
        Log.warn('Cannot store user state since handler is unavailable')
      }
    },

    // ------------------------------------------------------------------------------------------------------------------------------
    // TIMER MANAGEMENT
    // ------------------------------------------------------------------------------------------------------------------------------

    /**
      * Starts interaction monitor timer
      * @private
      */

    _timerStart: function () {
      var state = YouboraProductAnalyticsUserState.States.PASSIVE

      this._timerStop()

      this._timerId = setTimeout(function () {
        this._fireEvent(state, 'timer')
        this._storeState(state)
      }.bind(this), this._timerInterval)
    },

    /**
      * Stops interaction monitor timer
      * @private
      */

    _timerStop: function () {
      if (this._timerId) {
        clearTimeout(this._timerId)
        this._timerId = null
      }
    }
  },
  /** @lends youbora.ProductAnalyticsUserState */
  {
    // Static Members //
    /**
     * Interaction state enumeration
     * @enum
     * @event
     */
    States: {
      ACTIVE: 'active',
      PASSIVE: 'passive'
    }
  }
)

module.exports = YouboraProductAnalyticsUserState
