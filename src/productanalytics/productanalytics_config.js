var Log = require('../log')
var YouboraObject = require('../object')

var YouboraProductAnalyticsConfig = YouboraObject.extend(
  /** @lends youbora.ProductAnalyticsConfig.prototype */
  {
    /**
     * This class handles Product Analytics configuration.
     *
     * @param {function} fireEvent Fire Event method
     *
     * @constructs youbora.ProductAnalyticsConfig
     * @extends youbora.Object
     * @memberof youbora
     */

    constructor: function (fireEvent) {
      this._fireEvent = (typeof (fireEvent) === 'function' ? fireEvent : null)

      // Load npawtmConfig object

      if (typeof (window) === 'undefined' || typeof (window.npawtmConfig) !== 'object') {
        // Load directly from AdminAPI?: this.loadTagManager()
        this._npawtmConfig = null
      } else {
        this._npawtmConfig = window.npawtmConfig
      }
    },

    /**
      * Identifies current page and get events to monitor when available.
      * @param {URL} URL to be identified
      * @returns {string} Identified page name / null
      */
    identify: function (url) {
      var events
      var page

      page = null
      events = []

      if (!this._npawtmConfig) {
        Log.warn('no config descriptor available')
      } else {
        page = this._getPage(url)

        if (page === null) {
          // No page identified: set default page name
          Log.warn('no page found')
        } else {
          // Page identified: look for bound events
          events = this._getEvents(page)
        }
      }

      // Bind events

      if (typeof (window) === 'undefined') {
        Log.warn('Product Analytics Config: cannot bind events since DOM is not available')
      } else if (events.length > 0) {
        window.addEventListener('load', function () {
          this._bindElements(events)
        }.bind(this))
      }

      return page
    },

    /**
      * Looks for a page matching the supplied URL (either directly or by means of a regular expression).
      * @param {URL} url URL to look for.
      * @returns {string|*} Name of the page matching the URL or null if no matches have been found.
      * @private
      */
    _getPage: function (url) {
      var urlString
      var regexInfo
      var pageInfo
      var regex
      var i

      var page = null

      if (typeof (url) === 'object' && url !== null && this._npawtmConfig && this._npawtmConfig.urls && this._npawtmConfig.regexes && this._npawtmConfig.domains && this._npawtmConfig.domains.includes(url.hostname.toLowerCase())) {
        // Direct URL lookup
        pageInfo = this._npawtmConfig.urls[url.pathname]

        if (typeof (pageInfo) !== 'undefined' && typeof (pageInfo.page) !== 'undefined') {
          page = pageInfo.page
        }

        // If URL lookup fails, start regex lookup
        urlString = url.pathname + url.search

        for (i = 0; i < this._npawtmConfig.regexes.length && page === null; i++) {
          regexInfo = this._npawtmConfig.regexes[i]
          regex = new RegExp(regexInfo.regex)

          if (urlString.match(regex)) {
            page = regexInfo.page
          }
        }
      }

      return page
    },

    /**
      * Gets events bound to the current page.
      * @param {string} page Page name to look for.
      * @private
      */
    _getEvents: function (page) {
      var events = (this._npawtmConfig && this._npawtmConfig.events ? this._npawtmConfig.events[page] : null)

      if (typeof (events) === 'undefined' || events === null) {
        events = []
      }

      return events
    },

    /**
      * Binding of element events pointed out by npawtmConfig.
      * @param {Array} events List of events to bind
      * @private
      */
    _bindElements: function (events) {
      var eventInfo
      var eventType
      var element
      var j
      var i

      for (i = 0; i < events.length; i++) {
        eventInfo = events[i]
        element = document.querySelector(eventInfo.selector)

        if (element === null) {
          Log.warn('element ' + eventInfo.selector + ' not found')
        } else if (eventInfo.events.length === 0) {
          Log.warn('element ' + eventInfo.selector + ' has no events to bind')
        } else {
          for (j = 0; j < eventInfo.events.length; j++) {
            eventType = eventInfo.events[j]
            element.npawEventInfo = eventInfo
            element.npawEventType = eventType
            element.addEventListener(eventType, function (eventData) {
              var currentEventInfo = eventData.currentTarget.npawEventInfo
              var currentEventType = eventData.currentTarget.npawEventType
              this._elementEvent(currentEventInfo, currentEventType, eventData)
            }.bind(this))
          }
        }
      }
    },

    /**
      * Tracks an event after a monitored element event has been triggered.
      * @param {Object} eventInfo
      * @param {string} eventType type of the triggered event (click...).
      * @param {Object} eventData additional event data supplied by the event listener.
      * @private
      */
    _elementEvent: function (eventInfo, eventType, eventData) {
      if (this._fireEvent) {
        this._fireEvent(
          eventInfo.name,
          eventType,
          {
            uiElement: eventInfo.name,
            eventName: eventInfo.name
          })
      } else {
        Log.warn('Cannot trigger event since no handler has been provided')
      }
    }
  }
)

module.exports = YouboraProductAnalyticsConfig
