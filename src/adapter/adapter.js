var Emitter = require('../emitter')
var Log = require('../log')
var Util = require('../util')
var version = require('../version')
var PlaybackChronos = require('./playbackchronos')
var PlaybackFlags = require('./playbackflags')
var PlayheadMonitor = require('./playheadmonitor')
var StateMonitor = require('./statemonitor')
var AdapterConstants = require('../constants/adapter')

var Adapter = Emitter.extend(
  /** @lends youbora.this.prototype */
  {
    /**
     * Main Adapter class. All specific player adapters should extend this class specifying a player
     * class.
     *
     * The Adapter works as the 'glue' between the player and YOUBORA acting both as event
     * translator and as proxy for the {@link Plugin} to get info from the player.
     *
     * @constructs Adapter
     * @extends youbora.Emitter
     * @memberof youbora
     *
     * @param {object|string} player Either an instance of the player or a string containing an ID.
     */
    constructor: function (player) {
      /** An instance of {@link FlagStatus} */
      this.flags = new PlaybackFlags()

      /** An instance of {@link ChronoStatus} */
      this.chronos = new PlaybackChronos()

      /** A dictionary to store the events fired */
      this.fireEventsStruct = {}
      this.fireEventsStruct.buffer = []
      this.fireEventsStruct.seek = []

      /** Reference to {@link PlayheadMonitor}. Helps the plugin detect buffers/seeks. */
      this.monitor = null

      /** Reference to {@link Plugin}. */
      this.plugin = null

      /** Reference to the player tag */
      this.player = null

      /** Defines if the adapter is used as adapter or adsAdapter */
      this._isAdsAdapter = null

      // Register player and event listeners
      this.setPlayer(player)

      /** Reference to the video/object tag, could be the same as the player. */
      this.tag = this.player

      Log.notice('Adapter ' + this.getVersion() + ' with Lib ' + version + ' is ready.')
    },

    /**
     * Sets a new player, removes the old listeners if needed.
     *
     * @param {Object} player Player to be registered.
     */
    setPlayer: function (player) {
      if (this.player) this.unregisterListeners()

      if (typeof player === 'string' && typeof document !== 'undefined') {
        this.player = document.getElementById(player)
      } else {
        this.player = player
      }
      this.registerListeners()
    },

    /**
     * Override to create event binders.
     * It's a good practice when implementing a new Adapter to create intermediate methods and call
     * those when player events are detected instead of just calling the `fire*` methods. This
     * will allow future users of the Adapter to customize its behaviour by overriding these
     * methods.
     *
     * @example
     * registerListeners: function () {
     *  this.player.addEventListener('start', this.onStart.bind(this))
     * },
     *
     * onStart: function (e) {
     *  this.emit('start')
     * }
     */
    registerListeners: function () {
    },

    /**
     * Override to create event de-binders.
     *
     * @example
     * registerListeners: function () {
     *  this.player.removeEventListener('start', this.onStart)
     * }
     */
    /** Unregister listeners to this.player. */
    unregisterListeners: function () {
    },

    /**
     * This function disposes the currend adapter, removes player listeners and drops references.
     */
    dispose: function () {
      // Stop Monitor and Network monitor
      this.stopMonitor()
      this.stopReadyStateMonitor()
      // Fire Stop
      this.fireStop()
      // Unregister listener
      this.unregisterListeners()
      this.player = null
      this.tag = null
    },

    /**
     * Creates a new {@link PlayheadMonitor} at this.monitor.
     *
     * @param {bool} monitorBuffers If true, it will monitor buffers.
     * @param {bool} monitorSeeks If true, it will monitor seeks.
     * @param {number} [interval=800] The interval time in ms.
     */
    monitorPlayhead: function (monitorBuffers, monitorSeeks, interval) {
      this.stopMonitor()
      var type = 0
      if (monitorBuffers) type |= PlayheadMonitor.Type.BUFFER
      if (monitorSeeks) type |= PlayheadMonitor.Type.SEEK

      if (!this.monitor || !this.monitor._timer.isRunning) {
        this.monitor = new PlayheadMonitor(this, type, interval)
      } else {
        this.monitor.skipNextTick()
      }
    },

    stopMonitor: function () {
      if (this.monitor) this.monitor.stop()
    },

    /**
     *
     * @param {number} [intervalMilliseconds=300]
     */
    monitorReadyState: function (intervalMilliseconds) {
      this.stopReadyStateMonitor()
      this.stateMonitor = new StateMonitor(this, intervalMilliseconds)
    },

    /**
     * Start ReadyState monitor
     */
    startReadyStateMonitor: function () {
      if (this.stateMonitor) this.stateMonitor.start()
    },

    /**
     * Stop ReadyState monitor
     */
    stopReadyStateMonitor: function () {
      if (this.stateMonitor) this.stateMonitor.stop()
    },

    /**
     * Check readyState video properties
     * @param readyState
     * @param triggeredEvent
     */
    checkReadyState: function (readyState, triggeredEvent) {
      try {
        if (this.plugin && this.plugin.getReadyStateMonitorEnabled()) {
          if (readyState) {
            if (readyState > 3 && !this.flags.isSeeking) {
              if (this.flags.isBuffering) {
                this.fireBufferEnd({}, triggeredEvent + '-readyState')
              } else if (!this.flags.isJoined) {
                this.fireJoin({}, triggeredEvent + '-readyState')
              }
            } else if (readyState < 4 && !this.flags.isBuffering) {
              this.fireBufferBegin({}, false, triggeredEvent + '-readyState', true)
            }
          }
        }
      } catch (e) {
        Log.error('Can\'t check readyState property correctly')
      }
    },

    // GETTERS //

    /** Override to return current playhead of the video */
    getPlayhead: function () {
      return null
    },

    /** Override to return video duration */
    getDuration: function () {
      return null
    },

    /** Override to return current bitrate */
    getBitrate: function () {
      return null
    },

    /** Override to return total downloaded bytes */
    getTotalBytes: function () {
      return null
    },

    /** Override to return title */
    getTitle: function () {
      return null
    },

    /** Override to return resource URL. */
    getResource: function () {
      return null
    },

    /** Override to return player version */
    getPlayerVersion: function () {
      return null
    },

    /** Override to return player's name */
    getPlayerName: function () {
      return null
    },

    /** Override to return adapter version. */
    getVersion: function () {
      return version + '-generic-js'
    },

    /** Override to return video object */
    getVideoObject: function () {
      return null
    },

    // FLOW //

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent init if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    fireInit: function (params, triggeredEvent) {
      if (this.plugin) this.plugin.fireInit(params, triggeredEvent)
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    fireStart: function (params, triggeredEvent) {
      if (this.plugin && this.plugin.backgroundDetector && this.plugin.backgroundDetector.canBlockStartCalls()) {
        return null
      }
      if (!this.flags.isStarted) {
        this.flags.isStarted = true
        this.chronos.total.start()
        this.chronos.join.start()
        if (triggeredEvent) {
          params = params || {}
          params.triggeredEvents = [triggeredEvent]
        }
        this.startReadyStateMonitor()
        this.emit(AdapterConstants.Event.START, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    fireJoin: function (params, triggeredEvent) {
      if (!this.flags.isJoined && !this.flags.isStarted && !this._isAds() && this.plugin && this.plugin.isInitiated) {
        this.fireStart({}, triggeredEvent)
      }
      if (this.flags.isStarted && !this.flags.isJoined) {
        this.flags.isStarted = true
        if (this.monitor) this.monitor.start()
        this.flags.isJoined = true
        this.chronos.join.stop()

        params = params || {}
        if (triggeredEvent) {
          params.triggeredEvents = [triggeredEvent]
        }

        if (this._shouldSplitView()) {
          params.viewIndex = this.plugin.viewTransform._viewIndex
        }

        this.emit(AdapterConstants.Event.JOIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    firePause: function (params, triggeredEvent) {
      // First, discard false buffers triggered by state changed before pause
      this._discardFalseBuffers()
      if (this.flags.isBuffering) {
        this.fireBufferEnd(null, 'firePauseCall')
      }
      if (this.flags.isJoined && !this.flags.isPaused) {
        this.flags.isPaused = true

        this.chronos.pause.start()

        params = params || {}
        if (triggeredEvent) {
          params.triggeredEvents = [triggeredEvent]
        }

        if (this._shouldSplitView()) {
          params.viewIndex = this.plugin.viewTransform._viewIndex
        }

        this.emit(AdapterConstants.Event.PAUSE, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    fireResume: function (params, triggeredEvent) {
      // First, discard false buffers triggered by state changed before resume
      this._discardFalseBuffers()
      if (this.flags.isJoined && this.flags.isPaused) {
        this.flags.isPaused = false

        this.chronos.pause.stop()

        if (this.monitor) this.monitor.skipNextTick()

        params = params || {}
        if (triggeredEvent) {
          params.triggeredEvents = [triggeredEvent]
        }

        if (this._shouldSplitView()) {
          params.viewIndex = this.plugin.viewTransform._viewIndex
        }

        this.emit(AdapterConstants.Event.RESUME, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {boolean} [convertFromSeek=false] If true, will convert current seek to buffer.
     * @param {string} [triggeredEvent]
     * @param {boolean} [triggeredByStateProperty]
     */
    fireBufferBegin: function (params, convertFromSeek, triggeredEvent, triggeredByStateProperty) {
      triggeredByStateProperty = triggeredByStateProperty || false
      if (this.flags.isJoined && !this.flags.isBuffering) {
        if (this.flags.isSeeking) {
          if (convertFromSeek) {
            Log.notice('Converting current buffer to seek')

            this.chronos.buffer = this.chronos.seek.clone()
            this.chronos.seek.reset()

            this.flags.isSeeking = false
          } else {
            return
          }
        } else {
          this.chronos.buffer.start()
        }

        try {
          // Create new buffer empty structure to add event to detect buffer begins
          this.fireEventsStruct.buffer = []
          if (triggeredEvent) {
            this.fireEventsStruct.buffer.push(triggeredEvent)
          } else {
            this.fireEventsStruct.buffer.push('undefinedEvent')
          }
        } catch (e) {}

        this.flags.isBuffering = true
        this.flags.isVideoStateBuffering = triggeredByStateProperty

        if (this._shouldSplitView()) {
          params = params || {}
          params.viewIndex = this.plugin.viewTransform._viewIndex
        }

        this.emit(AdapterConstants.Event.BUFFER_BEGIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    fireBufferEnd: function (params, triggeredEvent) {
      if (this.flags.isJoined && this.flags.isBuffering) {
        try {
          // Add triggered event to detect buffer ends
          if (triggeredEvent) {
            this.fireEventsStruct.buffer.push(triggeredEvent)
          } else {
            this.fireEventsStruct.buffer.push('undefinedEvent')
          }
        } catch (e) {}

        params = params || {}
        params.triggeredEvents = this.fireEventsStruct.buffer

        if (this._shouldSplitView()) {
          params.viewIndex = this.plugin.viewTransform._viewIndex
        }

        this.cancelBuffer()
        this.emit(AdapterConstants.Event.BUFFER_END, { params: params })
      }
    },

    /**
     * Discard false buffer events from state changed
     * @private
     */
    _discardFalseBuffers: function () {
      try {
        if (this.flags.isBuffering && this.flags.isVideoStateBuffering && (this._getDeltaBufferTime() <= 100)) {
          this.cancelBuffer()
        }
      } catch (e) {}
    },

    /**
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    cancelBuffer: function (params) {
      if (this.flags.isJoined && this.flags.isBuffering) {
        this.flags.isBuffering = false
        this.flags.isVideoStateBuffering = false

        this.chronos.buffer.stop()

        if (this.monitor) this.monitor.skipNextTick()
      }
    },

    /**
     * Get Delta buffer time
     * @returns {number|number|*}
     * @private
     */
    _getDeltaBufferTime: function () {
      if (this.chronos && this.chronos.buffer) {
        return this.chronos.buffer.getDeltaTime(false)
      }
      return 0
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    fireStop: function (params, triggeredEvent) {
      if (this._isAds() || (this.plugin && this.plugin._isStopReady())) {
        if ((this._isAds() && this.flags.isStarted) ||
          (!this._isAds() && (this.flags.isStarted || (this.plugin && this.plugin.isInitiated)))
        ) {
          // Stop Monitor and Network monitor
          this.stopMonitor()
          this.stopReadyStateMonitor()

          this.flags.reset()
          this.chronos.total.stop()
          this.chronos.join.reset()
          this.chronos.pause.stop()
          this.chronos.buffer.stop()
          this.chronos.seek.stop()

          params = params || {}
          if (triggeredEvent) {
            params.triggeredEvents = [triggeredEvent]
          }

          if (this._shouldSplitView()) {
            params.viewIndex = this.plugin.viewTransform._viewIndex
          }

          this.emit(AdapterConstants.Event.STOP, { params: params })

          this.chronos.pause.reset()
          this.chronos.buffer.reset()
          this.chronos.seek.reset()
          this.chronos.viewedMax.splice(0, this.chronos.viewedMax.length)
        }
      }
    },

    setIsAds: function (value) {
      this._isAdsAdapter = value
    },

    _isAds: function () {
      return this._isAdsAdapter
    },

    /**
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {string} [triggeredEvent]
     */
    fireCasted: function (params, triggeredEvent) {
      if (!params) params = {}
      params.casted = true
      this.fireStop(params, triggeredEvent)
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
     * @param {String} [msg] Error Message
     * @param {Object} [metadata] Object defining error metadata
     * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
     * @param {string} [triggeredEvent]
     */
    fireError: function (code, msg, metadata, level, triggeredEvent) {
      var params = Util.buildErrorParams(code, msg, metadata, level)
      if (params.code) {
        delete params.code
      }

      params = params || {}
      if (triggeredEvent) {
        params.triggeredEvents = [triggeredEvent]
      }

      if (this._shouldSplitView()) {
        params.viewIndex = this.plugin.viewTransform._viewIndex
      }

      var options = this.plugin ? this.plugin.options : {}
      if (typeof params.errorCode !== 'undefined' && options['errors.ignore'] &&
      options['errors.ignore'].indexOf(params.errorCode.toString()) > -1) {
        // ignore error
      } else {
        this.emit(AdapterConstants.Event.ERROR, { params: params })
        if (typeof params.errorCode !== 'undefined' && options['errors.fatal'] &&
          options['errors.fatal'].indexOf(params.errorCode.toString()) > -1) {
          this.fireStop({}, triggeredEvent)
        }
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
     * @param {String} [msg] Error Message
     * @param {Object} [metadata] Object defining error metadata
     * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
     * @param {string} [triggeredEvent]
     */
    fireFatalError: function (code, msg, metadata, level, triggeredEvent) {
      var options = this.plugin ? this.plugin.options : {}
      if (typeof code !== 'undefined' && options['errors.ignore'] &&
      options['errors.ignore'].indexOf(code.toString()) > -1) {
        // ignore error
      } else {
        if (this.monitor) this.monitor.stop()
        this.fireError(code, msg, metadata, level, triggeredEvent)
        if (typeof code !== 'undefined' && options['errors.nonFatal'] &&
          options['errors.nonFatal'].indexOf(code.toString()) > -1) {
          // no stop
        } else {
          this.fireStop({}, triggeredEvent)
        }
      }
    },
    _shouldSplitView: function () {
      return (this.plugin && this.plugin.options && this.plugin.options['session.splitViews'])
    }
  },
  {
    /** @lends youbora.Adapter */
    // Static Memebers //

    /**
     * List of events that could be fired
     * @enum
     * @event
     */
    Event: AdapterConstants.Event
  }
)

Util.assign(Adapter.prototype, require('./adapter+ads'))
Util.assign(Adapter.prototype, require('./adapter+content'))

module.exports = Adapter
