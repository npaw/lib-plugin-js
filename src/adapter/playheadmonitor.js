var YouboraObject = require('../object')
var Timer = require('../timer')
var Chrono = require('../chrono')

var PlayheadMonitor = YouboraObject.extend(
  /** @lends youbora.PlayheadMonitor.prototype */
  {
    /**
     * This class periodically checks the player's playhead in order to infer buffer and/or seek
     * events.
     *
     * Instances of this class are bounded to an {@link Adapter} and fires its buffer and seek
     * start and end methods.
     *
     * In order to use this feature, {@link Adapter#monitorPlayhead} should be used.
     *
     * @constructs PlayheadMonitor
     * @extends youbora.YouboraObject
     * @memberof youbora
     *
     * @param {Adapter} adapter Adapter to monitor. Must have getPlayhead defined.
     * @param {PlayheadMonitor.Type} [type=NONE]
     * Which metric to monitor seek and/or buffer.
     * Use bitwise operators to join both values (Type.BUFFER | Type.SEEK)
     * @param {int} [interval=800] How many ms will wait between progress. -1 to disable.
     */
    constructor: function (adapter, type, interval) {
      this._adapter = adapter
      this._seekEnabled = type & PlayheadMonitor.Type.SEEK
      this._bufferEnabled = type & PlayheadMonitor.Type.BUFFER
      interval = interval || 800

      this._chrono = new Chrono()
      this._lastPlayhead = 0

      if (interval > 0) {
        this._timer = new Timer(this.progress.bind(this), interval)
      }
    },

    /**
     * Start interval checks.
     */
    start: function () {
      this.stop()
      if (this.canBeUsed()) this._timer.start()
    },

    /**
     * Stop interval checks.
     */
    stop: function () {
      this._lastPlayhead = 0
      if (this._timer) this._timer.stop()
    },

    skipNextTick: function () {
      this._lastPlayhead = 0
    },

    /**
     * Call this method at every tick of timeupdate/progress.
     * If you defined an interval, do not fire this method manually.
     */
    progress: function () {
      // Reset timer
      var deltaTime = this._chrono.stop()
      this._chrono.start()

      // Define thresholds
      var bufferThreshold = deltaTime * PlayheadMonitor.kBUFFER_THRESHOLD_RATIO
      var seekThreshold = deltaTime * PlayheadMonitor.kSEEK_THRESHOLD_RATIO

      if (this._adapter.getPlayrate && this._adapter.getPlayrate() && this._adapter.getPlayrate() !== 1) {
        bufferThreshold *= this._adapter.getPlayrate()
        seekThreshold *= this._adapter.getPlayrate()
      }

      // Calculate diff playhead
      var currentPlayhead = this._getPlayhead()
      var diffPlayhead = Math.abs(this._lastPlayhead - currentPlayhead) * 1000

      // youbora.Log.debug('curr: ' + currentPlayhead + ' last: ' + this._lastPlayhead + ' diff: ' + diffPlayhead)

      if (diffPlayhead < bufferThreshold) {
        // Playhead is stalling > buffer
        if (this._bufferEnabled &&
          this._lastPlayhead > 0 &&
          !this._adapter.flags.isPaused &&
          !this._adapter.flags.isSeeking
        ) {
          this._adapter.fireBufferBegin(null, false, 'playheadMonitor')
        }
      } else if (diffPlayhead > seekThreshold) {
        // Playhead has jumped > seek
        if (this._seekEnabled && this._lastPlayhead > 0) {
          this._adapter.fireSeekBegin(null, false, 'playheadMonitor')
        }
      } else {
        // Healthy
        if (this._seekEnabled) {
          this._adapter.fireSeekEnd(null, 'playheadMonitor')
        }
        if (this._bufferEnabled) {
          this._adapter.fireBufferEnd(null, 'playheadMonitor')
        }
      }

      // Update Playhead
      this._lastPlayhead = currentPlayhead
    },

    /**
     * Returns if the monitor can be used or not.
     * Enabled by default, except the case where the content is live and the option 'content.isLive.noMonitor' is true.
     * @public
     * @returns {boolean} If the monitor can be used or not.
     */
    canBeUsed: function () {
      var plugin = this._adapter.plugin
      if (plugin && plugin.getPlayheadMonitorEnabled() && (this._seekEnabled || this._bufferEnabled)) {
        return plugin.getIsLive() ? !plugin.options['content.isLive.noMonitor'] : true
      } else {
        return false
      }
    },

    /**
     * Returns adapter's playhead. Override to add a custom playhead getter.
     * @private
     * @returns {double} Playhead in seconds
     */
    _getPlayhead: function () {
      return this._adapter.getPlayhead()
    }
  },
  /** @lends youbora.PlayheadMonitor */
  {
    // Static methods

    /**
     * Enum for monitoring type
     * @enum
     */
    Type: {
      /** Would not monitor */
      NONE: 0,
      /** Sends buffer-begin/end */
      BUFFER: 1,
      /** Sends seek-begin/end */
      SEEK: 2
    },

    /** Buffer threshold */
    kBUFFER_THRESHOLD_RATIO: 0.5,

    /** Seek threshold */
    kSEEK_THRESHOLD_RATIO: 2
  })

module.exports = PlayheadMonitor
