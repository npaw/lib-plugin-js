/**
 * List of ad insertion types
 */
var InsertionType = {
  ClientSide: 'csai',
  ServerSide: 'ssai'
}
module.exports = InsertionType
