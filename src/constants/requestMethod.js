/**
 * List of Request Methods
 *   - GET
 *   - POST
 */
var RequestMethod = {
  GET: 'get',
  POST: 'post'
}
module.exports = RequestMethod
