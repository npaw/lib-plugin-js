## [6.8.59] - 2024-10-08
### Added
- [PROD-981]
  - Product Analytics: added `loginSuccessful`, `loginUnsuccessful` and `logout` methods. Also added `userProfileCreated`, `userProfileSelected` and `userProfileDeleted`.
  - `profileId` is sent on `infinity/session/start`, `infinity/session/nav`, `/data`, `/init`, `/start` and `/error` events (the same events where `username` is included).
### Modified
- Product Analytics: `trackNavByName` and `trackNavByRoute` no longer start a session or send a `session/nav` event.
- Product Analytics: `initialize` starts a session.
- Product Analytics: tracking methods fail when called within a closed session.
- Product Analytics: product analytics object is no longer destroyed after closing the session.

## [6.8.58] - 2024-10-07
### Fixed
- Product Analytics: fixed ECMAScript 5 incompatibility.

## [6.8.57] - 2024-07-10
### Added
- Product Analytics: add `trackNavByRoute` method.
- Product Analytics: add `section` and `sectionOrder` arguments to `trackSearchClick`.
### Modified
- Product Analytics: rename `trackNavigation` as `trackNavByName`.
- Product Analytics: user state is `passive` on video start.

## [6.8.56] - 2024-05-20
### Added
- Support for both `boolean` and `string` values in the `getIsLive()` function.

## [6.8.55] - 2024-05-09
### Added
- Product Analytics: call `trackNavigation` automatically when a cached page is visited and `autoTrackNavigation` is enabled.
- [PROD-889] Product Analytics: add `setUserProfile` method to allow collecting profile identifiers.
### Fixed
- TypeError when `session.splitViews` due to falsy params.
- Product Analytics: dispose user state upon adapter removal.

## [6.8.54] - 2024-04-04
### Added
- Product Analytics: added sectionOrder argument to section related tracking methods.
- Product Analytics: ensure Product Analytics is initialized before processing any request.
### Fixed
- Product Analytics: ensure Product Analytics is initialized before binding the adapter start event.

## [6.8.53] - 2024-03-22
### Added
- Added option `session.mergeViews` to enable the view splitting when some errors are stashed in the request.

## [6.8.52] - 2024-03-06
### Fixed
- Infinity: custom video events should report play head.

## [6.8.51] - 2024-01-08
### Added
- Product Analytics: initial version.
### Fixed
- Infinity: read `infinityStarted` value from local storage before checking whether to `fireSessionStart` or `fireNav`. Otherwise, `infinityStarted` is always false and no `fireNav` calls are made.

## [6.8.50] - 2023-08-28
### Fixed
- Update documentation URLs on the `options.js` file

## [6.8.49] - 2023-07-04
### Added
- Support set 'cdnbalancer.uuid' on options object, and deprecate smartswitch legacy options

## [6.8.48] - 2023-06-23
### Added
- ContentID available for switching dimensions (to inform on entities parameter)

## [6.8.47] - 2023-06-21
### Added
- If you call `begin` when infinity is started, the lib will fire navigation event

## [6.8.46] - 2023-03-24
### Added
- On startListener, prioritize get resource from plugin (to get if options have it configured correctly)

## [6.8.45] - 2023-03-22
### Added
- On startListener, if exists resourceTransform, process it gain because, in the fireInit, we didn't have the resource

## [6.8.44] - 2023-03-08
### Added
- On sessionStart send screenResolution parameter

## [6.8.43] - 2023-03-05
### Added
- Support set a string array element in parse.cdnNameHeader option (to define more headers to parse)

## [6.8.42] - 2023-02-27
### Added
- On streamingProtocol, support new DVB options: DVBC, DVBT, DVBT2

## [6.8.41] - 2023-02-01
### Added
- On streamingProtocol, support MULTICAST and DVB options

## [6.8.40] - 2023-01-30
### Added
- Remove some default values from some methods

## [6.8.39] - 2022-12-23
### Added
- Adding triggered events support for init event

## [6.8.38] - 2022-12-14
### Added
- On HLSParser, set transport format por m4a files

## [6.8.37] - 2022-12-14
### Added
- On Infinity begin, support send boolean to force open a new session

## [6.8.36] - 2022-12-13
### Added
- On HLSParser, add support to parsing m4a files

## [6.8.35] - 2022-11-10
### Added
- Adding stateReady monitor/check to support new ways to detect buffers
- Add options to disable playhead and stateReady monitors/checks
- Adding triggeredEvents on the logs for buffer and seek events

## [6.8.34] - 2022-10-27
### Fixed
- Fix issue with events don't have the valid POST message when plugin retry it

## [6.8.33] - 2022-10-24
### Added
- Adding triggered events parameter in the other fireEvents

## [6.8.32] - 2022-10-21
### Added
- Create a constant RequestMethod to define method options value
- Post Method is available for infinity/session events

## [6.8.31] - 2022-10-17
### Added
- Remove generated context (always default value)
- When infinity begin, send always start session event instead of nav event
### Fixed
- Fix issue to avoid use the same code to different sessions (after fire stop session)

## [6.8.30] - 2022-10-12
### Added
- Support send requests with POST method, and parameters on the request body

## [6.8.29] - 2022-10-12
### Added
- On buffer and seek events, send player/adapter events triggered to provoke plugin event
### Fixed
- Check timer is running to throw the next tick method

## [6.8.28] - 2022-08-22
### Added
- `content.autodetect.language` option
- `content.autodetect.subtitles` option

## [6.8.27] - 2022-07-20
### Fixed
- Simplified host data recovery from storages

## [6.8.26] - 2022-07-18
### Fixed
- Jointime detection when removing adsadapter object after having prerolls

## [6.8.25] - 2022-07-07
### Added
- `user.privacyProtocol` option

## [6.8.24] - 2022-06-14
### Fixed
- Typo on p2p data object

## [6.8.23] - 2022-06-14
### Added
- More P2P metrics when using NPAW's CDN balancer SDK

## [6.8.22] - 2022-06-07
### Added
- Checks to prevent issues when session storage on cookies set variables as `undefined`

## [6.8.21] - 2022-05-26
### Added
- CDN download time parameter for NPAW's CDN balancer SDK

## [6.8.20] - 2022-05-20
### Added
- Support for NPAW's CDN balancer / active switching / P2P client analytics reporting: `cdnBalancerResponseUUID`, global CDN and P2P data, and specific CDN use stats.

## [6.8.19] - 2022-05-12
### Added
- `parse.manifest.auth` option

## [6.8.18] - 2022-04-28
### Added
- `parse.cdnNodeHeader` option, to parse from additional requests the host name providing content when using a custom balancer

## [6.8.17] - 2022-04-04
### Fixed
- Workarounds for cookie storage when enabled and the device(s) are unable to work consistently when deleting fields
### Added
- `ad.duration` option

## [6.8.16] - 2022-03-31
### Fixed
- Minor changes related to sessions interacting with different storages

## [6.8.15] - 2022-03-22
### Fixed
- Case where dash parser wasnt working as expected so it returned wrong (non url) values

## [6.8.14] - 2022-03-11
### Fixed
- Uncontrolled exception when parse hls class tried to parse an invalid resource

## [6.8.13] - 2022-03-03
### Added
- `content.package` option

## [6.8.12] - 2022-02-02
### Added
- `authToken` and `authType` options to send Authorization header in the requests to nqs.
### Fixed
- Ad not being closed when plugin.fireStop() was called
- PlaybackType ignoring the default false value of getIsLive: it will be empty as default value instead of VoD.

## [6.8.11] - 2022-01-10
### Added
- `NosOtt` as CDN node detector descriptor.
### Removed
- `content.package` option

## [6.8.10] - 2021-11-30
### Added
- Added `adNumberInBreak` parameter in ad events.

## [6.8.9] - 2021-11-23
### Fixed
- Added empty path to cookies

## [6.8.8] - 2021-11-22
### Fixed
- Fixed cookies storage

## [6.8.7] - 2021-11-11
### Added
- Duration of the content can be reported if the content is live if it is set as an option, using `content.duration`
### Fixed
- Fixed cookies storage
- `device.EDID` format correction if an array of byes is set

## [6.8.6] - 2021-11-08
### Added
- Now cookies storage uses host-shared location
- `device.EDID` option

## [6.8.5] - 2021-10-25
### Added
- `forceCookies` option to use them instead of localStorage and sessionStorage

## [6.8.4] - 2021-10-04
### Added
- `player` parameter on `adStart` `adError` and `adInit` events
- Automatic triggering of `pause` event when triggering `adBreakStart` to prevent overlapping metrics with content and ads
- Url obtained directly from the player in `getURLToParse` reported as `parsedResource` if there is no response from `parse.manifest` feature or is disabled

## [6.8.3] - 2021-09-29
### Removed
- `ad.blockDetection` option and feature
### Added
- `ad.blockerDetected` option. Boolean to report if adblocker is detected or not

## [6.8.2] - 2021-08-20
### Fixed
- Mls parser case where the url is not the last parameter in the same line

## [6.8.1] - 2021-08-19
### Added
- `content.customDimensions` option, as an alternative way to use custom dimensions like `content.customDimension.1`

## [6.8.0] - 2021-08-09
### Added
- `content.isLive.noMonitor` option and functionality

## [6.7.42] - 2021-08-03
### Added
- Additional case for `parse.manifest` when manifest is redirected and can't access to Location header

## [6.7.41] - 2021-07-23
### Added
- Splitted logic for additional parameters in pings (for content and ads)

## [6.7.40] - 2021-06-18
### Added
- Parser for hls manifests with multiple variables instead of only a url
- `ObfuscateIP` parameter for session start

## [6.7.39] - 2021-06-08
### Fixed
- Refactor for usehaders variable while using parsecdn to avoid endless loops

## [6.7.38] - 2021-06-04
### Fixed
- No limit on recursivity depth for parse manifest (dash, location tag case), now the limit is 3
- Checking that the parsed manifest (dash) is not requesting itself on location tag
- Improved dash location tag parser performance
### Added
- Encoding correction on location tags for parse manifest (dash)

## [6.7.37] - 2021-06-03
### Fixed
- Parse CDN for akamai now uses HEAD instead of GET request

## [6.7.36] - 2021-06-02
### Fixed
- ES6ish code added by webpack removed
- Check for `window.addEventListener` before using it
### Added
- Retry for cdn parser without headers when the headers force the request to fail

## [6.7.35] - 2021-05-26
### Fixed
- Dash manifest format detection
### Added
- Cdn parser using case insensitive headers

## [6.7.34] - 2021-05-07
### Fixed
- Dash manifest parser issues with regex

## [6.7.33] - 2021-05-03
### Added
- `referral` option, to set previous page url.

## [6.7.32] - 2021-04-26
### Added
- Ad insertion type dimension and adapter getter `getAdInsertionType`
- Ad insertion type constants
### Fixed
- Crash when utils.builderrorparams receives null
- referer option with wrong name, now is `referer`
- `ManifestErrror` constants
- Small refactor for getExpectedBreaks nad getExpectedAds
- `getGivenAds` using wrong option

## [6.7.31] - 2021-04-06
### Added
- `param1` to `param20` for session start event.
- Minor code refactor for ad viewability calculation.

## [6.7.30] - 2021-03-23
### Added
- `.cmfv` as video segment format, for detectors and reporting.
- Excluded `creativeId` for filtering repeated ad errors (flood preventing exception for consecutive errors if this happens for different ads)

## [6.7.29] - 2021-03-04
### Added
- Metrics/session metrics on `stop` and session `stop` events

## [6.7.28] - 2021-02-26
### Fixed
- Issue when parse options are enabled and players fail to return a resource

## [6.7.27] - 2021-02-05
### Added
- `linkedViewId` option
- 4th parameter for additional parameters on `fireEvent` method

## [6.7.26] - 2021-01-15
### Added
- Edgecast CDN added to the list of CDNs we can detect

## [6.7.25] - 2020-12-11
### Added
- `ip`, `isp`, `userType` and `connectionType` parameters on session start request.

## [6.7.24] - 2020-11-27
### Added
- `device.id` option.

## [6.7.23] - 2020-11-20
### Fixed
- Removed `Object.assign` and `array.includes` references
### Added
- Extra delay to loadtime metrics event

## [6.7.22] - 2020-10-30
### Fixed
- Checked that `largest-contentful-paint` is a valid entry type for PerformanceObserver.

## [6.7.21] - 2020-10-28
### Fixed
- Adapter chronos reset when calling fireStop from the plugin and not from the adapter. This was causing issues for the following views if the previous ended during an unfinished buffer, seek or pause.

## [6.7.20] - 2020-10-26
### Added
- Fix for use of `newSession` instead of begin when no session was created before, to avoid issues ending in having no host to send the requests.

## [6.7.19] - 2020-10-26
### Added
- `fistContentfulPaint`, `LargestContentfulPaint` metrics added on session events.

## [6.7.18] - 2020-10-19
### Added
- Check to not report latency while playing VOD content.

## [6.7.17] - 2020-09-09
### Fixed
- Session restoring when calling newSession, with a new session code.

## [6.7.16] - 2020-09-02
### Added
- `errors.fatal` , `errors.nonFatal` and `errors.ignore` options.

## [6.7.15] - 2020-08-19
### Added
- Removed `Object.values` calls, for compatibility.

## [6.7.14] - 2020-08-03
### Added
- `scrollDepth` metric. Reported in `nav` events.

## [6.7.13] - 2020-07-21
### Fixed
- `getEntriesByType` checked and trycatched access

## [6.7.12] - 2020-07-20
### Added
- Playhead reported on error event
- CdnSwitch detection feature: `parse.cdnTTL` and `parse.cdnSwitchHeader` options

## [6.7.11] - 2020-06-29
### Fixed
- `parse.cdnNode` enabled when `parse.manifest` disabled was not working
- When fireJoin() triggered /start, /jointime was sending start parameters.
- Now `parsedResource`, `transportFormat`, `nodeHost`, `nodeType`, `nodeTypeString` values can be forced on fireInit() and fireStart()

## [6.7.10] - 2020-06-22
### Fixed
- Options hotfix, ES5: property shorthand removed

## [6.7.9] - 2020-06-22
### Added
- `content.transportFormat` option
- `transportFormat` value in init, start and error, the possible values are MP4 and TS
- `transportFormat` autodetection for dash and hls using `parse.manifest`
- `getManifest` method in adapter, to get mid level manifests or video segments url from the player, so `parse.manifest` can skip a top level manifest request
- `getFastDataConfig` method in the plugin, so it returns the fastdata response
- Third optional parameter for plugin constructor `dataReq`, to send a fastData response so two plugin instances can report events to the same session
### Fixed
- Check to prevent crashes in browsers without performance.getEntriesByType` method
- Check for multiple plugin instances created at the same time to share nqs host. Before they were sharing sessionRoot only.
- Now `streamingProtocol` can contain only specific values: HDS, HLS, MSS, DASH, RTMP, RTP, RTSP

## [6.7.8] - 2020-06-15
### Added
- Now hls resource parser works with video segments sharing the same route or only the domain with the manifest
### Fixed
- Removed negative and decimal values for page load metrics
- Session events being reported when shouldnt be allowed: multiple starts with the same code, navigations and events without session started, multiple stops...
- Simplified and optimized session flags logic
- Removed and updated code using deprecated apis for firstPaint time

## [6.7.7] - 2020-05-27
### Added
- `disableStorage` option to disable the write/read/delete operations in localStorage, sessionStorage and cookies
- `bitrate` in joinTime event
- adManifest and adBreakStart now can wait until adStart event (and ignore adInit)
- Stop event can be blocked using the information provided in `ad.expectedPattern` and given postrolls in the adsAdapter. Stop can be sent using adStop too, when last postroll ends.

## [6.7.6] - 2020-04-27
### Added
- Simplified logic for `sessionRoot` parameter, preventing bugs in some usecases

## [6.7.5] - 2020-04-16
### Added
- `parentId` for start, init and error when sessions are active

## [6.7.4] - 2020-04-15
### Added
- With `content.sendTotalBytes` enabled, it sends both bitrate and totalBytes.

## [6.7.3] - 2020-04-14
### Added
- totalBytes metric: `getTotalBytes` method for adapter, `content.totalBytes` and `content.sendTotalBytes` options

## [6.7.2] - 2020-04-06
### Fixed
- More checks for window.performance object to prevent crashes

## [6.7.1] - 2020-04-01
### Fixed
- Check for window.performance object to prevent crashes

## [6.7.0] - 2020-02-27
### Added
- Support to send audio and video codecs by the adapters
- Now adapterless events are reported as `adapterless-js`

## [6.5.26] - 2020-02-05
### Added
- Support to send audio and video codecs by the adapters
### Fixed
- Support for samsung 2012 models, changed bind() polyfill

## [6.5.25] 2020-01-22
## Removed
- `mediaDuration` from join time event
## Added
- `/init` fired before `/error` if no view was opened

## [6.5.24] 2020-01-10
## Fixed
- IE compatibility issues

## [6.5.23] 2020-01-03
## Added
- Codecov
- jsinspect to test script to prevent duplicated code
- Unit test coverage for most classes
- StreamingProtocol constants to use on `content.streamingProtocol` option
- Math.trunc polyfill
## Fixed
- `adPosition` and `breakPosition` replaced by `position`
- Chrono issues when pausing it
- Plugin getters refactor
- Plugin listeners not able to unregister from adapter events
- Optimized type parsers
- Adapter split in 3 files, but still one class
- Background and block detectors refactor
- Refactor for hybridnetwork and storage
## Removed
- `content.streamingProtocol` option restrictions
- `parentId` references

## [6.5.22] 2019-12-18
## Fixed
- Player load time calculation when setting the adapter together with the plugin instantiation

## [6.5.21] 2019-12-18
## Added
- Player load time calculation
- `parse.manifest` option, replacing `parse.dash`, `parse.hls` and `parse.locationHeader`
- `Amazon` cdn added to `parse.cdnNode` / `parse.cdnNode.list`
## Fixed
- Regexp for parsing dash to reduce `/start` event size

## [6.5.20] 2019-11-20
## Fixed
- Removed es6 code on fireQuartile

## [6.5.19] 2019-11-05
## Added
- New case for `parse.dash`: now reads `baseUrl` tag before segmentUrl and segmentTemplate
## Fixed
- In stop event `sessionRoot` was reported wrongly

## [6.5.18] 2019-10-23
## Added
- `ad.blockDetection` option, false by default, to make a request to detect if an adblocker is enabled.
## Fixed
- Check to prevent the same adQuartile being triggered more than once for the same ad
- Check to ignore `stop` restrictions when no ad manifest is sent

## [6.5.17] 2019-10-15
## Fixed
- Video custom event fix on willsend

## [6.5.16] 2019-10-03
## Fixed
- In some cases monitor reporting fake buffers because the timer was being triggered twice

## [6.5.15] 2019-09-23
## Added
- Changed hybridNetwork class
- Added eslint
- Removed experiment option
- Request for adblock detection is now Async
- ParsedResource not sent when is not needed for init, start and error events
- Added `background.settings.playstation`

## [6.5.14] 2019-08-26
## Fixed
- DataExtractor trycatched

## [6.5.13] 2019-08-22
## Fixed
- Retries mechanism not working

## [6.5.12] 2019-08-19
## Added
- adManifest and adBreak events forced before adErrors if were not sent

## [6.5.11] 2019-08-07
## Added
- Support for session events for appletv3
## Fixed
- Parse dash definition

## [6.5.10] 2019-08-02
## Added
- `parse.dash` feature, updated from `parse.dashLocation` (removed)
- `parsedResource` parameter

## [6.5.9] 2019-07-29
## Fixed
- Adblock request trycatched

## [6.5.8] 2019-07-25
## Fixed
- Issues related to buffer and seek monitor reporting fake events increasing the playhead check interval

## [6.5.7] 2019-07-15
## Added
- Support for TVML

## [6.5.6] 2019-07-03
## Fixed
- Wrong ad join duration for first ad for non first views.

## [6.5.5] 2019-06-19
## Fixed
- Forced code for view and session stop events.
- Forced context for session event.
- Simplified `init` logic and chronos.
- P2P not being reported
## Removed
- `Sessions` from `beat` requests
## Added
- `device.isAnonymous` option

## [6.5.4] 2019-06-17
## Added
- `ad.provider` option, `getProvider` method for adapter `provider` parameter in `adStart`
- Fingerprint now is deviceUUID, simplified logic.
- Device metadata now sent in `deviceInfo`.
## Removed
- `preload` metric
## Fixed
- Chrono for jointime with prerolls when adapter `init` is called manually
- Protected `code` parameter to fireError() overrides

## [6.5.3] 2019-06-06
## Removed
- `Sessions` parameter in `beat`
## Fixed
- Https for adblock detector

## [6.5.2] 2019-05-31
## Fixed
- Postrolls after stop fix

## [6.5.1] 2019-05-31
## Removed
- ParentId for sessions
## Fixed
- Jointime calculation when the view has init

## [6.5.0] 2019-05-30
## Added
- Smartads v3.0
- Removed standardadapter
- Some options deprecated, a warning is shown if the used option has a new name
## Important notes
- Adapters with version 6.4.x and lower may not be compatible with this lib.
If there is no 6.5.0 or higher version for the adapter you need please ask plugins@nicepeopleatwork.com

## [6.4.30] 2019-05-24
## Added
- `parse.fdsResponseHost` option

## [6.4.29] 2019-05-22
## Fixed
- Wrong jointime for non-first views, with prerolls

## [6.4.28] 2019-05-13
## Changed
- Akamai parser

## [6.4.27] 2019-05-09
## Changed
- Default host from `nqs.nice264.com` to `a-fds.youborafds01.com`

## [6.4.26] 2019-04-30
## Fixed
- Parse hls feature when the manifest has more than 2 levels in the same host.

## [6.4.25] 2019-04-18
## Added
- `parse.dashLocation`feature, parsing dash manifests with `location` tag.

## [6.4.24] 2019-04-12
## Added
- Extra check to prevent wrong ad positions
## Fixed
- Minor fixes, found improving test coverage

## [6.4.23] 2019-04-05
## Added
- Feature to delay start event, using `waitForMetadata` and `pendingMetadata` options.

## [6.4.22] 2019-03-28
## Added
- `fireEvent` method for adapters to report custom events.
- `content.metrics` and `session.metrics`options to report custom metrics in ping and beat events.
- `getMetrics` method in adapter, to report custom metrics in ping.
- Automatic data request every hour if no views or sessions are active.
- New options: `user.email`, `content.package`, `content.saga`, `content.tvShow`, `content.season`, `content.episodeTitle`, `content.channel`, `content.id`, `content.imdbId`, `content.gracenoteId`, `content.type`, `content.genre`, `content.language`, `content.subtitles`, `content.contractedResolution`, `content.cost`, `content.price`, `content.playbackType`, `content.drm`, `content.encoding.videoCodec`, `content.encoding.audioCodec`, `content.encoding.codecSettings`, `content.enconding.codecProfile`, `content.encoding.containerFormat`,
- New option alias: `user.type`, `user.name`, `user.obfuscateIp`, `user.anonymousId`, `app.https`, `background.settings.iOS`, `content.customdimension.x`instead of `customDimension.x`
- Deprecated `isInfinity`
### Fixed
- Minor infinity fixes

## [6.4.21] 2019-03-26
## Fixed
- Wrong jointime for views without init with prerolls.

## [6.4.20] 2019-03-13
## Added
- Retry for requests with response code higher than 399

## [6.4.19] 2019-03-06
## Added
- `preventZombieViews` option, true by default

## [6.4.18] 2019-03-05
## Added
- `npawPluginUUID` parameter
## Fixed
- Adapter not working when it needs no player reference

## [6.4.17] 2019-02-21
## Fixed
- `deviceCode` not working properly

## [6.4.16] 2019-01-30
## Added
 - Telefonica CDN Host and type detection
 - `app.name` and `app.releaseVersion` options
 - Check to prevent wrong values as ad position
## Fixed
 - CDN Host and type not being reported always
 - Device options being not sent

## [6.4.15] 2019-01-30
## Fixed
 - AdAdapter closing views when ad ended

## [6.4.14] 2019-01-29
## Fixed
 - Stop not being sent when called by the adapter, after a manual call of init
## Added
 - Closing buffers when pause begins
 - Updated fingerprint
 - Alias for `extraparam` : `customDimension`
 - Alias for `title2`: `program`
 - Device options: `device.brand`, `device.type`, `device.model`, `device.name`, `device.osName`,
 `device.osVersion`,`device.browserName`,`device.browserVersion`, `device.browserType`, `device.browserEngine`
 - setExtraParams methods: `setCustomDimensions` and `setAdCustomDimensions`
 - Infinity internal refactor
 - Cookies disabled by default, no fallback.
 - Libversion reported in start events
 - Teltoo p2p support

## [6.4.13] 2018-12-27
## Added
 - Automatic polyfill when needed (`bind` and `foreach`)
 - System sent in all events
 - Infinity events not reported without sessionId

## [6.4.12] 2018-12-18
## Fixed
 - Wrong jointime calculation with multiple prerolls and init

## [6.4.11] 2018-11-23
## Added
 - `newSession` method for infinity
## Fixed
 - `InfinityStarted` using wrong storage

## [6.4.10] 2018-11-21
## Fixed
 - Nav after a navigation when data is requested in a previous page but start is not sent
 - NaN renditions, renditions with non numeric value as a bitrate

## [6.4.9] 2018-10-18
## Added
 - Smartswitch options: `smartswitch.configCode`, `smartswitch.groupCode` and `smartswitch.contractCode`
 - Teltoo p2p

## [6.4.8] 2018-09-17
## Fixed
 - ParseHls resource priority over content.resource option

## [6.4.7] 2018-09-04
## Added
 - `forceInit` option

## [6.4.6] 2018-08-30
## Fixed
 - Forced to send session root for infinity events

## [6.4.5] 2018-08-29
## Fixed
 - Sessions not reported properly on few devices
 - Pause duration in stop event, after a view with pause and resume

## [6.4.4] 2018-08-28
## Added
 - Check for infinity between pages not sharing localstorage

## [6.4.3] 2018-08-28
## Added
 - device code for infinity

## [6.4.2] 2018-08-27
## Fixed
 - host race condition for infinity

## [6.4.1] 2018-08-16
## Added
 - isInfinity option

## [6.4.0] 2018-08-16
## Added
 - New infinity design: methods rework
 - Infinity register values
 - Fingerprint
 - Use of cookies when no storages available
 - Referral and language report
 - AnonymousUser option
 - Background detector for infinity
 - Viewcodes with timestamp instead of numbers
 ## Removed
 - Unused values from entities
 - `session.expire`option
 ## Fixed
 - Now stop can be called after init (without start)

## [6.3.5] 2018-08-02
## Fixed
 - LocationHeader resource priority over content.resource option

## [6.3.4] 2018-07-17
## Fixed
 - Iterating Streamroot instances as array

## [6.3.3] 2018-07-11
## Fixed
 - Streamroot not working

## [6.3.2] 2018-07-06
## Fixed
 - Location header functionality
 - Blocked error event flood
 - Sending adErrors after session started

## [6.3.1] 2018-07-04
## Added
 - Support for new streamroot api
## Removed
 - Wrong throughput value calculated with p2p plugins
## Fixed
 - Ad number increase during adErrors.

## [6.3.0] 2018-06-14
## Added
 - Username reported in /data when available
 - New adapter object to extend (StandardAdapter) making easier to implement adapters for html5ish players.
 - Offline mode
 - `Offline` option
 - Fatal error not sending 'fatal' level
 - Fireclick can be used with string parameter as url directly
 - fireSkip() method (fireStop({skipped: true}) alias)
## Fixed
 - Jointime value for views started with ads and without adapter

## [6.2.6] 2018-05-15
## Fixed
 - Aderror cant create views
 - Jointime value when used plugin.fireInit() instead of plugin.getAdapter().fireInit()/start

## [6.2.5] 2018-05-10
## Fixed
 - Now an adstart can start a view (sending an init/start before)
 - Stop being reported twice when called from the plugin after being called from the adapter.
 - Fixed ad error report creating new views and being reported as errors

## [6.2.4] 2018-05-08
## Fixed
 - Forced start/init before adError to track preroll/precontent ad errors
## Added
 - String constants for ad positions `Adapter.AdPosition` PRE, MID and POST

## [6.2.3] 2018-04-27
## Added
 - getLatency, getPacketLoss, getPacketSent adapter methods
 - reporting latency, packetLoss and packetSent in pings
 - `content.isLive.noSeek` option to ignore seeks, only for live content.
## Fixed
 - Wrong value for resource was sent when it was set as an option after init and before start

## [6.2.2] 2018-04-13
## Removed
 - Duration and playhead for live content

## [6.2.1] 2018-04-12
## Added
 - SmartTV device type

## [6.2.0] 2018-04-06
## Added
 - Device (android, iphone and desktop) detection
 - Background detection
 - Background.settings, background.enabled, background.settings for each device type options.
## Fixed
 - Reset pause seek and buffer chronos after stop is sent
 - Removed errorLevel value by default

## [6.1.15] 2018-03-28
## Fixed
 - Restart isStarted plugin flag

## [6.1.14] 2018-03-21
## Added
 - Experiment option
 - Referer option, to set it manually

## [6.1.13] - 2018-03-02
## Fixed
 - Fixed view code when view starts with an error
 - Fixed double start sent in some cases
 - Fixed wrong jointime when using plugin init
 - Viewcode changed for frozen views
 ## Added
 - Cancel buffer method
 - Cancel seek method
 - getHouseshold method in adapter
 - parse.locationHeader option

## [6.1.12] - 2018-02-13
## Fixed
 - Fixed pauseDuration not being sent in stop
 - Adapter fireInit behaviour with auto start
 - Prevent crash if storage is not available
## Added
 - cdnType and cdnNode can be set as an option
 - fireCasted method for adapter

## [6.1.11] - 2018-01-30
## Added
 - Buffer monitor for custom playrates

## [6.1.10] - 2018-01-29
## Added
 - IsP2PEnabled for peer5
 - Added 10 ad extraparam
 - Reported playhead for live content

## [6.1.9] - 2018-01-16
## Added
 - Added userType and content.streamingProtocol options
 ## Fixed
 - Changed p2p params names

## [6.1.8] - 2017-12-22
## Fixed
 - Fixed access to storages when is not possible but exists
 - Added ad.campaign, ad.title and ad.resource options
 - Removed background detection

## [6.2.0-beta2] - 2017-12-18
## Added
 - Request number param to prevent /data calls being cached
 - P2PEnabled param activated

## [6.2.0-beta] - 2017-12-05
## Added
 - Device detection (android, iphone, desktop)
 - Background detection and actions
 - Options for background detection 'autoDetectBackground' and 'autoDetectBackgroundSettings'

## [6.1.7] - 2017-12-04
## Fixed
 - Now error message is 'msg' instead of 'errorMsg'

## [6.1.6] - 2017-11-28
## Fixed
 - FatalError now sends error and stop
 - Stop event sends pauseDuration if needed

## [6.1.5] - 2017-11-23
## Added
 - P2P enabled parameter getter
 - Option obfuscateIP

## [6.1.4] - 2017-11-14
### Fixed
 - Semicolons no longer ignored in hlsparser
 - IsLive detection when start
 - resource.js init fix
 - Preventing to send init twice
## Added
 - Automatic use of adInit when needed

## [6.1.3] - 2017-10-31
### Fixed
 - Several issues with init, start and jointime events

## [6.1.0] - 2017-10-23
### Added
- Support for Streamroot and peer5 p2p metrics
- Send player name on start
- Send media duration and resource on jointime
- Get throughput values by default when using p2p
- Remove playhead and media duration of live content

## [6.0.10] - 2017-10-16
### Added
- FireStop with end param to end views with ad.afterStop option

## [6.0.9] - 2017-10-10
### Added
- Option ad.afterStop to catch ads after player stop

## [6.0.8] - 2017-10-04
### Added
- Option ad.ignore to block AdsAdapter messages

## [6.0.7] - 2017-10-02
### Added
- Add playhead on ad events

## [6.0.6] - 2017-09-21
### Added
- Add p2p metrics

## [6.0.5] - 2017-07-24
### Added
- Add init and adInit events, called with `fireInit`
- Add `fireNoServedAd` as special error

## [6.0.4] - 2017-07-24
### Added
- add `isDone` property to `ResourceTransform`.
- Add more `extraParam`, up to 20.

## [6.0.3] - 2017-06-06
### Changed
- Removed `manifest.json` from repo.
- Removed `manifest.json` from `.npmignore`.

## [6.0.2] - 2017-06-06
### Added
- `username` added to infinity's `nav`.

### Git
- Added `manifest.json` to `.gitignore`.

## [6.0.1] - 2017-06-01
### Fixed
- Fix several `infinity` module issues.

### Added
- Support for `adError`, `adClick` and `adBlocked`.
- Add `getComm` to `Plugin` and `Infinity`.
- Add `npm run clean` command to delete `dist`, `coverage` and `deploy` folders.

## [6.0.0] - 2017-05-30
### Fixed
- Fixed CDN Parser issues
- Fixed /error before /init

## [6.0.0-rc.infinity] - 2017-05-19
### Added
- Infinity module

## [6.0.0-rc] - 2017-01-09
### Added
- First release
